package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class gui2d {
	//private static Context appContext;
	f3ds F3ds;
	a3df A3df;
	static int ILE_MAX=10;
	Vec2 pos[];
	float wave[];
	int co[];
	int ile;
	float Hit_wave;
	//public static int Menu_Flaga;
	Vec2 shadowPos;
	float scrollMap, scrollMaptmp, scrollMapold;
	Vec2 ShottingPoint;
	int downTouch=0;
	int upTouch=0;
	fontInv FontInv;
	private static Context appContext;

	float buttonsLag;

	int levelStateInt[];
	public static int actualLevel;
	public static int survivemodeflag;
	public static int multiplayerFlag;
	Vec4 lvlTheme[];

	gui2d(Context aC)
	{
		buttonsLag=0.0f;
		appContext=aC;
		survivemodeflag=0;
		multiplayerFlag=0;

		actualLevel=-1;
		levelState=new float[24];
		levelStateInt=new int[24];
		for(int i=0;i<24;i++) {
			levelState[i] = 0.0f;
			levelStateInt[i]=0;
		}
		levelStateInt[0]=1;//zmiana na 1 do finalnej wersji

		touchIndexTmp=-1;

		lvlTheme= new Vec4[24];
		lvlTheme[0]=new Vec4( 1.0f, 1.0f, 1.0f, 1.0f );
		lvlTheme[1]=new Vec4( 0.75f, 1.5f, 1.5f, 1.0f );
		lvlTheme[2]=new Vec4( 0.5f, 0.75f, 1.0f, 2.5f  );
		lvlTheme[3]=new Vec4( 0.75f, 1.0f, 0.5f, 1.0f  );
		lvlTheme[4]=new Vec4( 0.34f, 0.61f, 0.93f, 2.2f );
		lvlTheme[5]=new Vec4( 1.0f, 0.75f, 0.75f, 1.7f  );
		lvlTheme[6]=new Vec4( 0.7f, 0.6f, 0.6f, 2.5f  );
		lvlTheme[7]=new Vec4( 1.0f, 1.0f, 0.9f, 0.8f  );
		lvlTheme[8]=new Vec4( 0.5f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[9]=new Vec4( 0.8f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[10]=new Vec4( 1.0f, 1.0f, 1.0f, 2.75f  );
		lvlTheme[11]=new Vec4( 0.75f, 1.5f, 1.5f, 1.0f  );
		lvlTheme[12]=new Vec4( 0.7f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[13]=new Vec4( 0.5f, 0.5f, 1.5f, 2.7f  );
		lvlTheme[14]=new Vec4( 1.0f, 1.0f, 0.6f, 1.0f  );
		lvlTheme[15]=new Vec4( 1.0f, 0.5f, 0.5f, 2.0f  );
		lvlTheme[16]=new Vec4( 0.6f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[17]=new Vec4( 1.0f, 1.0f, 1.0f, 0.7f  );
		lvlTheme[18]=new Vec4( 0.0f, 0.8f, 1.0f, 2.0f  );
		lvlTheme[19]=new Vec4( 1.5f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[20]=new Vec4( 1.0f, 1.5f, 1.5f, 1.0f  );
		lvlTheme[21]=new Vec4( 0.5f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[22]=new Vec4( 0.5f, 1.0f, 1.0f, 1.0f  );
		lvlTheme[23]=new Vec4( 0.5f, 1.0f, 1.0f, 1.0f  );

		FontInv= new fontInv(aC);
		FontInv.init();
		scrollMap=0.0f;
		scrollMaptmp=0.0f;
		scrollMapold=0.0f;
		pos= new Vec2[ILE_MAX];
		wave= new float [ILE_MAX];
		co= new int[ILE_MAX];
		shadowPos = new Vec2();
		ShottingPoint = new Vec2();
		shadowPos.set(-150.0f,0.0f);
		touchPos3d=new Vec2();
	//Menu_Flaga=1;  /// 1 - menu        0 - gra         2 - game over
	//appContext=aC;
	F3ds  = new f3ds(aC);
	A3df = new a3df(aC);
	ile=0;
	Hit_wave=0.0f;

	for(int i=0;i<ILE_MAX;i++)
	{
		wave[i]=-1.0f;
		pos[i]=new Vec2();
		co[i]=-1;
	}
	
	for(int i=0;i<sovietILE;i++)
	{
		sovietAnim[i]=0.0f;
		sovietPos[i]=new Vec2();
		///sovietPos[i].set(1.5f*i, -2.0f);
	}
	sovietCo[0]=0;
	sovietCo[1]=1;
	sovietCo[2]=2;
	sovietCo[3]=3;
	sovietCo[4]=0;

	sovietPos[0].set(0.0f, -3.7f);
	sovietPos[1].set(1.5f, -2.8f);
	sovietPos[2].set(3.0f, -3.4f);
	sovietPos[3].set(4.5f, -3.2f);
	sovietPos[4].set(7.0f, -3.7f);
	
	sovietSpeed[0]=0.04f;
	sovietSpeed[1]=0.035f;
	sovietSpeed[2]=0.03f;
	sovietSpeed[3]=0.03f;
	sovietSpeed[4]=0.04f;
	}
	

	
	
	
	
	
	
	void drawReset(int ShaderProgram)
	{
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		
		float[] tmpMatrix = new float[16];
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, -0.125f+(float)Math.sin(GrassWave)*0.002f, -0.09f+(float)Math.cos(GrassWave)*0.002f, -0.15f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
		
		F3ds.DrawModel(56, tekstury.texture[3], ShaderProgram );	
		

		GLES20.glDisable(GLES20.GL_BLEND);
	}


	float YouAreDeadwave, YouKillEnemywave;

	void YouAreDead(int ShaderProgram, float loop)
	{
		YouAreDeadwave=YouAreDeadwave-0.01f*loop;
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

		float[] tmpMatrix = new float[16];
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, -0.125f+(float)Math.sin(GrassWave)*0.002f, -0.09f+(float)Math.cos(GrassWave)*0.002f, -0.15f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(116, tekstury.texture[3], ShaderProgram );

		GLES20.glDisable(GLES20.GL_BLEND);
	}



	void YouKillEnemy(int ShaderProgram, float loop)
	{
		YouKillEnemywave=YouKillEnemywave-0.01f*loop;
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

		float[] tmpMatrix = new float[16];
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, -0.125f+(float)Math.sin(GrassWave)*0.002f, -0.09f+(float)Math.cos(GrassWave)*0.002f, -0.15f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(115, tekstury.texture[3], ShaderProgram );

		GLES20.glDisable(GLES20.GL_BLEND);
	}
	
	
	
	

	void drawMenu(int ShaderProgram)
	{
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		
		float[] tmpMatrix = new float[16];
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, -0.125f+(float)Math.sin(GrassWave)*0.002f, -0.09f+(float)Math.cos(GrassWave)*0.002f, -0.15f+(float)Math.sin(GrassWave)*0.002f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
		
		F3ds.DrawModel(43, tekstury.texture[25], ShaderProgram );	
		
		
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, -0.125f+(float)Math.sin(-GrassWave+0.98f)*0.002f, -0.09f+(float)Math.cos(-GrassWave+0.98f)*0.002f, -0.15f-(float)Math.sin(GrassWave)*0.002f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

		F3ds.DrawModel(55, tekstury.texture[25], ShaderProgram );
		
		
		GLES20.glDisable(GLES20.GL_BLEND);
	}



	void drawBlue(int ShaderProgram)
	{
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);

		float[] tmpMatrix = new float[16];
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, -0.125f+(float)Math.sin(GrassWave)*0.002f, -0.09f+(float)Math.cos(GrassWave)*0.002f, -0.15f+(float)Math.sin(GrassWave)*0.002f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

		F3ds.DrawModel(114, tekstury.texture[18], ShaderProgram );
	}



	static int sovietILE = 5;
	Vec2 sovietPos[]=new Vec2[sovietILE];
	float sovietAnim[]=new float[sovietILE];
	float sovietSpeed[]=new float[sovietILE];
	int sovietCo[]=new int[sovietILE];
	
	void SovietDraw(int ShaderProgram, float loop)
	{
		 float[] tmpMatrix = new float[16];
		 	
		 int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y);

		 for(int i=0;i<sovietILE;i++)
			 {
			 sovietAnim[i]=sovietAnim[i]+sovietSpeed[i]*loop;
			 if(sovietAnim[i]>4.0f)
				 sovietAnim[i]=0.0f;
			 sovietPos[i].x=sovietPos[i].x-0.01f*loop;
			 if(sovietPos[i].x<-9.0f)
				 sovietPos[i].x=8.0f;
			 
			 Matrix.setIdentityM(tmpMatrix, 0);
			 Matrix.translateM(tmpMatrix, 0, sovietPos[i].x, sovietPos[i].y, 0.0f);
	
			 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			 	
			 if(sovietCo[i]==2 || sovietCo[i]==3)
			 A3df.DrawAnimModel(sovietCo[i],tekstury.texture[23],ShaderProgram,sovietAnim[i]);
			 else
			 A3df.DrawAnimModel(sovietCo[i],tekstury.texture[1],ShaderProgram,sovietAnim[i]); 
			 
			 if(sovietCo[i]==0)
				 F3ds.DrawModel(29, tekstury.texture[1], ShaderProgram );//flaga
			 if(sovietCo[i]==2)
				 F3ds.DrawModel(35, tekstury.texture[23], ShaderProgram );//flaga
			 
	
			 
			 if(sovietCo[i]==0 || sovietCo[i]==1)
				 {
				 Matrix.setIdentityM(tmpMatrix, 0);
				 Matrix.translateM(tmpMatrix, 0, sovietPos[i].x, sovietPos[i].y, 0.0f);
				 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
				 F3ds.DrawModel(4, tekstury.texture[1], ShaderProgram );
				 }
			 }
	}
	
	
	
float GrassWave;
	
void grassDraw(int ShaderProgram, float loop)
{
	GrassWave+=0.07f*loop;
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	GLES20.glUniform1f(iWaveVS, GrassWave);
	

	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0, 0.0f, -4.4f, 0.0f);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 

	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0, 0.0f, 0.0f, 0.0f);
	GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);


	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y);
	
	int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture");
	GLES20.glUniform1i(texLoc, 0);
	
	//texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texshoot");
	//GLES20.glUniform1i(texLoc, 1);
	
	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[8]);

	//GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
	//GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.dynamicTexture[0]);
	
	//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
	
	F3ds.DrawModel(8, ShaderProgram );
}
	
	

void DrawTree(int ShaderProgram)// 9 10 10 11
{
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	
	int iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	GLES20.glUniform1f(iVwave, GrassWave);

	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y);
		
	Matrix.setIdentityM(tmpMatrix, 0);

	Matrix.translateM(tmpMatrix, 0, 3.5f, -4.4f, -1.0f);

	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	
	
	F3ds.DrawModel(9, tekstury.texture[10], ShaderProgram );
}


void DrawStatic(int ShaderProgram)//65.1 8.0 16 5 2
{
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	
	Matrix.setIdentityM(tmpMatrix, 0);

	Matrix.translateM(tmpMatrix, 0, 5.9f, -4.4f, 0.0f);
	
	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y);
	
	
	GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
	
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	
	
 
	
	
	F3ds.DrawModel(16, tekstury.texture[5], ShaderProgram );
}




void DrawLeaf(int ShaderProgram)
{
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

	int iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	GLES20.glUniform1f(iVwave, GrassWave); 
	
	iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave2");
	GLES20.glUniform1f(iVwave, GrassWave);

	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y);

	Matrix.setIdentityM(tmpMatrix, 0);

	Matrix.translateM(tmpMatrix, 0, 4.5f, -4.4f, -1.0f);

	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	
	F3ds.DrawModel(10, tekstury.texture[11], ShaderProgram );
}	
	
	
	

void add(float x, float y, int c)
{
pos[ile].set(x, y);
co[ile]=c;
wave[ile]=0.0f;
ile++;
}


void del(int j)
{
for(int i=j; i<ile-1; i++)
	{
	pos[i].set(pos[i + 1].x, pos[i + 1].y);
	wave[i]=wave[i+1];
	co[i]=co[i+1];
	}
ile--;
}

void DrawDymek(int ShaderProgram, float loop)
{
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");


	
	for(int i=0;i<ile;i++)
			{
			Matrix.setIdentityM(tmpMatrix, 0);
			Matrix.translateM(tmpMatrix, 0, pos[i].x,  pos[i].y+ (float)Math.sin(wave[i]*5.0f)*0.5f, -wave[i]*5.0f);
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			
			F3ds.DrawModel(31, tekstury.texture[3], ShaderProgram );
			
			wave[i]+=0.01f*loop;
			if(wave[i]>1.0f)
				del(i);
			}
}



Vec2 touchPos3d;
float levelState[];






void reset()
{
	survivemodeflag=0;
	multiplayerFlag=0;
	for(int i=0;i<24;i++)
	{
		levelState[i]=0.0f;
	}
	//levelState[0]=1;
}

int touchIndexTmp;
void MapLogic(float loop)
{
	if(downTouch==1)
		for (int i = 0; i < 24; i++) {
			if (Math.abs(touchPos3d.x - 2.0f * i) < 1.0f)
				{
				levelState[i] = 1.0f;
				touchIndexTmp=i;
				}
			else
				levelState[i] = 0.0f;
			}




	for(int i=0;i<24;i++)
	{
	if(levelState[i]>0.0f)
		levelState[i]=levelState[i]-0.02f*loop;
	}


	downTouch=0;
	upTouch=0;
}








void DrawMap(int ShaderProgram)
{
	float[] tmpMatrix = new float[16];

	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");


	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, 1.0f, 0.1f);

	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0, 10.0f, 0.0f, 0.0f);
	Matrix.scaleM(tmpMatrix, 0, 0.5f, 0.5f, 0.5f);
	GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

	F3ds.DrawModel(0, tekstury.texture[0], ShaderProgram);

	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0, 40.0f, 0.0f, 0.0f);
	Matrix.scaleM(tmpMatrix, 0, 0.5f, 0.5f, 0.5f);
	GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);

	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

	F3ds.DrawModel(0, tekstury.texture[0], ShaderProgram);
for(int i=0;i<24;i++) {
	Matrix.setIdentityM(tmpMatrix, 0);

	Matrix.translateM(tmpMatrix, 0, 2.0f * i, 3.1f, 0.0f);

	Matrix.scaleM(tmpMatrix, 0, 0.5f, 0.5f, 0.5f);

	GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


	F3ds.DrawModel(79, tekstury.texture[23], ShaderProgram);
	}
}


void DrawMapFlag(int ShaderProgram)
{
	float[] tmpMatrix = new float[16];

	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");

	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	int iColorVS = GLES20.glGetUniformLocation(ShaderProgram, "colorUni");

	GLES20.glUniform1f(iColorVS, 0.0f);
	GrassWave=GrassWave+0.1f;
	GLES20.glUniform2f(shadowposFS, 1.0f, 0.1f);
	for(int i=0;i<24;i++) {
		Matrix.setIdentityM(tmpMatrix, 0);


		GLES20.glUniform1f(iWaveVS, i + GrassWave);

		Matrix.translateM(tmpMatrix, 0, 2.0f * i, 3.1f, 0.0f);


		Matrix.scaleM(tmpMatrix, 0, 0.5f + levelState[i] * 0.25f, 0.5f + levelState[i] * 0.25f, 0.5f + levelState[i] * 0.25f);

		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

		if(levelStateInt[i]==1)
			F3ds.DrawModel(80, tekstury.texture[26], ShaderProgram);
		if(levelStateInt[i]==0)
			F3ds.DrawModel(81, tekstury.texture[26], ShaderProgram);
	}


}

	void DrawLvlNumber(int ShaderProgram)//0.43
	{
		int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		int iColorVS = GLES20.glGetUniformLocation(ShaderProgram, "colorUni");
		GLES20.glUniform1f(iColorVS, 0.85f);

		for(int i=0;i<24;i++) if(levelStateInt[i]==1) {
			GLES20.glUniform1f(iWaveVS, i + GrassWave);

			int istring=i+1;
			FontInv.DrawText3D(new Vec2( 0.3f+2.0f * i+levelState[i]*0.2f, 2.67f-levelState[i]*0.2f), 1.0f+levelState[i]*0.25f, ShaderProgram, tekstury.texture[4], istring + " ");
		}
	}




	void DrawMapGras(int ShaderProgram,float loop)
	{
		float[] tmpMatrix = new float[16];

		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");


		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS, 1.0f, 0.1f);
		int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		GLES20.glUniform1f(iWaveVS, GrassWave);
		GrassWave+=0.07f*loop;


		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, 0.0f, -1.0f, 1.0f);
		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(8, tekstury.texture[8], ShaderProgram);


		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, 0.0f, 1.1f, -1.1f);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

		F3ds.DrawModel(8, tekstury.texture[8], ShaderProgram );
	}


	void DrawMapLeaf(int ShaderProgram)
	{
		float[] tmpMatrix = new float[16];

		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");

		int iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave");


		iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave2");
		GLES20.glUniform1f(iVwave, GrassWave);

		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS,  1.0f, 0.1f);

		for(int i=0;i<10;i++) {
			Matrix.setIdentityM(tmpMatrix, 0);
			GLES20.glUniform1f(iVwave, GrassWave+i);
			Matrix.translateM(tmpMatrix, 0, -1.9f+i * 5.5f, 1.0f, -1.7f);
			GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

			F3ds.DrawModel(10+i%2*2, tekstury.texture[11]+i%2, ShaderProgram);
		}
	}


	void DrawMapTree(int ShaderProgram)// 9 10 10 11
	{
		float[] tmpMatrix = new float[16];

		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");

		int iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave");


		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS,  1.0f, 0.1f);

		for(int i=0;i<10;i++) {
			GLES20.glUniform1f(iVwave, GrassWave + i);
			Matrix.setIdentityM(tmpMatrix, 0);
			Matrix.translateM(tmpMatrix, 0, -1.9f+i * 5.5f, 1.0f, -1.7f);

			GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
			F3ds.DrawModel(9+i%2*2, tekstury.texture[10], ShaderProgram);
		}
	}











	void draw(int ShaderProgram, float loop, int allyIle)
{
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

	Matrix.setIdentityM(tmpMatrix, 0);
	
	Matrix.translateM(tmpMatrix, 0, -0.125f, -0.09f, -0.15f);

	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	
	if(Hit_wave>0.5f)
	{
	F3ds.DrawModel(1, tekstury.texture[2], ShaderProgram );	
	Hit_wave-=0.1f*loop;
	}

	
//	if(gun==0)
//		F3ds.DrawModel(2+44*rope, tekstury.texture[3], ShaderProgram );
//	if(gun==1)
	if(tekstury.secNdPlayer_flag>0)
		F3ds.DrawModel(46, tekstury.texture[14], ShaderProgram );
	else {
		F3ds.DrawModel(2, tekstury.texture[14], ShaderProgram);

		if(tekstury.bulletTime<0.5) {
			int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "pulse");
			GLES20.glUniform1f(iWaveVS, (float) Math.sin(GrassWave*4.0f));
			//GrassWave = GrassWave + 0.2f;
			F3ds.DrawModel(118, tekstury.texture[14], ShaderProgram);

			GLES20.glUniform1f(iWaveVS, (float) Math.sin(1.0f));
		}
	}

//	if(gun==2)
//		F3ds.DrawModel(2+44*rope, tekstury.texture[18], ShaderProgram );
//	if(gun==3)
//		F3ds.DrawModel(2+44*rope, tekstury.texture[26], ShaderProgram );
//
//
	
	if(allyIle>0)
		for(int i=0; i<allyIle; i++)
		{
			Matrix.setIdentityM(tmpMatrix, 0);
			
			Matrix.translateM(tmpMatrix, 0, -0.125f, -0.09f-0.027f*i, -0.15f);

			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
		
			F3ds.DrawModel(49, tekstury.texture[3], ShaderProgram );	//ally icon
		}
	
	
}

	void drawBulletTime(int ShaderProgram, float loop, int allyIle)
	{
		if(tekstury.secNdPlayer_flag<1) {
			float activbullet = 0.0f;
			if (tekstury.bulletTime < 0.5f) {
				tekstury.bulletZasob = tekstury.bulletZasob - 0.007f * loop;
				activbullet = 1.0f;
			} else
				tekstury.bulletZasob = tekstury.bulletZasob + 0.001f * loop;

			if (tekstury.bulletZasob < 0.0f)
				tekstury.bulletTime = 1.0f;

			if (tekstury.bulletZasob > 1.0f)
				tekstury.bulletZasob = 1.0f;


			float[] tmpMatrix = new float[16];

			GrassWave += 0.05f * loop;

			int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

			int bulletlevelFS = GLES20.glGetUniformLocation(ShaderProgram, "bulletlevel");
			GLES20.glUniform1f(bulletlevelFS, tekstury.bulletZasob * 0.04f);
			int waveFS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
			GLES20.glUniform1f(waveFS, GrassWave);

			int activFS = GLES20.glGetUniformLocation(ShaderProgram, "activ");
			GLES20.glUniform1f(activFS, activbullet);

			Matrix.setIdentityM(tmpMatrix, 0);

			Matrix.translateM(tmpMatrix, 0, -0.125f, -0.09f, -0.15f);

			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
			F3ds.DrawModel(99, tekstury.texture[14], ShaderProgram);
		}
	}





	
}
