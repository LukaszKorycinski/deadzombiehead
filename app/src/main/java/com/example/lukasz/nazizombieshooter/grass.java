package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class grass {
	//private static Context appContext;
	 static int ILE_MODEL = 20;
	 Vector3f pos[];
	 Vector3f angle[];
	 float wave;
	 int co[];
	 int tex[];
	 int ile;
	 
	matrixInv MatrixInv = new matrixInv();
	f3ds F3ds;
	
	void reset()
	{
	ile=0;
	wave=0.0f;
	}
	 
	grass(Context aC)
	{
		  pos=new Vector3f[ILE_MODEL];
		  angle=new Vector3f[ILE_MODEL];
		  co=new int[ILE_MODEL];
		  tex=new int[ILE_MODEL];
		
		
	F3ds  = new f3ds(aC);
	wave=0.0f;
	ile=0;
	for(int i=0;i<ILE_MODEL;i++)
		{
		co[i]=0;
		pos[i]=new Vector3f();
		}	
	}
	
	
	void add(float x, float y, float z, int c, int t)
	{
		co[ile]=c;
		tex[ile]=t;
		pos[ile].set(x, y, z);
		ile++;	
	}
	
	




	public void Draw(int ShaderProgram, float loop, Vec2 shadowPos)
	{
		loop=loop*tekstury.bulletTime;
		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);


		wave+=0.1f*loop;
		float[] tmpMatrix = new float[16];
		
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
		
		int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		GLES20.glUniform1f(iWaveVS, wave);
		define Define=new define();
		for(int i=0;i<ile;i++)
			if(Define.distanc(pos[i].x, pos[i].y, shadowPos.x, shadowPos.y)<14.0f || co[i]==8  || co[i]==90)
		{
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, pos[i].z);

		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);

		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 

		//Matrix.setIdentityM(tmpMatrix, 0);
		//Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);

		
		
		int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture");
		GLES20.glUniform1i(texLoc, 0);
		
		//texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texshoot");
		//GLES20.glUniform1i(texLoc, 1);
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[tex[i]]);
	
		//GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		//GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.dynamicTexture[0]);
		
		//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		
		
		
		F3ds.DrawModel(co[i], ShaderProgram );
		}
	}


}










///////////////////////////////////////////////////////
////////////////////////////////////
//GLES20.glUseProgram(ShaderProgramGrass);
//iVPMatrix = GLES20.glGetUniformLocation(ShaderProgramGrass, "u_VPMatrix");
//
//int iWaveParameter = GLES20.glGetUniformLocation(ShaderProgramGrass, "wave");
//GLES20.glUniform1f(iWaveParameter, Grass.wave);Grass.wave=Grass.wave+0.012f;
//for(int i=0;i<Grass.ile;i++)
//{
//Matrix.setIdentityM(tmpMatrix, 0);
//Matrix.translateM(tmpMatrix, 0,Grass.pos[i].x, Grass.pos[i].y, Grass.pos[i].z);
//Matrix.multiplyMM(tmpMatrix, 0, ViewProjMatrix, 0, tmpMatrix, 0);
//GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
//
////DrawModel(Grass.co[i],Grass.tex[i]);
//}
//////////////////////////////////
