package com.example.lukasz.nazizombieshooter;

/**
 * Created by Lukasz on 2016-09-23.
 */
public interface OnHomePressedListener {
    public void onHomePressed();

    public void onHomeLongPressed();
}
