package com.example.lukasz.nazizombieshooter;

//import org.jbox2d.common.Vec2;

import android.opengl.Matrix;

public class matrixInv {
public static float[] mProjectionMatrix = new float[16];
public static float[] mViewProjectionMatrix = new float[16];
public static float[] ViewMatrix = new float[16];
//public static float[] ZeroMatrix = new float[16];


public static void camMatrix(float rotX, float rotY, float posX, float posY, float posZ)
{
	Matrix.setIdentityM(ViewMatrix, 0);

	
	Matrix.rotateM(ViewMatrix, 0, rotX, 1.0f, 0.0f, 0.0f);      
	Matrix.rotateM(ViewMatrix, 0, rotY, 0.0f, 1.0f, 0.0f); 
	Matrix.translateM(ViewMatrix, 0, posX, posY, posZ);
	
	Matrix.setIdentityM(mViewProjectionMatrix, 0);
	
	Matrix.multiplyMM(mViewProjectionMatrix, 0, mProjectionMatrix, 0, ViewMatrix, 0);
}



public static void perspectiveINV(float aspect) 
{
	float angle=1.3f;
	
	float near=0.1f;
	float far=15.0f;
	
float f = (float)Math.tan(0.5 * (Math.PI - angle));
float range = near - far;

mProjectionMatrix[0] = f / aspect;
mProjectionMatrix[1] = 0;
mProjectionMatrix[2] = 0;
mProjectionMatrix[3] = 0;

mProjectionMatrix[4] = 0;
mProjectionMatrix[5] = f;
mProjectionMatrix[6] = 0;
mProjectionMatrix[7] = 0;

mProjectionMatrix[8] = 0;
mProjectionMatrix[9] = 0; 
mProjectionMatrix[10] = far / range;
mProjectionMatrix[11] = -1;

mProjectionMatrix[12] = 0;
mProjectionMatrix[13] = 0;
mProjectionMatrix[14] = near * far / range;
mProjectionMatrix[15] = 0;
}


public Vec2 unproject(float xkursor, float ykursor, float szer, float wys)
{
    // Initialize auxiliary variables.
    Vec2 worldPos = new Vec2();

    // SCREEN height & width (ej: 320 x 480)
    float screenW = szer;
    float screenH = wys;

    // Auxiliary matrix and vectors
    // to deal with ogl.
    float[] invertedMatrix, transformMatrix,
        normalizedInPoint, outPoint;
    invertedMatrix = new float[16];
    transformMatrix = new float[16];
    normalizedInPoint = new float[4];
    outPoint = new float[4];

    // Invert y coordinate, as android uses
    // top-left, and ogl bottom-left.
    int oglTouchY = (int) (screenH - ykursor);

    /* Transform the screen point to clip
    space in ogl (-1,1) */       
    normalizedInPoint[0] =  (float) ((xkursor) * 2.0f / screenW - 1.0)*109;
    normalizedInPoint[1] =  (float) ((oglTouchY) * 2.0f / screenH - 1.0)*109;
    normalizedInPoint[2] = - 1.0f;
    normalizedInPoint[3] = 1.0f;

    /* Obtain the transform matrix and
    then the inverse. */

    Matrix.multiplyMM(
        transformMatrix, 0,
        mProjectionMatrix, 0,
        ViewMatrix, 0);
    Matrix.invertM(invertedMatrix, 0,
        transformMatrix, 0);       

    /* Apply the inverse to the point
    in clip space */
    Matrix.multiplyMV(
        outPoint, 0,
        invertedMatrix, 0,
        normalizedInPoint, 0);

    if (outPoint[3] == 0.0)
    {//error
        return worldPos;
    }

    // Divide by the 3rd component to find
    // out the real position.
    worldPos.set(
        outPoint[0] / outPoint[3],
        outPoint[1] / outPoint[3]);



	
//Vec2 wynik=new Vec2();

return worldPos;
}




}
