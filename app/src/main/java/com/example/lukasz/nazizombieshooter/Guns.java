package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

/**
 * Created by Lukasz on 2016-06-01.
 */
public class Guns {
    static int ILE_MODEL = 7;
    Vec2 pos[];
    int co[];
    int ile;
    matrixInv MatrixInv;
    f3ds F3ds;
    define Define;
    float wave;

    void reset()
    {
        ile=0;
    }


    Guns(Context aC)
    {
        Define=new define();
        pos=new Vec2[ILE_MODEL];
        co=new int[ILE_MODEL];

        MatrixInv = new matrixInv();

        F3ds  = new f3ds(aC);
        ile=0;
        wave=0.0f;
        for(int i=0;i<ILE_MODEL;i++)
        {
            pos[i]=new Vec2();
            co[i]=0;
        }
    }



    void del(int j)
    {
        for(int i=j; i<ile-1; i++) {
            pos[i].set(pos[i + 1].x, pos[i + 1].y);
            co[i]=co[i+1];
        }
        ile--;
    }



    void add(float x, float y, int c)
    {
        pos[ile].set(x,y);
        co[ile]=c;
        ile++;
    }



    public void Draw(int ShaderProgram, float loop)
    {
        //loop=loop*tekstury.bulletTime;
        float[] tmpMatrix = new float[16];

        int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
        int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
        wave=wave+5.0f*loop;

        for(int i=0;i<ile;i++)
        {


            Matrix.setIdentityM(tmpMatrix, 0);
            Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
            Matrix.rotateM(tmpMatrix, 0, wave, 0.0f, 1.0f, 0.0f);
            Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
            GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


            Matrix.setIdentityM(tmpMatrix, 0);
            Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
            Matrix.rotateM(tmpMatrix, 0, wave, 0.0f, 1.0f, 0.0f);
            GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);


            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[10]);


            F3ds.DrawModel(83+co[i], ShaderProgram );
        }
    }


}
