package com.example.lukasz.nazizombieshooter;

/**
 * Created by Lukasz on 2015-07-11.
 */
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class AppView extends GLSurfaceView {

    AppRenderer renderer;
    private Context appContext;
    private SharedPreferences preferences;
    private Activity activity;

    public AppView(Context context, Activity ac) {
        super(context);
        activity=ac;
        appContext=context;
        setEGLContextClientVersion(2);
        renderer = new AppRenderer(context,activity);
        setRenderer(renderer);
    }





//    @Override
//    public void onPause() {
//
//    }
//
//
//    @Override
//    public void onResume() {
//
//    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {







        if(Globalne.Menu_Flaga==2)//gameover
        {
            int actionId = event.getActionIndex();


            float x = event.getX(actionId);
            float y = event.getY(actionId);
            x = x / (renderer.hero.with/800) ;
            y = y / (renderer.hero.high/480) ;

            switch(event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                {
                    if(x>192.0f && y>82.0f && x<703.0f && y<282.0f)//new game
                    {
                        if(gui2d.survivemodeflag==1)
                        {
                            Globalne.Menu_Flaga = 1;
                        }
                        else {
                            Globalne.Menu_Flaga = 3;
                            int last_openlvl = -2;
//                            for (int i = 0; i < 24; i++)
//                                if (renderer.Gui2d.levelStateInt[i] == 1) last_openlvl++;
//                            renderer.Gui2d.scrollMap = -(2.0f * last_openlvl + 2.0f) * 100.0f;
                        }
                    }
                    break;
                }
            }

        }








        if(Globalne.Menu_Flaga==1)//menu
        {
            int actionId = event.getActionIndex();


            float x = event.getX(actionId);
            float y = event.getY(actionId);
            x = x / (renderer.hero.with/800) ;
            y = y / (renderer.hero.high/480) ;

            switch(event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                {
                    if(x<300.0f && y<143.0f)//new game
                    {


                        //renderer.reset();
                        Globalne.Menu_Flaga=3;
                        //renderer.mPlayer.start();
                        //int last_openlvl=-2;
//                        for(int i=0;i<24;i++)
//                            if(renderer.Gui2d.levelStateInt[i]==1)last_openlvl++;
//                        renderer.Gui2d.scrollMap = -(2.0f*last_openlvl+2.0f)*100.0f;
                    }

                    if(x<300.0f && y<253.0f && y>146.0f)//get premium
                    {}

                    if(x<300.0f && y<253.0f && y>146.0f)//survi
                    {

                            renderer.reset();
                            gui2d.survivemodeflag=1;
                            renderer.load("survive.map");
                            renderer.mL = (MyInterstitialListener)activity;
                            renderer.mL.callback();
                            //renderer.Gui2d.levelState[1]=0.0f;
                            Globalne.Menu_Flaga=0;//gra
                            gui2d.actualLevel=1;
                            tekstury.loadlandscape(0);
                            renderer.Bullet.ShotingId=-1;

                            preferences = appContext.getSharedPreferences("danesharedpref", Activity.MODE_PRIVATE);
                            renderer.Menager.reckord_kill = preferences.getInt("reckord", 0);
                    }


                    if(x>520.0f && x<700.0f && y<260.0f && y>150.0f)//rate
                    {
                        Uri uri = Uri.parse("market://details?id=" + appContext.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        // To count with Play market backstack, After pressing back button,
                        // to taken back to our application, we need to add following flags to intent.
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            appContext.startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            appContext.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=com.lukasz.zombieshooter" + appContext.getPackageName())));
                        }
                    }

                    if(x>520.0f && x<700.0f && y<140.0f && y>30.0f)//share
                    {
                        String message = "Look this Android shooter game, https://play.google.com/store/apps/details?id=com.lukasz.zombieshooter";
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("text/plain");
                        share.putExtra(Intent.EXTRA_TEXT, message);

                        appContext.startActivity(Intent.createChooser(share, "Share dead zombie head"));
                    }

                    if(x>32.0f && x<260.0f && y<350.0f && y>255.0f)//multiplayer
                    {
                        renderer.Blutacz.init();
                        Globalne.Menu_Flaga=4;
                        renderer.Gui2d.buttonsLag=1.0f;
//                            renderer.reset();
//                            renderer.Gui2d.survivemodeflag=1;
//                            renderer.load("survive.map");
//                            //renderer.Gui2d.levelState[1]=0.0f;
//                            renderer.Gui2d.Menu_Flaga=0;//gra
//                            renderer.Gui2d.actualLevel=1;
//                            tekstury.loadlandscape(0);
//                            renderer.Bullet.ShotingId=-1;
//
//                            preferences = appContext.getSharedPreferences("danesharedpref", Activity.MODE_PRIVATE);
//                            renderer.Menager.reckord_kill = preferences.getInt("reckord", 0);

                    }

                    if(x<245.0f && y>371.0f)//exit
                        android.os.Process.killProcess(android.os.Process.myPid());
                    break;
                }
            }

        }


        if(Globalne.Menu_Flaga==4)//menu
        {
            int actionId = event.getActionIndex();


            float x = event.getX(actionId);
            float y = event.getY(actionId);
            x = x / (renderer.hero.with/800) ;
            y = y / (renderer.hero.high/480) ;

            switch(event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN: {
                    if(x>110.0f && x<350.0f && y<340.0f && y>140.0f && renderer.Gui2d.buttonsLag<0.5f)//create game
                    {
                        renderer.Blutacz.Create();
                        //renderer.Blutacz.Join();
                        tekstury.secNdPlayer_flag=1;
                        renderer.reset();
                        renderer.load("multimap.map");
                        Globalne.Menu_Flaga=0;
                        //renderer.Gui2d.levelState[2]=0.0f;
                        gui2d.actualLevel=20;
                        tekstury.loadlandscape(1);
                        renderer.Bullet.ShotingId=-1;
                    }
                    if(x>415.0f && x<630.0f && y<340.0f && y>140.0f)//join game
                    {
                        //renderer.Blutacz.Create();
                        renderer.Blutacz.Join();
                        tekstury.secNdPlayer_flag=2;
                        renderer.reset();
                        renderer.load("multimap.map");
                        Globalne.Menu_Flaga=0;
                        //renderer.Gui2d.levelState[2]=0.0f;
                        gui2d.actualLevel=20;
                        tekstury.loadlandscape(1);
                        renderer.Bullet.ShotingId=-1;
                    }
                }
            }

        }







//        if(renderer.Globalne.Menu_Flaga==0 && renderer.rope.its==1)
//        {
//            int actionId = event.getActionIndex();
//
//
//            float x = event.getX(actionId);
//            float y = event.getY(actionId);
//            x = x / (renderer.hero.with/800) ;
//            y = y / (renderer.hero.high/480) ;
//
//            switch(event.getActionMasked()) {
//                case MotionEvent.ACTION_MOVE:
//                {
//                    if( (x>111.0f && x<205.0f && y>378.0f) || (x<118.0f && y>362) || (x>202.0f && x<301.0f && y>362.0f) || (x>110.0f && x<208.0f && y>275.0f && y<370.0f) || (x>655.0f && y<110.0f))
//                    {
//                        renderer.hero.MovePoint.x=x;
//                        renderer.hero.MovePoint.y=y;
//                        renderer.hero.MoveId=actionId;
//                        if(renderer.hero.MoveId==renderer.Bullet.ShotingId)
//                            renderer.Bullet.ShotingId=-1;
//                    }
//                    else
//                    {
//                        renderer.Bullet.ShottingPoint.x=x;
//                        renderer.Bullet.ShottingPoint.y=y;
//                        renderer.Bullet.ShotingId=actionId;
//                        if(renderer.hero.MoveId==renderer.Bullet.ShotingId)
//                            renderer.hero.MoveId=-1;
//                    }
//                    break;
//                }
//
//                case MotionEvent.ACTION_DOWN:
//                case MotionEvent.ACTION_POINTER_DOWN:
//                {
//                    if( (x>111.0f && x<205.0f && y>378.0f) || (x<118.0f && y>362) || (x>202.0f && x<301.0f && y>362.0f) || (x>110.0f && x<208.0f && y>275.0f && y<370.0f) || (x>655.0f && y<110.0f))
//                    {
//                        renderer.hero.MovePoint.x=x;
//                        renderer.hero.MovePoint.y=y;
//                        renderer.hero.MoveId=actionId;
//                    }
//                    else
//                    {
//                        renderer.Bullet.ShottingPoint.x=x;
//                        renderer.Bullet.ShottingPoint.y=y;
//                        renderer.Bullet.ShotingId=actionId;
//                    }
//                    break;
//                }
//                case MotionEvent.ACTION_UP:
//                case MotionEvent.ACTION_POINTER_UP:
//                {
//                    if( (x>111.0f && x<205.0f && y>378.0f) || (x<118.0f && y>362) || (x>202.0f && x<301.0f && y>362.0f) || (x>110.0f && x<208.0f && y>275.0f && y<370.0f) || (x>655.0f && y<110.0f))
//                    {
//                        renderer.hero.MoveId=-1;
//                    }
//                    else
//                    {
//                        renderer.Bullet.ShotingId=-1;
//                        renderer.rope.ropeDelay=0;
//                        if(renderer.hero.bron==2)
//                            renderer.Bullet.addArrow=1;
//                    }
//                    break;
//                }
//            }
//
//            renderer.dotykX[actionId]=x;
//            renderer.dotykY[actionId]=y;
//        }









        if(Globalne.Menu_Flaga==3)//wybor lvl
        {
            int actionId = event.getActionIndex();


            float x = event.getX(actionId);
            float y = event.getY(actionId);
            x = x / (renderer.hero.with/800) ;
            y = y / (renderer.hero.high/480) ;

            switch(event.getActionMasked()) {
                case MotionEvent.ACTION_MOVE: {
                    renderer.Gui2d.scrollMap=renderer.Gui2d.scrollMapold+x-renderer.Gui2d.scrollMaptmp;
//                    renderer.Gui2d.downTouch=0;
//                    renderer.Gui2d.upTouch=0;
                }break;
                case MotionEvent.ACTION_DOWN:
                {
                    renderer.Gui2d.downTouch=1;
                    //renderer.Gui2d.upTouch=0;

                    renderer.Gui2d.scrollMaptmp=x;
                    renderer.Gui2d.scrollMapold=renderer.Gui2d.scrollMap;

                    renderer.Gui2d.ShottingPoint.x=x;
                    renderer.Gui2d.ShottingPoint.y=y;
                }break;
                case MotionEvent.ACTION_UP:
                {
                    int indextouch=-1;

                    for (int i = 0; i < 24; i++)
                        if (Math.abs(renderer.Gui2d.touchPos3d.x - 2.0f * i) < 1.0f)
                            indextouch=i;

                        if(indextouch==renderer.Gui2d.touchIndexTmp)
                            renderer.Gui2d.upTouch=1;
                    //renderer.Gui2d.downTouch=0;

                    renderer.Gui2d.scrollMapold=renderer.Gui2d.scrollMap;
                    renderer.Gui2d.ShottingPoint.x=x;
                    renderer.Gui2d.ShottingPoint.y=y;
                }
            }
        }



        if(Globalne.Menu_Flaga==0 && renderer.rope.its==0)
        {
            int actionId = event.getActionIndex();


            float x = event.getX(actionId);
            float y = event.getY(actionId);
            x = x / (renderer.hero.with/800) ;
            y = y / (renderer.hero.high/480) ;

            switch(event.getActionMasked()) {
                case MotionEvent.ACTION_MOVE:
                {
                    if((x<118.0f && y>362) || (x>202.0f && x<301.0f && y>362.0f) || (x>110.0f && x<208.0f && y>275.0f && y<370.0f) || (x>655.0f && y<110.0f && tekstury.secNdPlayer_flag<1))
                    {
                        renderer.hero.MovePoint.x=x;
                        renderer.hero.MovePoint.y=y;
                        renderer.hero.MoveId=actionId;
                        if(renderer.hero.MoveId==renderer.Bullet.ShotingId)
                            renderer.Bullet.ShotingId=-1;
                    }
                    else
                    {
                        renderer.Bullet.ShottingPoint.x=x;
                        renderer.Bullet.ShottingPoint.y=y;
                        renderer.Bullet.ShotingId=actionId;
                        if(renderer.hero.MoveId==renderer.Bullet.ShotingId)
                            renderer.hero.MoveId=-1;
                    }
                    break;
                }

                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                {
                    if((x<118.0f && y>362) || (x>202.0f && x<301.0f && y>362.0f) || (x>110.0f && x<208.0f && y>275.0f && y<370.0f) || (x>655.0f && y<110.0f && tekstury.secNdPlayer_flag<1))
                    {
                        renderer.hero.MovePoint.x=x;
                        renderer.hero.MovePoint.y=y;
                        renderer.hero.MoveId=actionId;
                    }
                    else
                    {
                        renderer.Bullet.ShottingPoint.x=x;
                        renderer.Bullet.ShottingPoint.y=y;
                        renderer.Bullet.ShotingId=actionId;
                    }
                    break;
                }
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                {
                    if((x<118.0f && y>362) || (x>202.0f && x<301.0f && y>362.0f) || (x>110.0f && x<208.0f && y>275.0f && y<370.0f) || (x>655.0f && y<110.0f && tekstury.secNdPlayer_flag<1) )
                    {
                        renderer.hero.MoveId=-1;


                        if(x>655.0f && y<110.0f && tekstury.secNdPlayer_flag<1) {
                            if (tekstury.bulletTime < 0.5f)
                                tekstury.bulletTime = 1.0f;
                            else
                                tekstury.bulletTime = 0.25f;
                        }
                    }
                    else
                    {
                        renderer.Bullet.ShotingId=-1;
                        renderer.rope.ropeDelay=0;
                        if(renderer.hero.bron==2)
                            renderer.Bullet.addArrow=1;
                    }
                    break;
                }
            }

            renderer.dotykX[actionId]=x;
            renderer.dotykY[actionId]=y;
        }











        return true;
    }
















}
