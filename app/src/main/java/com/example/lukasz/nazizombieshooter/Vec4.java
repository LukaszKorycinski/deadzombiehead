package com.example.lukasz.nazizombieshooter;

public class Vec4 {
    float x,y,z,w;

    Vec4()
    {
        x=0.0f;
        y=0.0f;
        z=0.0f;
    }



    Vec4(float a, float b, float c, float d)
    {
        x=a;
        y=b;
        z=c;
        w=d;
    }


    public void set(Vec4 vec) {
        x=vec.x;
        y=vec.y;
        z=vec.z;
        w=vec.w;
    }





    void set(float a, float b, float c, float d)
    {
        x=a;
        y=b;
        z=c;
        w=d;
    }


    void normalize()
    {
        float l=(float) Math.sqrt(   x*x + y*y + z*z + w*w  )	;

        x=x/l;
        y=y/l;
        z=z/l;
        w=w/l;
    }

}
