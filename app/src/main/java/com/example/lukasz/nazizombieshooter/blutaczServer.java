package com.example.lukasz.nazizombieshooter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.UUID;

/**
 * Created by Lukasz on 2016-08-16.
 */
public class blutaczServer extends Thread {

public  BluetoothServerSocket mmServerSocket;
int state;

    public blutaczServer() {
        state=0;
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        BluetoothServerSocket tmp = null;
        try {
            UUID uuid=UUID.fromString("550e8400-e29b-41d4-a716-446677880000");
            //tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord("Server", uuid);
            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord("Server", uuid);
        } catch (IOException e) { }
        mmServerSocket = tmp;
    }



    BluetoothSocket socket = null;
    PrintWriter out;
    BufferedReader in;

    public void run() {
       // Log.d("INFO","Uruchamiam serwer");

        while (true) {
            try {
                if(state==0) {
                    //Log.d("INFO", "Czekam na polaczenie od clienta");
                    socket = mmServerSocket.accept();
                    BluetoothDevice devicetmp =socket.getRemoteDevice();



                    //Log.d("INFO", "Mam clienta!");
                    state=1;
                    out = new PrintWriter(socket.getOutputStream(),true);
                    //Log.d("INFO", "utworzono strumien OUT serwera state = " + state);


                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    tekstury.komunikat="connected with " + devicetmp.getName();
                   // Log.d("INFO", "utworzono strumien IN serwera state = " + state);


                }

                if(state==1 && tekstury.synhronizedBlueThread==1) {
                    out.println(tekstury.HeroPosStatic.x + " " + tekstury.HeroPosStatic.y + " " + tekstury.HeroAngleStatic + " " + tekstury.HeroreloadStatic + " " + tekstury.HeroAnimStatic + " "
                            + tekstury.HeroShootingStatic + " " + tekstury.HeroShotStatic.x + " " + tekstury.HeroShotStatic.y + " " + tekstury.hitPlayerEnemy + " " + tekstury.hpMulti + " " + tekstury.HeroFlagaStatic);
                    //Log.d("INFO", "Wyslalem dane");

                    String input = in.readLine();
                    //Log.d("INFO", "Odbieram dane");
                    String delims = " ";
                    String[] tokens = input.split(delims);
                    tekstury.NdPlayerPosStatic.x = Float.parseFloat(tokens[0]);
                    tekstury.NdPlayerPosStatic.y = Float.parseFloat(tokens[1]);
                    tekstury.NdPlayerAngleStatic = Float.parseFloat(tokens[2]);
                    //tekstury.NdPlayerBronStatic = Integer.parseInt(tokens[3]);
                    tekstury.NdPlayerreloadStatic = Float.parseFloat(tokens[3]);
                    tekstury.NdPlayerAnimStatic = Float.parseFloat(tokens[4]);
                    tekstury.NdPlayerShootingStatic = Integer.parseInt(tokens[5]);
                    tekstury.NdPlayerShotStatic.x = Float.parseFloat(tokens[6]);
                    tekstury.NdPlayerShotStatic.y = Float.parseFloat(tokens[7]);
                    tekstury.ndhitPlayerEnemy = Integer.parseInt(tokens[8]);
                    tekstury.ndhpMulti = Integer.parseInt(tokens[9]);
                    tekstury.NdPlayerFlagaStatic = Integer.parseInt(tokens[10]);

                    tekstury.synhronizedBlueThread=0;
                }
                //out.println(tekstury.HeroPosStatic.y);
                //out.close();
                // BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                break;
            }
//            if (socket != null) {
//               /*
//                *
//                * */
//                try {
//                    mmServerSocket.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
//            }
        }
    }

        //if(state==0) {
//        while (true) {
//            Log.d("INFO", "Uruchamiam serwer");
//
//                Log.d("INFO", "Czekam na poczenie od clienta");
//                try {
//                    socket = mmServerSocket.accept();
//                } catch (IOException e) {e.printStackTrace();}
//                Log.d("INFO", "Mam clienta!");
//            //}
//
//
//            //if()
//
//
//                    try {
//                        PrintWriter out = new PrintWriter(socket.getOutputStream(),true);
//                        if(socket != null)
//                            out.println(tekstury.HeroPosStatic.x);
//                    } catch (IOException e) {e.printStackTrace();}
//                }
//        }


}
