package com.example.lukasz.nazizombieshooter;

import java.util.Random;

//import org.jbox2d.common.Vec2;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class Bang {
	int ile;
	public static int ILE = 4;

	float wave[];
	float wave2[];
	Vec2 vec[];
	Vec2 pos[];



	
	define Define=new define();
	
	f3ds F3ds;
	matrixInv MatrixInv = new matrixInv();
	
	
void reset()
{
for(int i=0;i<ILE;i++) {
	wave[i] = -1.0f;
	wave2[i] = -1.0f;
}
ile=0;
}
	
	

	
Bang(Context aC)
{

	wave=new float[ILE];
	wave2=new float[ILE];
	vec=new Vec2[ILE];
	
	pos=new Vec2[ILE];


	define Define=new define();
	matrixInv MatrixInv = new matrixInv();

	ile=0;
	F3ds  = new f3ds(aC);	

	for(int i=0;i<ILE;i++)
	{
		wave[i]=-1.0f;
		wave2[i]=-1.0f;
		vec[i]=new Vec2();
		pos[i]=new Vec2();
	}
}
	
	
void add(float x, float y)
{
	Random random=new Random();


	pos[ile].set(x,y);

	vec[ile].set(random.nextFloat(),random.nextFloat());
	vec[ile].normalize();
	vec[ile].set(vec[ile].x - 0.5f, vec[ile].y - 0.5f);
	vec[ile].set(vec[ile].x*0.7f, vec[ile].y*0.05f);

	wave[ile]=5.0f;
	wave2[ile]=5.0f;
	ile++;
	if(ile>ILE-1)
		ile=0;
}



void Logic(int i, float loop)
{
wave[i]=wave[i]-0.05f*loop;
wave2[i]=wave2[i]-0.025f*loop;
}


	float waveFunc(float low, float hi, int i)
	{

		float w=wave[i]-low ;

		w=w*(1.0f/(hi-low));

		if(w>1.0f || w<0.0f)w=0.0f;
		return w;
	}

public void Draw(int ShaderProgram, float loop)
{
	loop=loop*tekstury.bulletTime;
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");


	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[19]);


	for(int i=0;i<ile;i++)
	{

		Logic(i, loop);
		//Matrix.scaleM(tmpMatrix, 0, 1.3f * (1.5f - wave[i]), 1.3f * (1.5f - wave[i]), 1.0f);

		float wavetmp = waveFunc(2.0f, 4.7f, i);
		Matrix.setIdentityM(tmpMatrix, 0);
		GLES20.glUniform1f(iWaveVS, wavetmp);
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
		Matrix.scaleM(tmpMatrix, 0, 1.0f - wavetmp + 0.7f, 1.0f - wavetmp + 0.7f, 1.0f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(25, ShaderProgram);//dym duzy jasny

		wavetmp = waveFunc(2.8f, 4.9f, i);
		Matrix.setIdentityM(tmpMatrix, 0);
		GLES20.glUniform1f(iWaveVS, wavetmp);wavetmp=wavetmp*0.7f;
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
		Matrix.scaleM(tmpMatrix, 0, 1.0f - wavetmp + 0.6f, 1.0f - wavetmp + 0.6f, 1.0f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(26, ShaderProgram);//dym jasny

		Matrix.setIdentityM(tmpMatrix, 0);
		GLES20.glUniform1f(iWaveVS, waveFunc(4.2f, 5.0f, i));
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(71, ShaderProgram);//lighting

		wavetmp = waveFunc(0.5f, 4.0f, i);
		Matrix.setIdentityM(tmpMatrix, 0);
		GLES20.glUniform1f(iWaveVS, wavetmp);
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y + 1.0f - waveFunc(0.5f, 4.0f, i), 0.0f);
		Matrix.scaleM(tmpMatrix, 0, 1.0f - wavetmp + 0.9f, 1.0f - wavetmp + 0.9f, 1.0f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(72, ShaderProgram);//dym ciemny duzy

		wavetmp = waveFunc(0.0f,3.7f,i);
		Matrix.setIdentityM(tmpMatrix, 0);
		GLES20.glUniform1f(iWaveVS, wavetmp);
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y + 2.0f - waveFunc(0.5f, 4.0f, i) * 2.0f, 0.0f);
		Matrix.scaleM(tmpMatrix, 0, 1.0f - wavetmp + 0.9f, 1.0f - wavetmp + 0.9f, 1.0f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(73, ShaderProgram );//dym ciemny duzy
	}

}
}



	
