package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class Spikes {
	
	
	 static int ILE_MODEL = 20;
	 int co[];
	 int chapeIndex[];
	 int tex[];
	 Vec2 pos[];
	 Vec2 shape[];
	 int ile;

	 matrixInv MatrixInv;
	 f3ds F3ds;
	 define Define;
	 
	 
	 void reset()
	 {
	 ile=0;	 
	 }
	 
	 
	 
	 Spikes(Context aC)
	 {
	 	 co=new int[ILE_MODEL];
	 	 chapeIndex=new int[ILE_MODEL];
	 	 tex=new int[ILE_MODEL];
	 	 pos=new Vec2[ILE_MODEL];
	 	 shape=new Vec2[2];
	 	
	 	
	 MatrixInv = new matrixInv();
	 Define = new define();


	 shape[0]=new Vec2(2.73f, 0.47f);
	 shape[1]=new Vec2(2.73f, 0.47f);//wolne

	 F3ds  = new f3ds(aC);
	 ile=0;


	 for(int i=0;i<ILE_MODEL;i++)
	 {
	 chapeIndex[i]=0;
	 co[i]=-1;
	 pos[i]=new Vec2();
	 }	

	 }

	 
	 
	 
	 public int colision(float x, float y, float sx, float sy)
	 {
	 //if( y-sy<-1.0f)
	 //	return 1;
	 	
	 for(int i=0;i<ile;i++)	
	 	{
	 	if( pos[i].x+shape[chapeIndex[i]].x > x-sx && pos[i].x-shape[chapeIndex[i]].x < x+sx 
	 			&&
	 		pos[i].y+shape[chapeIndex[i]].y > y-sy && pos[i].y-shape[chapeIndex[i]].y < y+sy )
	 		return 1;
	 	}	
	 	
	 return 0;	
	 }

	 
	 
	 void add(float x, float y, int c, int t, int si)
	 {
	 	tex[ile]=t;
	 	pos[ile].set(x,y);
	 	co[ile]=c;
	 	chapeIndex[ile]=si;
	 	ile++;
	 }
	 
	 
	 
	 
	 
	 
	 public void Draw(int ShaderProgram)
	 {
	 	float[] tmpMatrix = new float[16];
	 	
	 	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	 	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	 	

	 	
	 	//int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	 	//GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);
	 	
	 	for(int i=0;i<ile;i++)
	 		{
	 		Matrix.setIdentityM(tmpMatrix, 0);
	 		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
	 		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	 		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	 		
	 		
	 		Matrix.setIdentityM(tmpMatrix, 0);
	 		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
	 		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0); 
	 		
	 		
	 		
	 		
	 		
	 		int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture");
	 		GLES20.glUniform1i(texLoc, 0);
	 		
	 		//texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texshoot");
	 		//GLES20.glUniform1i(texLoc, 1);


	 			//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
	 			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[tex[i]]);
	 		
	 			//GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
	 			//GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.dynamicTexture[0]);//pole do optymalizacji nie potrzeba cieni do spikesow
	 			
	 			//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
	 		
	 		F3ds.DrawModel(co[i], ShaderProgram );
	 		}
	 }
	 
	 
	 
	 
	 
	 
	 
	 
}
