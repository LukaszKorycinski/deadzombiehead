package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

/**
 * Created by Lukasz on 2016-04-13.
 */
public class Barrel {
    static int ILE_MODEL = 20;
    Vec2 pos[];
    int co[];
    Vec2 shape[];
    int ile;
    matrixInv MatrixInv;
    f3ds F3ds;
    define Define;

    void reset()
    {
        ile=0;
    }


    Barrel(Context aC)
    {
        Define=new define();
        pos=new Vec2[ILE_MODEL];
        shape=new Vec2[2];
        co=new int[ILE_MODEL];

        MatrixInv = new matrixInv();

        shape[0]=new Vec2(0.45f, 0.5f);
        shape[1]=new Vec2(0.7f, 0.5f);


        F3ds  = new f3ds(aC);
        ile=0;

        for(int i=0;i<ILE_MODEL;i++)
        {
            pos[i]=new Vec2();
            co[i]=0;
        }
    }



    public int colision(float x, float y, float sx, float sy)
    {
        for(int i=0;i<ile;i++)
        {
            if( pos[i].x+shape[co[i]].x > x-sx && pos[i].x-shape[co[i]].x < x+sx
                    &&
                    pos[i].y+shape[co[i]].y > y-sy && pos[i].y-shape[co[i]].y < y+sy )
                return 1;
        }
        return 0;
    }








    public Vec2f1i colisionRay(Vec2 x, Vec2 y, float length)
    {
        Vec2f1i w=new Vec2f1i();
        w.set(10000.0f, 10000.0f, -1);




        float a1 = x.y-y.y;
        float a2 = x.x-y.x;
        float a =  a1/a2;
        float b = y.y-(a*y.x);

        //y=ax+b
        //x=(y-b)/a



        for(int i=0;i<ile;i++)
            if( Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length+Define.Length(shape[co[i]].x, shape[co[i]].y) )
            {
                float MinY = a*(pos[i].x-shape[co[i]].x)+b;
                float MaxY = a*(pos[i].x+shape[co[i]].x)+b;

                float MinX = ((pos[i].y-shape[co[i]].y)-b)/a;
                float MaxX = ((pos[i].y+shape[co[i]].y)-b)/a;



                if( shape[co[i]].y >= Math.abs(pos[i].y-MinY) )
                    if( Define.distanc( x.x, x.y, pos[i].x-shape[co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
                        if(  ((y.x-x.x > 0) == (pos[i].x-shape[co[i]].x-x.x > 0))  )
                            w.set( pos[i].x-shape[co[i]].x, MinY, i);

                if( shape[co[i]].y >= Math.abs(pos[i].y-MaxY) )
                    if( Define.distanc( x.x, x.y, pos[i].x+shape[co[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
                        if(  ((y.x-x.x > 0) == (pos[i].x+shape[co[i]].x-x.x > 0))  )
                            w.set( pos[i].x+shape[co[i]].x, MaxY, i);

                if( shape[co[i]].x >= Math.abs(pos[i].x-MinX) )
                    if( Define.distanc( x.x, x.y, MinX, pos[i].y-shape[co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
                        if(  ((y.y-x.y > 0) == (pos[i].y-shape[co[i]].y-x.y > 0))  )
                            w.set( MinX, pos[i].y-shape[co[i]].y, i);

                if( shape[co[i]].x >= Math.abs(pos[i].x-MaxX) )
                    if( Define.distanc( x.x, x.y, MaxX, pos[i].y+shape[co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
                        if(  ((y.y-x.y > 0) == (pos[i].y+shape[co[i]].y-x.y > 0))  )
                            w.set( MaxX, pos[i].y+shape[co[i]].y, i);
            }

        if( Define.distanc(x.x, x.y, w.x, w.y) < length )
            return w;
        else
            return new Vec2f1i(0.0f, 0.0f, -1);
    }









    public int colisionCircle(float x, float y, float r)
    {
        for(int i=0;i<ile;i++)
        {
            float l1=Math.abs(pos[i].y-y);
            float l2=Math.abs(pos[i].x-x);

            if( l1<r+shape[co[i]].y   &&  l2<r+shape[co[i]].x  )
                return i;
        }

        return -1;
    }




    void add(float x, float y, int c)
    {
        pos[ile].set(x,y);
        co[ile]=c;
        ile++;
    }

    void del(int j)
    {
        for(int i=j; i<ile-1; i++) {
            pos[i].set(pos[i + 1].x, pos[i + 1].y);
            co[i]=co[i+1];
        }
        ile--;
    }

    public void Draw(int ShaderProgram)
    {
        float[] tmpMatrix = new float[16];

        int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
        int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");



        //int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
        //GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);

        for(int i=0;i<ile;i++)
        {
            Matrix.setIdentityM(tmpMatrix, 0);
            Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
            Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
            GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


            Matrix.setIdentityM(tmpMatrix, 0);
            Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
            GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);


            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[31]);


            F3ds.DrawModel(70, ShaderProgram );
        }
    }


}
