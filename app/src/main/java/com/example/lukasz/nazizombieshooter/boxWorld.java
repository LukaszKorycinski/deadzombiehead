package com.example.lukasz.nazizombieshooter;

//import org.jbox2d.common.Vec2;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;




public class boxWorld {

	static int ILE_CORPS = 24;
	Vec2 pos[];
	Vec2 vec[];
	float angle[];
	float wave[];
	float waveAddBlood[];
	int co[];
	int ile;
	
	private static Context appContext;
	matrixInv MatrixInv;
	f3ds F3ds;

	
	
boxWorld(Context aC)
{
	pos=new Vec2[ILE_CORPS];
	vec=new Vec2[ILE_CORPS];
	angle=new float[ILE_CORPS];
	wave=new float[ILE_CORPS];
	waveAddBlood=new float[ILE_CORPS];
	co=new int[ILE_CORPS];

	MatrixInv = new matrixInv();

for(int i=0;i<ILE_CORPS;i++)
{
pos[i]=new Vec2();
vec[i]=new Vec2();

co[i]=-1;
angle[i]=0.0f;
wave[i]=0.0f;
waveAddBlood[i]=0.0f;
}
ile=0;
F3ds  = new f3ds(aC);
}


void reset()
{
	ile=0;
}


void add(float px, float py, float vx, float vy, int c)
{
	if(ile<23) {
		pos[ile].set(px, py);
		vec[ile].set(vx * 0.15f, vy * 0.15f);
		co[ile] = c;
		angle[ile] = 0.0f;
		wave[ile] = 1.0f;
		ile++;
	}
}


void del(int j)
{
for(int i=j; i<ile-1; i++)
	{
	pos[i].set(pos[i+1].x, pos[i+1].y);
	vec[i].set(vec[i+1].x, pos[i+1].y);
	co[i]=co[i+1];
	angle[i]=angle[i+1];
	wave[i]=wave[i+1];
	}
ile--;
}



void logic(float loop)
{
loop=loop*tekstury.bulletTime;

	for(int i=0;i<ile;i++)
	{
	vec[i].y-=0.002f*loop;
	waveAddBlood[i]+=0.1f*loop;
//		if(co[i]<10)
//	wave[i]-=0.007f*loop;
//		else
	wave[i]-=0.0045f*loop;

	angle[i]-=vec[i].x*100.0f*loop;
	
	if(wave[i]<0.2f)
		del(i);
	}
}


void Draw(int ShaderProgram)
{
	 float[] tmpMatrix = new float[16];
	 	
	 int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

	 	
	 for(int i=0;i<ile;i++)
	 {
	 Matrix.setIdentityM(tmpMatrix, 0);



	 Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);

		 if(co[i]==18)
			 Matrix.scaleM(tmpMatrix, 0, 2.0f, 2.0f, 2.0f);

	 Matrix.rotateM(tmpMatrix, 0, angle[i], 0, 0, 1);
	 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	 	
	 
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	GLES20.glUniform1f(iWaveVS, 1.0f - wave[i]);

		 int icolorFS = GLES20.glGetUniformLocation(ShaderProgram, "col");
		 if(co[i]<10)
			 GLES20.glUniform1f(icolorFS, 1.0f);
		 else
			 GLES20.glUniform1f(icolorFS, 0.0f);


	int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "tex");
	GLES20.glUniform1i(texLoc, 0);
	
	texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texalfa");
	GLES20.glUniform1i(texLoc, 1);

	if(co[i]==4 || co[i]==5 || co[i]==6 || co[i]==7)
		{
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[23]);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[24]);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		}
	else
		{
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[1]);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[16]);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		}

		 if(co[i]>9 && co[i]<14)
		 {
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[18]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[9]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		 }

		 if(co[i]>13 && co[i]<16)
		 {
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[28]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[9]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		 }


		 if(co[i]>15 && co[i]<24)
		 {
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[21]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[9]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		 }

		 if(co[i]>23 && co[i]<26)
		 {
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[18]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[9]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		 }

		 if(co[i]>25 && co[i]<28)
		 {
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[28]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[16]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		 }

		 if(co[i]>27)
		 {
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[26]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			 GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[16]);
			 GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		 }
	 	
	if(co[i]==0)
		 F3ds.DrawModel(20, ShaderProgram );
	if(co[i]==1)
		 F3ds.DrawModel(21, ShaderProgram );
	if(co[i]==2)
		 F3ds.DrawModel(24, ShaderProgram );
	if(co[i]==3)
		 F3ds.DrawModel(27, ShaderProgram );
	if(co[i]==4)
		 F3ds.DrawModel(36, ShaderProgram );
	if(co[i]==5)
		 F3ds.DrawModel(37, ShaderProgram );
	if(co[i]==6)
		 F3ds.DrawModel(38, ShaderProgram );
	if(co[i]==7)
		 F3ds.DrawModel(39, ShaderProgram );
	if(co[i]==8)
		 F3ds.DrawModel(41, ShaderProgram );
	if(co[i]==9)
		 F3ds.DrawModel(42, ShaderProgram );
		 if(co[i]==10)
			 F3ds.DrawModel(64, ShaderProgram );
		 if(co[i]==11)
			 F3ds.DrawModel(65, ShaderProgram );
		 if(co[i]==12)
			 F3ds.DrawModel(66, ShaderProgram );
		 if(co[i]==13)
			 F3ds.DrawModel(67, ShaderProgram );

		 if(co[i]==14)
			 F3ds.DrawModel(88, ShaderProgram );
		 if(co[i]==15)
			 F3ds.DrawModel(89, ShaderProgram );

		 if(co[i]==16)
			 F3ds.DrawModel(91, ShaderProgram );
		 if(co[i]==17)
			 F3ds.DrawModel(92, ShaderProgram );

		 if(co[i]==18)
			 F3ds.DrawModel(93, ShaderProgram );

		 if(co[i]==19)
			 F3ds.DrawModel(94, ShaderProgram );
		 if(co[i]==20)
			 F3ds.DrawModel(95, ShaderProgram );
		 if(co[i]==21)
			 F3ds.DrawModel(96, ShaderProgram );

		 if(co[i]==22)
			 F3ds.DrawModel(97, ShaderProgram );
		 if(co[i]==23)
			 F3ds.DrawModel(98, ShaderProgram );

		 if(co[i]==24)
			 F3ds.DrawModel(36, ShaderProgram );
		 if(co[i]==25)
			 F3ds.DrawModel(37, ShaderProgram );

		 if(co[i]==26)
			 F3ds.DrawModel(103, ShaderProgram );
		 if(co[i]==27)
			 F3ds.DrawModel(104, ShaderProgram );



		 if(co[i]==28)
			 F3ds.DrawModel(105, ShaderProgram );

	 }
}

	
	
}
