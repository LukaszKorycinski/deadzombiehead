package com.example.lukasz.nazizombieshooter;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class fontInv {
	private static Context appContext;
	private  FloatBuffer texBuffer[];
	private  FloatBuffer vertexBuffer;
	matrixInv MatrixInv;
    
public fontInv(Context context)
{
	appContext = context;
	texBuffer=new FloatBuffer[42];
	MatrixInv = new matrixInv();
}
	
	
//	public static void main(String[] args)
//	{
//	int liczba = 44;
//	tekst("blablabla "+liczba);
//	}   
//		
//		
//	static void tekst(String t)
//	{
//	System.out.println(t);
//	}
		
void init()//tworzymy atlas 36 quadow z texkordami.
{ 
	float size=0.25f; float zz=0.0f;
	float[] vertex_data = {
			-size, -size, zz,
			 size, -size, zz,
			-size,  size, zz,
			-size,  size, zz,
			 size, -size, zz,
			 size,  size, zz
			};

	
    ByteBuffer vbb = ByteBuffer.allocateDirect(vertex_data.length * 4); 
    vbb.order(ByteOrder.nativeOrder());
    vertexBuffer = vbb.asFloatBuffer();
    vertexBuffer.put(vertex_data);
    vertexBuffer.position(0);
    
  
    
  float LS=0.125f;int mark=0; int markY=0;
  for(int i=0;i<42;i++)
  {
    
	  
	float[] texData = {
			LS*mark,    LS+LS*markY,
			LS+LS*mark, LS+LS*markY,
			LS*mark,    LS*markY,
			LS*mark,    LS*markY,
			LS+LS*mark, LS+LS*markY,
			LS+LS*mark, LS*markY
			};
	mark++;if(mark>7){mark=0;markY++;}
	
    ByteBuffer tbb = ByteBuffer.allocateDirect(texData.length * 4); 
    tbb.order(ByteOrder.nativeOrder());
    texBuffer[i] = tbb.asFloatBuffer();
    texBuffer[i].put(texData);
    texBuffer[i].position(0);
  }
    
}
	
	



void DrawText(int ShaderProgram, int t, String tekst)//rysujemy 
{
	//97 to 122 litery
	//32 spacja
	//48 to 57 cyfry
	
	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t);
	
	float[] tmpMatrix = new float[16];
	
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");


for(int i=0;i<tekst.length();i++)
	{
		int indexLitery=37;
		if(  (int)tekst.charAt(i) > 47 && (int)tekst.charAt(i) < 58 )
			indexLitery=(int)tekst.charAt(i)-48;

		if(  (int)tekst.charAt(i) > 64 && (int)tekst.charAt(i) < 91 )
			indexLitery=(int)tekst.charAt(i)-54;

		if(  (int)tekst.charAt(i) > 96 && (int)tekst.charAt(i) < 123 )// '9' == 36
			indexLitery=(int)tekst.charAt(i)-86;
		if(  (int)tekst.charAt(i) ==32 )
			indexLitery=10;

		if((int)tekst.charAt(i) == 40)// (
			indexLitery=39;
		if((int)tekst.charAt(i) == 41)// )
			indexLitery=40;
		if((int)tekst.charAt(i) == 47)// /
			indexLitery=38;
		if((int)tekst.charAt(i) == 58)// :
			indexLitery=41;

//		if((int)tekst.charAt(i) > 96 && (int)tekst.charAt(i) < 123)// male litery
//			indexLitery=(int)tekst.charAt(i)-86;

	Matrix.setIdentityM(tmpMatrix, 0);


	int ii=i%46;
	int jj=i/46;
	Matrix.translateM(tmpMatrix, 0, -8.0f +  0.35f*ii, 4.2f-0.4f*jj, -6.5f);

	
	//Matrix.multiplyMM(tmpMatrix, 0, ViewProjMatrix, 0, tmpMatrix, 0);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	

	int mPositionHandle = GLES20.glGetAttribLocation(ShaderProgram, "vPosition");
	GLES20.glEnableVertexAttribArray(mPositionHandle);
	
	int mTexCoordHandle = GLES20.glGetAttribLocation(ShaderProgram, "texcoord");
	GLES20.glEnableVertexAttribArray(mTexCoordHandle);
	
	
	GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, texBuffer[indexLitery]); // texcoords_data is a float*, 2 per vertex, representing UV coordinates.
	GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer); // vertex_data is a float*, 3 per vertex, representing the position of each vertex
	
	
	GLES20.glDrawArrays( GLES20.GL_TRIANGLES, 0, 6 );
	
	
	GLES20.glDisableVertexAttribArray(mPositionHandle);//pole do optymalizacji
	GLES20.glDisableVertexAttribArray(mTexCoordHandle);//pole do optymalizacji
	}

}




	void DrawTextnd(int ShaderProgram, int t, String tekst)//rysujemy
	{
		//97 to 122 litery
		//32 spacja
		//48 to 57 cyfry

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t);

		float[] tmpMatrix = new float[16];


		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");


		for(int i=0;i<tekst.length();i++)
		{
			int indexLitery=37;
			if(  (int)tekst.charAt(i) > 47 && (int)tekst.charAt(i) < 58 )
				indexLitery=(int)tekst.charAt(i)-48;

			if(  (int)tekst.charAt(i) > 64 && (int)tekst.charAt(i) < 91 )
				indexLitery=(int)tekst.charAt(i)-54;

			if(  (int)tekst.charAt(i) > 96 && (int)tekst.charAt(i) < 123 )// '9' == 36
				indexLitery=(int)tekst.charAt(i)-86;
			if(  (int)tekst.charAt(i) ==32 )
				indexLitery=10;

			if((int)tekst.charAt(i) == 40)// (
				indexLitery=39;
			if((int)tekst.charAt(i) == 41)// )
				indexLitery=40;
			if((int)tekst.charAt(i) == 47)// /
				indexLitery=38;
			if((int)tekst.charAt(i) == 58)// :
				indexLitery=41;

//		if((int)tekst.charAt(i) > 96 && (int)tekst.charAt(i) < 123)// male litery
//			indexLitery=(int)tekst.charAt(i)-86;

			Matrix.setIdentityM(tmpMatrix, 0);


			int ii=i%46;
			int jj=i/46;
			Matrix.translateM(tmpMatrix, 0, -8.0f +  0.35f*ii, 3.8f-0.4f*jj, -6.5f);


			//Matrix.multiplyMM(tmpMatrix, 0, ViewProjMatrix, 0, tmpMatrix, 0);
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


			int mPositionHandle = GLES20.glGetAttribLocation(ShaderProgram, "vPosition");
			GLES20.glEnableVertexAttribArray(mPositionHandle);

			int mTexCoordHandle = GLES20.glGetAttribLocation(ShaderProgram, "texcoord");
			GLES20.glEnableVertexAttribArray(mTexCoordHandle);


			GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, texBuffer[indexLitery]); // texcoords_data is a float*, 2 per vertex, representing UV coordinates.
			GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer); // vertex_data is a float*, 3 per vertex, representing the position of each vertex


			GLES20.glDrawArrays( GLES20.GL_TRIANGLES, 0, 6 );


			GLES20.glDisableVertexAttribArray(mPositionHandle);//pole do optymalizacji
			GLES20.glDisableVertexAttribArray(mTexCoordHandle);//pole do optymalizacji
		}

	}



	void DrawText3D(Vec2 pos, float scale, int ShaderProgram,  int t, String tekst)//rysujemy
	{
		//97 to 122 litery
		//32 spacja
		//48 to 57 cyfry

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t);

		float[] tmpMatrix = new float[16];


		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

		for(int i=0;i<tekst.length();i++)
		{
			int indexLitery=37;
			if(  (int)tekst.charAt(i) > 47 && (int)tekst.charAt(i) < 58 )
				indexLitery=(int)tekst.charAt(i)-48;
			if(  (int)tekst.charAt(i) > 64 && (int)tekst.charAt(i) < 91 )
				indexLitery=(int)tekst.charAt(i)-54;
			if(  (int)tekst.charAt(i) > 96 && (int)tekst.charAt(i) < 123 )// '9' == 36
				indexLitery=(int)tekst.charAt(i)-86;
			if(  (int)tekst.charAt(i) ==32 )
				indexLitery=10;

			if((int)tekst.charAt(i) == 40)// (
				indexLitery=39;
			if((int)tekst.charAt(i) == 41)// )
				indexLitery=40;
			if((int)tekst.charAt(i) == 47)// /
				indexLitery=38;
			if((int)tekst.charAt(i) == 58)// :
				indexLitery=41;

			Matrix.setIdentityM(tmpMatrix, 0);


			int ii=i%8;
			int jj=i/8;
			Matrix.translateM(tmpMatrix, 0, pos.x+0.175f*ii, pos.y-0.4f*jj, 0.1f);
			Matrix.scaleM(tmpMatrix, 0, scale, scale, scale);

			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			//Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


			int mPositionHandle = GLES20.glGetAttribLocation(ShaderProgram, "vPosition");
			GLES20.glEnableVertexAttribArray(mPositionHandle);

			int mTexCoordHandle = GLES20.glGetAttribLocation(ShaderProgram, "texcoord");
			GLES20.glEnableVertexAttribArray(mTexCoordHandle);


			GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, texBuffer[indexLitery]); // texcoords_data is a float*, 2 per vertex, representing UV coordinates.
			GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer); // vertex_data is a float*, 3 per vertex, representing the position of each vertex


			GLES20.glDrawArrays( GLES20.GL_TRIANGLES, 0, 6 );


			GLES20.glDisableVertexAttribArray(mPositionHandle);//pole do optymalizacji
			GLES20.glDisableVertexAttribArray(mTexCoordHandle);//pole do optymalizacji
		}
	}












	void DrawText3DBig(Vec2 pos, float scale, int ShaderProgram,  int t, String tekst)//rysujemy
	{
		//97 to 122 litery
		//32 spacja
		//48 to 57 cyfry

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t);

		float[] tmpMatrix = new float[16];


		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

		for(int i=0;i<tekst.length();i++)
		{
			int indexLitery=37;
			if(  (int)tekst.charAt(i) > 47 && (int)tekst.charAt(i) < 58 )
				indexLitery=(int)tekst.charAt(i)-48;
			if(  (int)tekst.charAt(i) > 96 && (int)tekst.charAt(i) < 123 )// '9' == 36
				indexLitery=(int)tekst.charAt(i)-86;
			if(  (int)tekst.charAt(i) ==32 )
				indexLitery=10;

			if((int)tekst.charAt(i)==42)//gwiazdka koniec wiersza
				indexLitery=10;

			if((int)tekst.charAt(i) == 40)// (
				indexLitery=39;
			if((int)tekst.charAt(i) == 41)// )
				indexLitery=40;
			if((int)tekst.charAt(i) == 47)// /
				indexLitery=38;
			if((int)tekst.charAt(i) == 58)// :
				indexLitery=41;

			if(indexLitery!=10) {//pomijanie spacji
				Matrix.setIdentityM(tmpMatrix, 0);
				int ii = i % 13;
				int jj = i / 13;
				Matrix.translateM(tmpMatrix, 0, pos.x + 0.175f * ii, pos.y - 0.4f * jj, 0.1f);
				Matrix.scaleM(tmpMatrix, 0, scale, scale, scale);

				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				//Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


				int mPositionHandle = GLES20.glGetAttribLocation(ShaderProgram, "vPosition");
				GLES20.glEnableVertexAttribArray(mPositionHandle);

				int mTexCoordHandle = GLES20.glGetAttribLocation(ShaderProgram, "texcoord");
				GLES20.glEnableVertexAttribArray(mTexCoordHandle);


				GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, texBuffer[indexLitery]); // texcoords_data is a float*, 2 per vertex, representing UV coordinates.
				GLES20.glVertexAttribPointer(mPositionHandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer); // vertex_data is a float*, 3 per vertex, representing the position of each vertex


				GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6);


				GLES20.glDisableVertexAttribArray(mPositionHandle);//pole do optymalizacji
				GLES20.glDisableVertexAttribArray(mTexCoordHandle);//pole do optymalizacji
			}
		}

	}









}
