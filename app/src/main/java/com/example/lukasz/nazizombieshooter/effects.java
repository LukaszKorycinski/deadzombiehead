package com.example.lukasz.nazizombieshooter;

import java.util.Random;

//import org.jbox2d.common.Vec2;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class effects {
	
	static int ILE = 10;

	Vec2 pos[];
	Vec2 vec[];
	float wave[];
	float angle[];
	float wavetutorial;
	float wavepostutorial;
	Vec2 postutorial;
	Vec2 postutorialtmp;
	define Define;

	int co[];
	int coSmoke[];
	int ile;
	float jump;
	
	f3ds F3ds;
	matrixInv MatrixInv;
	
	
void reset()
{
ile=0;
wavetutorial=1.0f;
wavepostutorial=1.0f;
postutorial = new Vec2(19.0f, 3.5f);
postutorialtmp = new Vec2(19.0f, 3.5f);
}

	
	
effects(Context aC)
{
	pos=new Vec2[ILE];
	vec=new Vec2[ILE];
	wave=new float[ILE];
	angle=new float[ILE];
	co=new int[ILE];
	coSmoke=new int[ILE];
	MatrixInv = new matrixInv();
	postutorial = new Vec2(25.0f, 3.5f);
	postutorialtmp = new Vec2(25.0f, 3.5f);
	Define = new define();

	wavetutorial=0.0f;
	wavepostutorial=0.0f;
	ile=0;
	jump=-1.0f;
	F3ds  = new f3ds(aC);	
	for(int i=0;i<ILE;i++)
	{
		wave[i]=-1.0f;
		angle[i]=-1.0f;
		pos[i]=new Vec2();
		vec[i]=new Vec2();
		co[i]=-1;
		coSmoke[i]=0;
	}
}
	



void addSmoke(float x, float y)
{
	Random random=new Random();
	coSmoke[ile]=random.nextInt(2);
		pos[ile].set(x,y);
		co[ile]=2;
		wave[ile]=1.0f;
		ile++;
		if(ile>ILE-1)
			ile=0;
}


void addBloodFog(float x, float y)
{
		pos[ile].set(x,y);
		co[ile]=1;
		wave[ile]=1.0f;
		ile++;
		if(ile>ILE-1)
			ile=0;
}



void addBloodLine(float x, float y, float a)
{
		pos[ile].set(x,y);
		angle[ile]=a;
		co[ile]=4;
		wave[ile]=1.0f;
		ile++;
		if(ile>ILE-1)
			ile=0;
}



void addBlood(float x, float y)
{
		pos[ile].set(x,y);
		co[ile]=3;
		wave[ile]=1.2f;
		ile++;
		if(ile>ILE-1)
			ile=0;
}




//void addShootGun(float x, float y, float vx, float vy)
//{
//	pos[ile].set(x,y);
//	vec[ile].set(vx, vy);
//	vec[ile].normalize();
//	co[ile]=5;
//	wave[ile]=1.0f;
//	ile++;
//	if(ile>ILE-1)
//		ile=0;
//}




void DrawBloodShootGun(int ShaderProgram, float loop)
{
	loop=loop*tekstury.bulletTime;
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	
	Vec2 vecTmp=new Vec2();
	
	for(int i=0;i<ILE;i++)
		if(co[i]==5)
			{
			wave[i]=wave[i]-0.2f*loop;
			if(wave[i]<0.0f)
				co[i]=0;
			GLES20.glUniform1f(iWaveVS, wave[i]);
			
			
			Matrix.setIdentityM(tmpMatrix, 0);
			
			Matrix.translateM(tmpMatrix, 0, pos[i].x+vec[i].x*(1.0f-wave[i])*3.7f, pos[i].y+vec[i].y*(1.0f-wave[i])*3.7f, 0.0f);

			Matrix.scaleM(tmpMatrix, 0, 1.0f-wave[i]*0.8f, 1.0f-wave[i]*0.8f, 1.0f);
			
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			
			F3ds.DrawModel(26, tekstury.texture[19], ShaderProgram );
			
			
			
			vecTmp.set(vec[i].x, vec[i].y);
			vecTmp.rotate(7.5f);
			Matrix.setIdentityM(tmpMatrix, 0);
			
			Matrix.translateM(tmpMatrix, 0, pos[i].x+vecTmp.x*(1.0f-wave[i])*3.7f, pos[i].y+vecTmp.y*(1.0f-wave[i])*3.7f, 0.0f);

			Matrix.scaleM(tmpMatrix, 0, 1.0f-wave[i]*0.8f, 1.0f-wave[i]*0.8f, 1.0f);
			   
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			
			F3ds.DrawModel(26, tekstury.texture[19], ShaderProgram );
			
			
			
			vecTmp.set(vec[i].x, vec[i].y);
			vecTmp.rotate(-7.5f);
			Matrix.setIdentityM(tmpMatrix, 0);
			
			Matrix.translateM(tmpMatrix, 0, pos[i].x+vecTmp.x*(1.0f-wave[i])*3.7f, pos[i].y+vecTmp.y*(1.0f-wave[i])*3.7f, 0.0f);

			Matrix.scaleM(tmpMatrix, 0, 1.0f-wave[i]*0.8f, 1.0f-wave[i]*0.8f, 1.0f);
			
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			
			F3ds.DrawModel(26, tekstury.texture[19], ShaderProgram );
			}
}







void DrawBloodLine(int ShaderProgram, float loop)
{
	loop=loop*tekstury.bulletTime;
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	
	for(int i=0;i<ILE;i++)
		if(co[i]==4)
			{
			wave[i]=wave[i]-0.02f*loop;
			if(wave[i]<0.0f)
				co[i]=0;
			GLES20.glUniform1f(iWaveVS, wave[i]);
			Matrix.setIdentityM(tmpMatrix, 0);
		
			Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y-(1.9f-wave[i]*1.9f), 0.0f);
			Matrix.rotateM(tmpMatrix, 0, angle[i], 0,0,1);
			Matrix.scaleM(tmpMatrix, 0, 3.0f-wave[i]*2.0f, 3.0f-wave[i]*2.0f, 1.0f);
			
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			

			F3ds.DrawModel(52, tekstury.texture[2], ShaderProgram );
			}
}






public void DrawBloodFog(int ShaderProgram, float loop)
{
	loop=loop*tekstury.bulletTime;
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	
	for(int i=0;i<ILE;i++)
		if(co[i]==1)
			{
			wave[i]=wave[i]-0.02f*loop;
			if(wave[i]<0.0f)
				co[i]=0;
			GLES20.glUniform1f(iWaveVS, wave[i]);
			Matrix.setIdentityM(tmpMatrix, 0);
		
			Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y - (1.9f - wave[i] * 1.9f), 0.0f);
			Matrix.scaleM(tmpMatrix, 0, 1.7f-wave[i]*1.5f, 1.7f-wave[i]*1.5f, 1.0f);
			
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
			

			F3ds.DrawModel(28, tekstury.texture[2], ShaderProgram );
			}
}






	public void DrawBlood(int ShaderProgram, float loop)
	{
		loop=loop*tekstury.bulletTime;
		float[] tmpMatrix = new float[16];
		
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		
		for(int i=0;i<ILE;i++)
			if(co[i]==3)
				{
				wave[i]=wave[i]-0.1f*loop;
				if(wave[i]<0.0f)
					co[i]=0;
				GLES20.glUniform1f(iWaveVS, wave[i]);
				Matrix.setIdentityM(tmpMatrix, 0);
			
				Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y-(1.25f-wave[i]*1.25f), 0.0f);
				Matrix.scaleM(tmpMatrix, 0, 6.8f-wave[i]*5.0f, 6.8f-wave[i]*5.0f, 1.0f);
				
				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
				
	
				F3ds.DrawModel(13, tekstury.texture[2], ShaderProgram );
				}
	}


	

	public void DrawTutorial(int ShaderProgram, float loop)
	{
		loop=loop*tekstury.bulletTime;
		int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		GLES20.glUniform1f(iWaveVS, 1.0f);

		float[] tmpMatrix = new float[16];

		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

		if(Math.sin(wavetutorial)<-0.5) {
			Matrix.setIdentityM(tmpMatrix, 0);
			Matrix.translateM(tmpMatrix, 0, postutorial.x, postutorial.y, -0.2f);
			Matrix.scaleM(tmpMatrix, 0, 1.3f + (float) Math.abs(Math.sin(wavetutorial)), 1.3f + (float) Math.abs(Math.sin(wavetutorial)),1.0f);
			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
			F3ds.DrawModel(102, tekstury.texture[14], ShaderProgram);
		}
		else
		{
			float distans=(float)Define.distanc(postutorialtmp.x, postutorialtmp.y, postutorial.x, postutorial.y)*1.4f;
			if(postutorial.x<postutorialtmp.x)postutorial.x=postutorial.x+0.2f*loop*distans;
			if(postutorial.x>postutorialtmp.x)postutorial.x=postutorial.x-0.2f*loop*distans;
			if(postutorial.y<postutorialtmp.y)postutorial.y=postutorial.y+0.2f*loop*distans;
			if(postutorial.y>postutorialtmp.y)postutorial.y=postutorial.y-0.2f*loop*distans;
		}


				Matrix.setIdentityM(tmpMatrix, 0);
				wavetutorial=wavetutorial+0.15f*loop;

				Matrix.translateM(tmpMatrix, 0, postutorial.x, postutorial.y, (float) Math.sin(wavetutorial) * 0.2f);
				Matrix.scaleM(tmpMatrix, 0, 1.8f, 1.8f, 1.0f);

				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

				F3ds.DrawModel(60, tekstury.texture[14], ShaderProgram);


	}
	
	
	
	public void DrawSmoke(int ShaderProgram, float loop)
	{
		loop=loop*tekstury.bulletTime;
		float[] tmpMatrix = new float[16];
		
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");

		
		for(int i=0;i<ILE;i++)
			if(co[i]==2)
				{
				wave[i]=wave[i]-0.01f*loop;
				if(wave[i]<0.2f)
					co[i]=0;
				
				GLES20.glUniform1f(iWaveVS, wave[i]);
				Matrix.setIdentityM(tmpMatrix, 0);
		
				Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y+(1.0f-wave[i]), 0.1f);
				Matrix.scaleM(tmpMatrix, 0, 4.8f-wave[i]*4.0f, 4.8f-wave[i]*4.0f, 1.0f);
				
				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

				F3ds.DrawModel(15, tekstury.texture[2], ShaderProgram );
				
				
				
				
				GLES20.glUniform1f(iWaveVS, 1.0f);
				Matrix.setIdentityM(tmpMatrix, 0);
		
				Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y-(12.0f-wave[i]*12.0f), 0.0f);
				Matrix.scaleM(tmpMatrix, 0, 0.5f*(1.5f-wave[i]), 0.5f*(1.5f-wave[i]), 1.0f);
				
				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
				
				F3ds.DrawModel(5, tekstury.texture[2], ShaderProgram );
				}
	}
	
	
	
	
}
