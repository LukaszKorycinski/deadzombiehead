package com.example.lukasz.nazizombieshooter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;

import com.example.lukasz.style.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class MainActivity extends Activity  implements MyInterstitialListener {
    /** Called when the activity is first created. */


    private AppView view;


    InterstitialAd mInterstitialAd;



    @Override
    public void onBackPressed()
    {
        gui2d.survivemodeflag=0;
        Globalne.Menu_Flaga=1;
        view.renderer.mPlayer.pause();
        view.renderer.Blutacz.cleanup();
    }



//    @Override
//    protected void onResume()
//    {
//        // The activity must call the GL surface view's onResume() on activity onResume().
//        //super.onResume();
//        //view.onResume();
//    }

//    @Override
//    protected void onPause()
//    {
//        // The activity must call the GL surface view's onPause() on activity onPause().
//        //super.onPause();
//        //view.onPause();
//        //view.renderer.mPlayer.stop();
//        //view.renderer.sp.release();
//        //view.renderer.sp = null;
//        //tekstury.destroy();
//        //soundPool.release();
//        //soundPool = null;
//    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //view.renderer.Blutacz.OnActivityResults(requestCode, resultCode, data);
        if(view.renderer.Blutacz.OnActivityResults(requestCode, resultCode, data)==-1)
            Globalne.Menu_Flaga=4;


    }

    // method invoke when mybutton will click
    public void displayInterstitial() {
        // do your stuff here
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }

        }


    @Override
    public void onCreate(Bundle savedInstanceState)   {


        this.requestWindowFeature(Window.FEATURE_NO_TITLE); // (nowa linia)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); // (nowa linia)


        super.onCreate(savedInstanceState);

        HomeWatcher mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                // do something here...
                view.renderer.mPlayer.pause();
                view.renderer.Blutacz.cleanup();
            }

            @Override
            public void onHomeLongPressed() {
                view.renderer.mPlayer.pause();
                view.renderer.Blutacz.cleanup();
            }
        });
        mHomeWatcher.startWatch();


        view = new AppView(this, this);
        setContentView(view);






        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial_id));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();



    }




    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }



    @Override
    public void callback() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                displayInterstitial();
            }
        });
    }






}