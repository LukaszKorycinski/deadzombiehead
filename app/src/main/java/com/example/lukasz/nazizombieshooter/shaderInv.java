package com.example.lukasz.nazizombieshooter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.opengl.GLES20;

import com.example.lukasz.style.R;

public class shaderInv {
private static Context appContext;
int ShaderProgramFlag, ShaderProgramBulletTime, ShaderProgram, ShaderProgramChar, ShaderProgramGrass, ShaderProgramTree, ShaderProgramLef,
ShaderProgramSimple, ShaderProgramFlesh, ShaderProgramSky, ShaderProgramSmoke, ShaderProgramSimpleA,
		ShaderProgramVision, ShaderProgramFlagH, ShaderProgramFlagHFont, ShaderProgramFlagHFontGui, ShaderProgramCharBlood;


shaderInv(Context aC)
{
	appContext=aC;
}




public static int loadShader(int type, String shaderCode)
{
    int shader = GLES20.glCreateShader(type);

    GLES20.glShaderSource(shader, shaderCode);
    GLES20.glCompileShader(shader);

    return shader;
}





public void loadShaderS()
{

InputStream inputStream = appContext.getResources().openRawResource(R.raw.vp);
InputStreamReader inputreader = new InputStreamReader(inputStream);
BufferedReader buffreader = new BufferedReader(inputreader);
String line;
StringBuilder text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());



inputStream = appContext.getResources().openRawResource(R.raw.fp);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());





inputStream = appContext.getResources().openRawResource(R.raw.vpchar);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderChar = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());




inputStream = appContext.getResources().openRawResource(R.raw.fpchar);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderChar = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpgrass);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderGrass = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());



inputStream = appContext.getResources().openRawResource(R.raw.vpgrass);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderGrass = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());




inputStream = appContext.getResources().openRawResource(R.raw.vptree);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderTree = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fptree);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderTree = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());





inputStream = appContext.getResources().openRawResource(R.raw.vplef);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderLef = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fplef);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderLef = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());





inputStream = appContext.getResources().openRawResource(R.raw.vpsimple);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderSimple = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpsimple);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderSimple = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.vpflesh);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderFlesh = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpflesh);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderFlesh = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());





inputStream = appContext.getResources().openRawResource(R.raw.vpsky);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderSky = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpsky);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderSky = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());




inputStream = appContext.getResources().openRawResource(R.raw.vpsimplea);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderSimpleA = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpsimplea);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderSimpleA = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());







inputStream = appContext.getResources().openRawResource(R.raw.vpflag);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderFlag = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpflag);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderFlag = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());







inputStream = appContext.getResources().openRawResource(R.raw.vpsmoke);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int vertexShaderSmoke = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


inputStream = appContext.getResources().openRawResource(R.raw.fpsmoke);
inputreader = new InputStreamReader(inputStream);
buffreader = new BufferedReader(inputreader);
text = new StringBuilder();
try {
	while (( line = buffreader.readLine()) != null) 
		{
	    text.append(line);
	    text.append('\n');
		}
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
int fragmentShaderSmoke = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());







	inputStream = appContext.getResources().openRawResource(R.raw.vpvision);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int vertexShaderVision = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


	inputStream = appContext.getResources().openRawResource(R.raw.fpvision);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int fragmentShaderVision = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());


	inputStream = appContext.getResources().openRawResource(R.raw.vpflagh);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int vertexShaderFlagH = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


	inputStream = appContext.getResources().openRawResource(R.raw.fpflagh);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int fragmentShaderFlagH = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());






	inputStream = appContext.getResources().openRawResource(R.raw.vpflaghfont);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int vertexShaderFlagHFont = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


	inputStream = appContext.getResources().openRawResource(R.raw.fpflaghfont);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int fragmentShaderFlagHFont = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());










	inputStream = appContext.getResources().openRawResource(R.raw.fpflaghfontgui);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int fragmentShaderFlagHFontGui = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());





	inputStream = appContext.getResources().openRawResource(R.raw.fpcharblood);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int fragmentShaderCharBlood = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());










	inputStream = appContext.getResources().openRawResource(R.raw.vpbullettime);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int vertexShaderBulletTime = loadShader(GLES20.GL_VERTEX_SHADER, text.toString());


	inputStream = appContext.getResources().openRawResource(R.raw.fbbullettime);
	inputreader = new InputStreamReader(inputStream);
	buffreader = new BufferedReader(inputreader);
	text = new StringBuilder();
	try {
		while (( line = buffreader.readLine()) != null)
		{
			text.append(line);
			text.append('\n');
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	int fragmentShaderBulletTime = loadShader(GLES20.GL_FRAGMENT_SHADER, text.toString());







ShaderProgramBulletTime = GLES20.glCreateProgram();             // create empty OpenGL ES Program
	GLES20.glAttachShader(ShaderProgramBulletTime, vertexShaderBulletTime);   // add the vertex shader to program
	GLES20.glAttachShader(ShaderProgramBulletTime, fragmentShaderBulletTime); // add the fragment shader to program
	GLES20.glLinkProgram(ShaderProgramBulletTime);


ShaderProgramCharBlood = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramCharBlood, vertexShaderChar);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramCharBlood, fragmentShaderCharBlood); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramCharBlood);

ShaderProgramFlagHFontGui = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramFlagHFontGui, vertexShaderFlagHFont);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramFlagHFontGui, fragmentShaderFlagHFontGui); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramFlagHFontGui);

ShaderProgramFlagHFont = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramFlagHFont, vertexShaderFlagHFont);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramFlagHFont, fragmentShaderFlagHFont); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramFlagHFont);

ShaderProgramFlagH = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramFlagH, vertexShaderFlagH);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramFlagH, fragmentShaderFlagH); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramFlagH);

ShaderProgramVision = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramVision, vertexShaderVision);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramVision, fragmentShaderVision); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramVision);

ShaderProgramFlag = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramFlag, vertexShaderFlag);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramFlag, fragmentShaderFlag); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramFlag);  


ShaderProgramSimpleA = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramSimpleA, vertexShaderSimpleA);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramSimpleA, fragmentShaderSimpleA); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramSimpleA);  


ShaderProgramSmoke = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramSmoke, vertexShaderSmoke);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramSmoke, fragmentShaderSmoke); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramSmoke); 

ShaderProgramSky = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramSky, vertexShaderSky);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramSky, fragmentShaderSky); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramSky);    

ShaderProgramFlesh = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramFlesh, vertexShaderFlesh);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramFlesh, fragmentShaderFlesh); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramFlesh);    

ShaderProgramLef = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramLef, vertexShaderLef);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramLef, fragmentShaderLef); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramLef);    

ShaderProgramSimple = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramSimple, vertexShaderSimple);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramSimple, fragmentShaderSimple); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramSimple);   

ShaderProgram = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgram, vertexShader);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgram, fragmentShader); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgram);    

ShaderProgramGrass = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramGrass, vertexShaderGrass);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramGrass, fragmentShaderGrass); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramGrass);    

ShaderProgramTree = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramTree, vertexShaderTree);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramTree, fragmentShaderTree); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramTree);  

ShaderProgramChar = GLES20.glCreateProgram();             // create empty OpenGL ES Program
GLES20.glAttachShader(ShaderProgramChar, vertexShaderChar);   // add the vertex shader to program
GLES20.glAttachShader(ShaderProgramChar, fragmentShaderChar); // add the fragment shader to program
GLES20.glLinkProgram(ShaderProgramChar);                  // creates OpenGL ES program executables
}

	
}
