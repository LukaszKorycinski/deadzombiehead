package com.example.lukasz.nazizombieshooter;

/**
 * Created by Lukasz on 2015-07-11.
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Random;
import android.content.SharedPreferences;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.example.lukasz.style.R;
import com.example.lukasz.style.R.raw;


//import org.jbox2d.common.Vec2;

import android.app.Activity;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.opengl.GLES20;

import android.content.Context;
import android.opengl.GLSurfaceView;


public class AppRenderer implements GLSurfaceView.Renderer
{
    MyInterstitialListener mL;



    private Context appContext;
//private final float[] mProjectionMatrix = new float[16];
//private final float[] mViewMatrix = new float[16];


    private SharedPreferences preferences;
    shaderInv ShaderInv;
    Rope rope;
    f3ds F3ds;
    a3df A3df;
    tekstury Tekstury;
    Smugi smugi;
    Resources res;
    Globalne globalne;

    public AppRenderer(Context context, Activity ac)
    {
        this.appContext = context;
        this.activity=ac;
    }


    float dotykON[]={1.0f, 1.0f, 1.0f, 1.0f, 1.0f};
    float dotykX[]=new float[5];
    float dotykY[]=new float[5];
    int dotykActionIDCel=-1;
    int dotykActionIDMove=-1;

    float rotX=0.0f;
    float rotY=0.0f;
    float posX=0.0f;
    float posY=0.0f;
    float posZ=-5.5f;//-5.5f

    treespeed Treespeed;
    modelstatic Modelstatic;
    matrixInv MatrixInv;
//Back back;

    Spikes spikes;

    gui2d Gui2d;
    grass Grass;
    Hero hero;
    npc Npc;
    bullet Bullet;
    fontInv FontInv;
    FpsTime fpsTime;
    ally Ally;
    Guns guns;

    SoundPool sp;
    int soundId,soundId2,soundId4,soundId3,soundIdBang,soundIdCrack, soundIdPuke;//,soundIdMusic;
    MediaPlayer mPlayer;
    define Define;
    effects Effects;
    Bang bang;
    boxWorld BoxWorld;
    menager Menager;
    float dupa=0.0f;
    bulletEnemy BulletEnemy;



    Vec2f1i colPosLog;
    Vec2f1i colNpcLog;
    //Vec2f1i colAllyLog;
    Vec2f1i colBarrelLog;
    Vec2f1i colCageLog;

    blutacz Blutacz;
    Activity activity;

    float bulletEffect=0.0f;

    void DrawGame()
    {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        //GLES20.glClearDepthf(1.0f);


        matrixInv.camMatrix(rotX, rotY, posX, posY, posZ);


        if (tekstury.bulletTime<0.5f && bulletEffect<1.0f){bulletEffect=bulletEffect+0.05f*fpsTime.loop; if(bulletEffect>1.0f)bulletEffect=1.0f; }
        if (tekstury.bulletTime>0.5f && bulletEffect>0.0f){bulletEffect=bulletEffect-0.05f*fpsTime.loop; if(bulletEffect<0.0f)bulletEffect=0.0f; }


        GLES20.glUseProgram(ShaderInv.ShaderProgram);


        int lvlbulletEffect = GLES20.glGetUniformLocation(ShaderInv.ShaderProgram, "bulletEffect");
        GLES20.glUniform1f(lvlbulletEffect, 0.0f);
        int lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgram, "lvlTheme");


        GLES20.glUniform4f(lvlThemeFS, 1.0f + bulletEffect, 1.0f + bulletEffect, 1.0f + bulletEffect, 1.0f);
        Npc.DrawBalon(ShaderInv.ShaderProgram, hero.pos);
        Npc.DrawSamolot(ShaderInv.ShaderProgram, hero.pos, fpsTime.loop);


        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x, Gui2d.lvlTheme[gui2d.actualLevel].y, Gui2d.lvlTheme[gui2d.actualLevel].z, Gui2d.lvlTheme[gui2d.actualLevel].w);
        GLES20.glUniform1f(lvlbulletEffect, bulletEffect);


        Modelstatic.Draw(ShaderInv.ShaderProgram, hero.pos);
        if(tekstury.secNdPlayer_flag==0)
        Modelstatic.DrawEnd(ShaderInv.ShaderProgram, hero.pos);

        if(Math.abs(hero.pos.x - Modelstatic.EndPos.x)<1.5f && tekstury.secNdPlayer_flag==0) {
            Globalne.Menu_Flaga = 3;
            if (gui2d.actualLevel < 23)
                {
                Gui2d.levelStateInt[gui2d.actualLevel + 1] = 1;

                preferences = appContext.getSharedPreferences("danesharedpref", appContext.MODE_PRIVATE);
                SharedPreferences.Editor preferencesEditor = preferences.edit();
                int stateforshare = gui2d.actualLevel + 1;
                preferencesEditor.putInt("level" + stateforshare, Gui2d.levelStateInt[gui2d.actualLevel + 1]);
                preferencesEditor.commit();
                }

            Gui2d.scrollMap = -(2.0f*gui2d.actualLevel+2.0f)*100.0f;
        }
        spikes.Draw(ShaderInv.ShaderProgram);
        Modelstatic.barrel.Draw(ShaderInv.ShaderProgram);
        Modelstatic.cage.Draw(ShaderInv.ShaderProgram);
        guns.Draw(ShaderInv.ShaderProgram, fpsTime.loop);
        //hero.DrawSmuga(ShaderInv.ShaderProgram, fpsTime.loop, hero.pos);

//        for(int i=0; i<spikes.ile; i++)
//            if(spikes.colision(hero.pos.x, hero.pos.y, 0.4f, 0.4f)==1) {
//                if(hero.hp>0) {
//                    BoxWorld.add(hero.pos.x, hero.pos.y - 0.2f, 0.5f, 1.2f, 0);
//                    BoxWorld.add(hero.pos.x, hero.pos.y + 0.3f, -0.5f, 1.7f, 1);
//                }
//                hero.hp=0;
//            }



        GLES20.glUseProgram(ShaderInv.ShaderProgramFlesh);

        lvlbulletEffect = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramFlesh, "bulletEffect");
        GLES20.glUniform1f(lvlbulletEffect, bulletEffect);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramFlesh, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x*0.3f, Gui2d.lvlTheme[gui2d.actualLevel].y*0.3f, Gui2d.lvlTheme[gui2d.actualLevel].z*0.3f, Gui2d.lvlTheme[Gui2d.actualLevel].w);

        BoxWorld.logic(fpsTime.loop);
        BoxWorld.Draw(ShaderInv.ShaderProgramFlesh);



        for(int i=0;i<BoxWorld.ile;i++)
        {
            if(BoxWorld.waveAddBlood[i]>1.0f)
            {
                BoxWorld.waveAddBlood[i]=0.0f;
                if(BoxWorld.co[i]<10 || BoxWorld.co[i]>15)
                    Effects.addBloodFog(BoxWorld.pos[i].x, BoxWorld.pos[i].y);
                else
                Effects.addSmoke(BoxWorld.pos[i].x, BoxWorld.pos[i].y);
            }

            if( Modelstatic.colisionCircle(BoxWorld.pos[i].x+BoxWorld.vec[i].x, BoxWorld.pos[i].y, 0.3f*BoxWorld.wave[i])==0 )
                BoxWorld.pos[i].set(BoxWorld.pos[i].x+BoxWorld.vec[i].x, BoxWorld.pos[i].y);
            else
            {
                BoxWorld.vec[i].set(-BoxWorld.vec[i].x*0.5f, BoxWorld.vec[i].y);
                //BoxWorld.wave[i]-=0.1f*fpsTime.loop;
            }
            if( Modelstatic.colisionCircle(BoxWorld.pos[i].x, BoxWorld.pos[i].y+BoxWorld.vec[i].y, 0.3f*BoxWorld.wave[i])==0 )
                BoxWorld.pos[i].set(BoxWorld.pos[i].x, BoxWorld.pos[i].y+BoxWorld.vec[i].y);
            else
            {
                BoxWorld.vec[i].set(BoxWorld.vec[i].x,-BoxWorld.vec[i].y*0.5f);
                //BoxWorld.wave[i]-=0.1f*fpsTime.loop;
            }
        }



//
//        for(int i=0;i<Bullet.ile;i++)
//            if (barrel.colisionCircle(Bullet.pos[i].x + Bullet.vec[i].x, Bullet.pos[i].y + Bullet.vec[i].y, 0.2f) == 1)
//            {
////             bang.add(Bullet.pos[i].x + Bullet.vec[i].x, Bullet.pos[i].y + Bullet.vec[i].y);
////             barrel.del(i);
////
//            }



//        for(int i=0;i<Bullet.ile;i++) {//GRANATY WYWALIC?
//            if (Modelstatic.colisionCircle(Bullet.pos[i].x + Bullet.vec[i].x, Bullet.pos[i].y + Bullet.vec[i].y, 0.1f) == 0)
//                Bullet.pos[i].set(Bullet.pos[i].x + Bullet.vec[i].x, Bullet.pos[i].y + Bullet.vec[i].y);
//            else {
//                if(Bullet.activ[i] != 0)
//                    sp.play(soundId3, 1, 1, 0, 0, 1);
//                Bullet.activ[i] = 0;
//            }
//
//            int barrelArrowcoli = Modelstatic.barrel.colisionCircle(Bullet.pos[i].x + Bullet.vec[i].x, Bullet.pos[i].y + Bullet.vec[i].y, 0.1f);
//            if( barrelArrowcoli>-1 )
//                {
//                    bang.add(Modelstatic.barrel.pos[barrelArrowcoli].x, Modelstatic.barrel.pos[barrelArrowcoli].y);
//                    for(int j=0;j<Npc.ile;j++)
//                        if(Define.distanc(Npc.pos[j].x, Npc.pos[j].y, Modelstatic.barrel.pos[barrelArrowcoli].x, Modelstatic.barrel.pos[barrelArrowcoli].y)<5.0f)
//                            Npc.hp[j]=0;
//
//                    if(Define.distanc(Modelstatic.barrel.pos[barrelArrowcoli].x,Modelstatic.barrel.pos[barrelArrowcoli].y,hero.pos.x,hero.pos.y)<2.5f) {
//                        Vec2 tmpHeroBarrelVec = new Vec2(hero.pos.x - Modelstatic.barrel.pos[barrelArrowcoli].x, 1.0f);
//                        tmpHeroBarrelVec.setLength(0.4f);
//                        hero.tmpPos.set(tmpHeroBarrelVec.x, tmpHeroBarrelVec.y);
//                    }
//                    Modelstatic.barrel.del(barrelArrowcoli);
//                    sp.play(soundIdBang, 1, 1, 0, 0, 1);
//                }
//
//            int cageArrowcoli=Modelstatic.cage.colisionCircle(Bullet.pos[i].x + Bullet.vec[i].x, Bullet.pos[i].y + Bullet.vec[i].y, 0.1f);
//            if( cageArrowcoli>-1 )
//            {
//                BoxWorld.add(Modelstatic.cage.pos[cageArrowcoli].x, Modelstatic.cage.pos[cageArrowcoli].y, Bullet.vec[i].x*0.8f, Bullet.vec[i].y * 1.6f, 14);
//                BoxWorld.add(Modelstatic.cage.pos[cageArrowcoli].x, Modelstatic.cage.pos[cageArrowcoli].y,-Bullet.vec[i].x*1.3f, Bullet.vec[i].y * 2.2f, 15);
//                Ally.add(Modelstatic.cage.pos[cageArrowcoli].x, Modelstatic.cage.pos[cageArrowcoli].y);
//
//                Modelstatic.cage.del(cageArrowcoli);
//                sp.play(soundIdCrack, 1, 1, 0, 0, 1);
//            }
//
//            if (Bullet.activ[i] == 1) {
//                int coli = Npc.colisionBoxForArrow(Bullet.pos[i].x, Bullet.pos[i].y, 0.15f, 0.15f);
//                if (coli > -1) {
//                    Npc.addArrow(coli, new Vec3(Bullet.pos[i].x, Bullet.pos[i].y, Bullet.angle[i]));
//
//                    Vec2f1i arrowHitColiWal = Modelstatic.colisionRay(Bullet.pos[i], new Vec2(Bullet.pos[i].x+Bullet.vec[i].x, Bullet.pos[i].y+Bullet.vec[i].y), 3.2f);
//                    if(arrowHitColiWal.a>-1){//przybijanie do   sciany
//                        Npc.unieruchomiony[coli]=1;
//                        Vec2 PrzybityPos=new Vec2();
//                        PrzybityPos.set(arrowHitColiWal.x - Bullet.pos[i].x, arrowHitColiWal.y - Bullet.pos[i].y);
//                        float lengthPrzybity= (float)Math.sqrt(   PrzybityPos.x*PrzybityPos.x + PrzybityPos.y*PrzybityPos.y   );
//                        PrzybityPos.setLength(lengthPrzybity-0.9f);
//                        Npc.pos[coli].set(Npc.pos[coli].x+PrzybityPos.x, Npc.pos[coli].y+PrzybityPos.y);
//                    }
//                    sp.play(soundId3, 1, 1, 0, 0, 1);
//                    Bullet.del(i);
//                    Npc.hp[coli]--;
//                    if (Npc.co[coli] != 4) {
//                        if (hero.pos.x < Npc.pos[coli].x && Npc.vec[coli].x < 0.05f) {
//                            if(Npc.co[coli]!=6) {
//                                Npc.vec[coli].x += 0.3f;
//                                if(Npc.co[coli]==9){Npc.vec[coli].x+=0.7f;Npc.vec[coli].y+=0.3f;}
//                                }
//                            Npc.shake[coli]=30.0f;
//                        }
//                        if (hero.pos.x > Npc.pos[coli].x && Npc.vec[coli].x > -0.05f) {
//                            if(Npc.co[coli]!=6) {
//                                Npc.vec[coli].x -= 0.3f;
//                                if(Npc.co[coli]==9){Npc.vec[coli].x-=0.7f; Npc.vec[coli].y+=0.3f;}
//                                }
//                            Npc.shake[coli]=-30.0f;
//                        }
//
//                        Effects.addBlood(Bullet.pos[i].x, Bullet.pos[i].y);
//                    }
//
//                    if(/*hero.bron==0 ||  Npc.co[colNpcLog.a]==3 || Npc.co[colNpcLog.a]==0 ||*/ Npc.hp[coli]==0 /*|| (Npc.co[colNpcLog.a]==1 && colNpcLog.y-Npc.pos[colNpcLog.a].y>0.7f)*/) {
//                        if (Npc.co[coli] == 0 || Npc.co[coli] == 5 || Npc.co[coli] == 6 || Npc.co[coli] == 7) {
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y - 0.2f, Bullet.vec[i].x*0.8f, Bullet.vec[i].y*0.8f-0.2f, 0);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y + 0.3f, Bullet.vec[i].x*1.5f, Bullet.vec[i].y*1.5f+0.8f, 1);
//                        }
//                        if (Npc.co[coli] == 1) {
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y - 0.25f, Bullet.vec[i].x*0.8f, Bullet.vec[i].y*0.8f-0.2f, 2);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y + 1.0f, Bullet.vec[i].x*1.5f, Bullet.vec[i].y*1.5f+0.8f, 3);
//                        }
//                        if (Npc.co[coli] == 2) {
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y - 0.2f, Bullet.vec[i].x*0.8f, Bullet.vec[i].y*0.8f-0.2f, 4);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y + 0.3f, Bullet.vec[i].x*1.5f, Bullet.vec[i].y*1.5f+0.8f, 5);
//                        }
//                        if (Npc.co[coli] == 3) {
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y - 0.2f, Bullet.vec[i].x*0.8f, Bullet.vec[i].y*0.8f-0.2f, 6);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y + 0.3f, Bullet.vec[i].x*1.5f, Bullet.vec[i].y*1.5f+0.8f, 7);
//                        }
//                        if (Npc.co[coli] == 4) {
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y, 0.1f, 0.1f, 10);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y, 0.1f, 0.1f, 11);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y, 0.1f, 0.1f, 12);
//                            BoxWorld.add(Npc.pos[coli].x, Npc.pos[coli].y - 0.5f, 0.1f, 0.1f, 13);
//                        }
//
//                        Npc.del(coli);
//                        tekstury.bulletZasob=tekstury.bulletZasob+0.5f;
//                        if (hero.emotion == 0) {
//                            if(Gui2d.actualLevel>0)hero.DialogAdd(2);
//                            hero.emotion = 2;
//                            hero.emotionWave = 1.0f;
//                        }
//                    }
//
//                }
//            }
//        }


//        for(int i=0;i<Bang.ILE;i++)
//            if(bang.wave[i]>0.0f)
//                for(int i2=0;i2<4;i2++)
//                {
//                    if( Modelstatic.colisionCircle(bang.posOdlamek[i][i2].x+bang.vec[i][i2].x, bang.posOdlamek[i][i2].y, 0.2f)==0 )
//                        bang.posOdlamek[i][i2].set(bang.posOdlamek[i][i2].x+bang.vec[i][i2].x, bang.posOdlamek[i][i2].y);
//                    else
//                        bang.vec[i][i2].set(-bang.vec[i][i2].x, bang.vec[i][i2].y);
//                    if( Modelstatic.colisionCircle(bang.posOdlamek[i][i2].x,bang.posOdlamek[i][i2].y+bang.vec[i][i2].y*(4-0), 0.2f)==0 )
//                        bang.posOdlamek[i][i2].set(bang.posOdlamek[i][i2].x, bang.posOdlamek[i][i2].y+bang.vec[i][i2].y*(4-0));
//                    else
//                        bang.vec[i][i2].set(bang.vec[i][i2].x,-bang.vec[i][i2].y);
//
//
//                    int trafionyOdlamkiem=Npc.colision(bang.posOdlamek[i][i2].x, bang.posOdlamek[i][i2].y);
//                    if( trafionyOdlamkiem>-1 )
//                    {
//                        if(Npc.co[trafionyOdlamkiem]==0)
//                        {
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y-0.2f, bang.vec[i][i2].x*0.8f, bang.vec[i][i2].y*0.8f-0.2f, 0);
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y+0.3f, bang.vec[i][i2].x*1.5f, bang.vec[i][i2].y*1.5f+0.8f, 1);
//                        }
//                        if(Npc.co[trafionyOdlamkiem]==1)
//                        {
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y-0.25f, bang.vec[i][i2].x*0.8f, bang.vec[i][i2].y*0.8f-0.2f, 2);
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y+1.0f,  bang.vec[i][i2].x*1.5f, bang.vec[i][i2].y*1.5f+0.8f, 3);
//                        }
//                        if(Npc.co[trafionyOdlamkiem]==2)
//                        {
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y-0.2f, bang.vec[i][i2].x*0.8f, bang.vec[i][i2].y*0.8f-0.2f, 4);
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y+0.3f, bang.vec[i][i2].x*1.5f, bang.vec[i][i2].y*1.5f+0.8f, 5);
//                        }
//                        if(Npc.co[trafionyOdlamkiem]==3)
//                        {
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y-0.2f, bang.vec[i][i2].x*0.8f, bang.vec[i][i2].y*0.8f-0.2f, 6);
//                            BoxWorld.add(Npc.pos[trafionyOdlamkiem].x, Npc.pos[trafionyOdlamkiem].y+0.3f, bang.vec[i][i2].x*1.5f, bang.vec[i][i2].y*1.5f+0.8f, 7);
//                        }
//
//                        Npc.del(trafionyOdlamkiem);
//                        if(hero.emotion==0) {
//                            hero.emotion = 2;
//                            hero.emotionWave = 1.0f;
//                        }
//                        Effects.addBlood(bang.posOdlamek[i][i2].x, bang.posOdlamek[i][i2].y);
//                    }
//
//                }




        GLES20.glUseProgram(ShaderInv.ShaderProgramTree);
        lvlbulletEffect = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramTree, "bulletEffect");
        GLES20.glUniform1f(lvlbulletEffect, bulletEffect);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramTree, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[Gui2d.actualLevel].x, Gui2d.lvlTheme[Gui2d.actualLevel].y, Gui2d.lvlTheme[Gui2d.actualLevel].z, Gui2d.lvlTheme[Gui2d.actualLevel].w);

        Treespeed.Draw(ShaderInv.ShaderProgramTree, fpsTime.loop, hero.pos);

        GLES20.glUseProgram(ShaderInv.ShaderProgramLef);
        lvlbulletEffect = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramLef, "bulletEffect");
        GLES20.glUniform1f(lvlbulletEffect, bulletEffect);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramLef, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x, Gui2d.lvlTheme[gui2d.actualLevel].y, Gui2d.lvlTheme[gui2d.actualLevel].z, Gui2d.lvlTheme[gui2d.actualLevel].w);

        Treespeed.DrawLef(ShaderInv.ShaderProgramLef, fpsTime.loop, hero.pos);


        GLES20.glUseProgram(ShaderInv.ShaderProgramGrass);
        lvlbulletEffect = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramGrass, "bulletEffect");
        GLES20.glUniform1f(lvlbulletEffect, bulletEffect);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramGrass, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x, Gui2d.lvlTheme[gui2d.actualLevel].y, Gui2d.lvlTheme[gui2d.actualLevel].z, Gui2d.lvlTheme[gui2d.actualLevel].w);

        Grass.Draw(ShaderInv.ShaderProgramGrass, fpsTime.loop, hero.pos);


        GLES20.glUseProgram(ShaderInv.ShaderProgramSky);
        lvlbulletEffect = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramSky, "bulletEffect");
        GLES20.glUniform1f(lvlbulletEffect, bulletEffect);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramSky, "lvlTheme");
        GLES20.glUniform3f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x, Gui2d.lvlTheme[gui2d.actualLevel].y, Gui2d.lvlTheme[gui2d.actualLevel].z);

        Modelstatic.DrawSky(ShaderInv.ShaderProgramSky, posX);





        Vec2 vec=new Vec2();
        vec.set( MatrixInv.unproject(Bullet.ShottingPoint.x, Bullet.ShottingPoint.y, 800.0f, 480.0f) );
        Vec2 ClickPoint=new Vec2(vec.x, vec.y);

        vec.set( vec.x - hero.pos.x, vec.y - hero.pos.y );
        vec.normalize();
//vec.rotate(5.0f);


        if(hero.hp<=0) {
            //displayInterstitial();
            Globalne.Menu_Flaga = 2;
        }

        for(int i=0;i<Npc.ile;i++)
        {
//            if(Npc.co[i]!=4)
//                  if(spikes.colision(Npc.pos[i].x, Npc.pos[i].y, 0.4f, 0.4f)==1) {
//                      if (Npc.co[i] == 0) {
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f,-0.8f, 0.9f, 0);
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, 0.8f, 1.5f, 1);
//                      }
//                      if (Npc.co[i] == 1) {
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.25f,-0.8f, 0.7f, 2);
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 1.0f, 0.8f, 1.2f, 3);
//                      }
//                      if (Npc.co[i] == 2) {
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f,-0.8f, 0.6f, 4);
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, 0.8f, 1.4f, 5);
//                      }
//                      if (Npc.co[i] == 3) {
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f,-0.8f, 0.5f, 6);
//                          BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, 0.8f, 1.3f, 7);
//                      }
//                      Npc.del(i);
//                      tekstury.bulletZasob=tekstury.bulletZasob+0.5f;
//                  }





            if( Define.distanc(hero.pos.x, hero.pos.y, Npc.pos[i].x, Npc.pos[i].y) <1.0f && (Npc.co[i]==2 || Npc.co[i]==3 || Npc.co[i] ==8 || Npc.co[i] ==9 || Npc.co[i] ==14) )//zombiaki wchodza w nas
            {
                //--;
                if(Ally.ile>0)
                {
                    BoxWorld.add(Ally.pos[Ally.ile].x, Ally.pos[Ally.ile].y-0.2f, -0.8f, -0.2f, 8);
                    BoxWorld.add(Ally.pos[Ally.ile].x, Ally.pos[Ally.ile].y+0.3f, 1.5f, 1.5f, 9);
                    hero.emotion=1;
                    hero.emotionWave=1.0f;
                    if(Gui2d.actualLevel>0)Ally.DialogAdd(1);
                    Ally.del();
                }
                else
                {
                    BoxWorld.add(hero.pos.x, hero.pos.y-0.2f, -0.8f, -0.2f, 8);
                    BoxWorld.add(hero.pos.x, hero.pos.y+0.3f, 1.5f, 1.5f, 9);
                    hero.hp--;
                }

                Gui2d.Hit_wave=1.0f;
                //Effects.addBlood(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y);

                if(Npc.co[i]==2)
                {
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y-0.2f, vec.x*0.8f, vec.y*0.8f-0.2f, 4);
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.3f, vec.x*1.5f, vec.y*1.5f+0.8f, 5);
                }
                if(Npc.co[i]==3)
                {
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y-0.2f, vec.x*0.8f, vec.y*0.8f-0.2f, 6);
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.3f, vec.x*1.5f, vec.y*1.5f+0.8f, 7);
                }
                if(Npc.co[i] ==8 )
                {
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y-0.2f, vec.x*0.8f, vec.y*0.8f-0.2f, 16);
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.4f, vec.x*1.5f, vec.y*1.5f+0.8f, 17);
                }
                if( Npc.co[i] ==9)
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f + 0.8f, 18);

                if(Npc.co[i] ==11 )
                {
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x*0.8f, vec.y*0.8f-0.2f, 22);
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.3f, vec.x*1.5f, vec.y*1.5f+0.8f, 23);
                }
                if(Npc.co[i] ==13 )
                {
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 26);
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 27);
                    BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y, 0.0f, 1.0f, 1);
                    BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y, 0.2f, 0.9f, 1);
                    BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y, 0.1f, 0.9f, 1);
                    BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y,-0.3f, 0.85f, 1);
                }
                if(Npc.co[i] ==14 )
                {
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x*0.8f, vec.y*0.8f-0.2f, 5);
                    BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.3f, vec.x*1.5f, vec.y*1.5f+0.8f, 28);
                }


                Npc.del(i);
                tekstury.bulletZasob=tekstury.bulletZasob+0.5f;
                if(hero.emotion==0) {
                    hero.emotion = 2;
                    hero.emotionWave = 1.0f;
                    if(gui2d.actualLevel>0)Ally.DialogAdd(2);
                    }
            }
        }

        for(int i=0;i<BulletEnemy.ile;i++)
        {
            if( Modelstatic.colision(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y, 0.25f, 0.25f)==1 || Modelstatic.barrel.colision(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y, 0.25f, 0.25f)==1 || Modelstatic.cage.colision(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y, 0.25f, 0.25f)==1 )
            {
                Effects.addSmoke(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y);
                if(BulletEnemy.co[i]==0)sp.play(soundId4, 1, 1, 0, 0, 1);
                BulletEnemy.del(i);
            }
            if(hero.colision(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y)==1)
            {
                if(Ally.ile>0)
                {
                    BoxWorld.add(Ally.pos[Ally.ile].x, Ally.pos[Ally.ile].y-0.2f, BulletEnemy.vec[i].x*0.8f, BulletEnemy.vec[i].y*0.8f-0.2f, 9);
                    BoxWorld.add(Ally.pos[Ally.ile].x, Ally.pos[Ally.ile].y + 0.3f, BulletEnemy.vec[i].x * 1.5f, BulletEnemy.vec[i].y * 1.5f + 0.8f, 8);
                    Ally.del();
                    hero.emotion=1;
                    hero.emotionWave=1.0f;
                    if(gui2d.actualLevel>0)Ally.DialogAdd(1);
                }
                else
                {
                    BoxWorld.add(hero.pos.x, hero.pos.y-0.2f, -0.8f, -0.2f, 8);
                    BoxWorld.add(hero.pos.x, hero.pos.y+0.3f, 1.5f, 1.5f, 9);
                    hero.hp--;
                }
                Gui2d.Hit_wave=1.0f;
                Effects.addBlood(BulletEnemy.pos[i].x, BulletEnemy.pos[i].y);
                BulletEnemy.del(i);
            }
        }


        if(Npc.playBang==1) {
            sp.play(soundId4, 1, 1, 0, 0, 1);
            Npc.playBang = 0;
            BulletEnemy.add(Npc.posBang.x, Npc.posBang.y, Npc.vecBang.x, Npc.vecBang.y);     //tymaczasowo wlaczam
        }
        if(Npc.playBang==2) {
            sp.play(soundIdPuke, 1, 1, 0, 0, 1);
            Npc.playBang = 0;
            BulletEnemy.add(Npc.posBang.x, Npc.posBang.y, Npc.vecBang.x, Npc.vecBang.y, 1);     //tymaczasowo wlaczam
        }


        colPosLog.set(0.0f, 0.0f, -1);
        colNpcLog.set(0.0f, 0.0f, -1);
        //colAllyLog.set(0.0f, 0.0f, -1);
        colBarrelLog.set(0.0f, 0.0f, -1);
        colCageLog.set(0.0f, 0.0f, -1);
        //float granateL = (float) Define.distanc(hero.pos.x, hero.pos.y, ClickPoint.x, ClickPoint.y);


        if(tekstury.NdPlayerShootingStatic>0) {
            Vec2 vecNdPlayer=new Vec2(tekstury.NdPlayerShotStatic.x - tekstury.NdPlayerPosStatic.x, tekstury.NdPlayerShotStatic.y - tekstury.NdPlayerPosStatic.y);
            vecNdPlayer.normalize();

            if (tekstury.NdPlayerBronStatic != 2 && tekstury.NdPlayerBronStatic != 3)
            {
                colPosLog =  Modelstatic.colisionRay(tekstury.NdPlayerPosStatic, tekstury.NdPlayerShotStatic, 10.0f);
                sp.play(soundId2, 1, 1, 0, 0, 1);

                if(colPosLog.a==-1)
                {
                    Vec2 tmpvecPos = new Vec2(vecNdPlayer.x, vecNdPlayer.y );
                    tmpvecPos.setLength(10.0f);
                    tmpvecPos.set(tekstury.NdPlayerPosStatic.x + tmpvecPos.x, tekstury.NdPlayerPosStatic.y + tmpvecPos.y);
                    smugi.add(tekstury.NdPlayerPosStatic, tmpvecPos, tekstury.NdPlayerBronStatic);
                }
                 else
                    smugi.add(tekstury.NdPlayerPosStatic, new Vec2(colPosLog.x, colPosLog.y), tekstury.NdPlayerBronStatic);
            }
        }
        tekstury.HeroShootingStatic=0;



        colPosLog.set(0.0f, 0.0f, -1);
        colNpcLog.set(0.0f, 0.0f, -1);
        //colAllyLog.set(0.0f, 0.0f, -1);
        colBarrelLog.set(0.0f, 0.0f, -1);
        colCageLog.set(0.0f, 0.0f, -1);
        Vec2f1i colNdPlayerLog = new Vec2f1i(0.0f, 0.0f, -1);



        if(Bullet.addArrow==1 && hero.reload<0.1f)
        {
            Bullet.addArrow=0;
            hero.reload=1.0f;
            Bullet.addArrowfunc(hero.pos.x, hero.pos.y, vec.x * 0.75f, vec.y * 0.75f, 2);
            sp.play(soundId, 1, 1, 0, 0, 1);
        }




        if(Bullet.ShotingId>-1)
        {
            if(hero.bron==3)//lina
            {
                Vec2 vecPos=new Vec2();
                vecPos.set(hero.pos.x+vec.x, hero.pos.y+vec.y);
                colPosLog =  Modelstatic.colisionRayRope(hero.pos, vecPos, 7.0f);

                //if()
                //{
                if(rope.its==1 && rope.ropeDelay==0)
                    rope.del();
                else
                if( rope.ropeDelay==0)
                {
                    if(colPosLog.a!=-1 )
                    {
                        rope.add(hero.pos.x, hero.pos.y, colPosLog.x, colPosLog.y);
                        hero.tmpPos.set(0.0f,0.0f);
                    }
                    else
                    {
                        rope.addFake(hero.pos.x, hero.pos.y, vec.x, vec.y);
                    }
                }
                //}
            }



            if(hero.bron!=2 && hero.bron!=3)
                if(Bullet.add( hero.pos, vec, hero.reload)==1)
                {
                    sp.play(soundId2, 1, 1, 0, 0, 1);
                    hero.reload=1.0f;

                    for(int i=0;i<Npc.ile;i++)
                        if ( (Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y) < 11.0f && Npc.co[i]!=5) || Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y) < 8.8f )
                            if(gui2d.actualLevel>0)
                                Npc.activ[i] = 1.0f;



                    Effects.jump=1.0f;//skoki kamery

                    Vec2 vecPos=new Vec2();
                    vecPos.set(hero.pos.x+vec.x, hero.pos.y+vec.y);

                    int strzelbaFlag=1;
                    if(hero.bron==0)
                        strzelbaFlag=3;

                    for(int i=0;i<strzelbaFlag;i++)
                    {
                        if(hero.bron==0)//jesli strzelba
                        {
                            vec.rotate(-7.5f+7.5f*i);//obraca wektor 3 razy
                            vecPos.set(hero.pos.x + vec.x, hero.pos.y + vec.y);
                            //colNpcLog = Npc.colisionRay(hero.pos, vecPos, 10.0f);
                            //Effects.addShootGun(hero.pos.x, hero.pos.y, vec.x, vec.y);
                        }
                        //else

                        tekstury.HeroShotStatic.set(vecPos);
                        tekstury.HeroShootingStatic=1;


                        colNpcLog = Npc.colisionRay(hero.pos, vecPos, 10.0f);


                        colPosLog =  Modelstatic.colisionRay(hero.pos, vecPos, 10.0f);
                        //colAllyLog = Ally.colisionRay(hero.pos, vecPos,10.0f);
                        colBarrelLog = Modelstatic.barrel.colisionRay(hero.pos, vecPos,10.0f);
                        colCageLog = Modelstatic.cage.colisionRay(hero.pos, vecPos,10.0f);
                        colNdPlayerLog = hero.ndPlayerColisionRay(hero.pos, vecPos,10.0f);

                        double l1 = Define.distanc(colPosLog.x, colPosLog.y, hero.pos.x, hero.pos.y);
                        double l2 = Define.distanc(colNpcLog.x, colNpcLog.y, hero.pos.x, hero.pos.y);
                        //double l3 = Define.distanc(colAllyLog.x, colAllyLog.y, hero.pos.x, hero.pos.y);
                        double l4 = Define.distanc(colBarrelLog.x, colBarrelLog.y, hero.pos.x, hero.pos.y);
                        double l5 = Define.distanc(colCageLog.x, colCageLog.y, hero.pos.x, hero.pos.y);
                        double l6 = Define.distanc(colNdPlayerLog.x, colNdPlayerLog.y, hero.pos.x, hero.pos.y);

                        if(colPosLog.a==-1)
                            l1=l1+10000.0;
                        if(colNpcLog.a==-1)
                            l2=l2+10000.0;
//                        if(colAllyLog.a==-1)
//                            l3=l3+10000.0;
                        if(colBarrelLog.a==-1)
                            l4=l4+10000.0;
                        if(colCageLog.a==-1)
                            l5=l5+10000.0;
                        if(colNdPlayerLog.a==-1)
                            l6=l6+10000.0;

                        if( l1<l2 && l1<l4 && l1<l5 && l1<l6)//wyszukuje najlbizszy typ ktory zostal trafiony szczalem z raya
                        {
                            colNpcLog.a=-1;
                            //colAllyLog.a=-1;
                            colBarrelLog.a=-1;
                            colCageLog.a=-1;
                            colNdPlayerLog.a=-1;
                        }
                        if( l2<l1 && l2<l4 && l2<l5 && l2<l6)
                        {
                            colPosLog.a=-1;
                            //colAllyLog.a=-1;
                            colBarrelLog.a=-1;
                            colCageLog.a=-1;
                            colNdPlayerLog.a=-1;
                        }
//                        if( l3<l2 && l3<l1 && l3<l4 && l3<l5)
//                        {
//                            colPosLog.a=-1;
//                            colNpcLog.a=-1;
//                            colBarrelLog.a=-1;
//                            colCageLog.a=-1;
//                        }
                        if( l4<l2 && l4<l1  && l4<l5 && l4<l6)
                        {
                            colPosLog.a=-1;
                            //colAllyLog.a=-1;
                            colNpcLog.a=-1;
                            colCageLog.a=-1;
                            colNdPlayerLog.a=-1;
                        }
                        if( l5<l2 && l5<l1 && l5<l4 && l5<l6)
                        {
                            colPosLog.a=-1;
                            //colAllyLog.a=-1;
                            colNpcLog.a=-1;
                            colBarrelLog.a=-1;
                            colNdPlayerLog.a=-1;
                        }
                        if( l6<l2 && l6<l1 && l6<l4 && l6<l5)
                        {
                            colPosLog.a=-1;
                            //colAllyLog.a=-1;
                            colNpcLog.a=-1;
                            colBarrelLog.a=-1;
                            colCageLog.a=-1;
                        }


//                        if(hero.bron==0 && Define.distanc(hero.pos.x, hero.pos.y, colPosLog.x, colPosLog.y)<5.0f && Define.distanc(hero.pos.x, hero.pos.y, colAllyLog.x, colAllyLog.y)<5.0f && Define.distanc(hero.pos.x, hero.pos.y, colNpcLog.x, colNpcLog.y)<5.0f && Define.distanc(hero.pos.x, hero.pos.y, colBarrelLog.x, colBarrelLog.y)<5.0f );
//                        {
//                            colPosLog.a=-1;
//                            colAllyLog.a=-1;
//                            colNpcLog.a=-1;
//                            colBarrelLog.a=-1;
//                        }
//if(colAllyLog.a==-1 && colNpcLog.a==-1 && colPosLog.a==-1 && hero.bron==1) {
//    hero.smugaSet(hero.pos, new Vec2(hero.pos.x+vec.x*10.0f, hero.pos.y+vec.y*10.0f));
//}

                        if(colNdPlayerLog.a>-1)
                        {
                            smugi.add(hero.pos, new Vec2(colNdPlayerLog.x, colNdPlayerLog.y), hero.bron);
                            Effects.addBlood(colNdPlayerLog.x, colNdPlayerLog.y);
                            tekstury.hitPlayerEnemy++;
                            if(tekstury.ndhpMulti<1) {
                                hero.MyScore++;
                                Gui2d.YouKillEnemywave=1.0f;
                            }
                            //sp.play(soundIdCrack, 1, 1, 0, 0, 1);
                        }



                        if(colCageLog.a>-1)
                        {
                            //bang.add(Modelstatic.cage.pos[colCageLog.a].x, Modelstatic.cage.pos[colCageLog.a].y);

                            smugi.add(hero.pos, new Vec2(Modelstatic.cage.pos[colCageLog.a].x, Modelstatic.cage.pos[colCageLog.a].y), hero.bron);
                            BoxWorld.add(Modelstatic.cage.pos[colCageLog.a].x, Modelstatic.cage.pos[colCageLog.a].y, vec.x*0.8f, vec.y * 1.6f, 14);
                            BoxWorld.add(Modelstatic.cage.pos[colCageLog.a].x, Modelstatic.cage.pos[colCageLog.a].y,-vec.x*1.3f, vec.y * 2.2f, 15);
                            Ally.add(Modelstatic.cage.pos[colCageLog.a].x, Modelstatic.cage.pos[colCageLog.a].y);

                            Modelstatic.cage.del(colCageLog.a);
                            sp.play(soundIdCrack, 1, 1, 0, 0, 1);

                            smugi.add(hero.pos, new Vec2(colCageLog.x, colCageLog.y), hero.bron);
                        }

                        if(colBarrelLog.a>-1)
                        {
                        bang.add(Modelstatic.barrel.pos[colBarrelLog.a].x, Modelstatic.barrel.pos[colBarrelLog.a].y);
                        for(int j=0;j<Npc.ile;j++)
                            if(Define.distanc(Npc.pos[j].x, Npc.pos[j].y, Modelstatic.barrel.pos[colBarrelLog.a].x, Modelstatic.barrel.pos[colBarrelLog.a].y)<5.0f)
                                Npc.hp[j]=0;
                        Modelstatic.barrel.del(colBarrelLog.a);
                        sp.play(soundIdBang, 1, 1, 0, 0, 1);
                        if(Define.distanc(colBarrelLog.x,colBarrelLog.y,hero.pos.x,hero.pos.y)<2.5f) {
                            Vec2 tmpHeroBarrelVec = new Vec2(hero.pos.x - colBarrelLog.x, 1.0f);
                            tmpHeroBarrelVec.setLength(0.4f);
                            hero.tmpPos.set(tmpHeroBarrelVec.x, tmpHeroBarrelVec.y);
                        }
                        smugi.add(hero.pos, new Vec2(colBarrelLog.x,colBarrelLog.y), hero.bron);
                        }

//                        if(colAllyLog.a>-1 && Ally.safe[colAllyLog.a]==0)
//                        {
//                            Ally.del(colAllyLog.a);
//                            hero.emotion=1;
//                            hero.emotionWave=1.0f;
//                            if(Gui2d.actualLevel>0)hero.DialogAdd(1);
//                            BoxWorld.add(Ally.pos[colAllyLog.a].x, Ally.pos[colAllyLog.a].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 8);
//                            BoxWorld.add(Ally.pos[colAllyLog.a].x, Ally.pos[colAllyLog.a].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 9);
//                            smugi.add(hero.pos, new Vec2(colAllyLog.x,colAllyLog.y), hero.bron);
//                        }

                        if(colNpcLog.a>-1)
                        {
                            int shieldflag=0;
                            if(Npc.hp[colNpcLog.a]>0)//Npc.co[colNpcLog.a]==1 || Npc.co[colNpcLog.a]==2)
                            {

                                if(Npc.co[colNpcLog.a]==10 )//tarczownik
                                {
                                    if( colNpcLog.y>Npc.pos[colNpcLog.a].y+0.9 )
                                    {
                                        if (hero.bron == 0 && (Npc.co[colNpcLog.a] < 4 || Npc.co[colNpcLog.a] == 5))//jesli strzelba
                                            Npc.hp[colNpcLog.a] = 0;
                                        else
                                            Npc.hp[colNpcLog.a]--;
                                    }
                                    else
                                    {
                                        Effects.addSmoke(colNpcLog.x, colNpcLog.y);
                                        shieldflag=1;
                                        smugi.add(hero.pos, new Vec2(colNpcLog.x,colNpcLog.y), hero.bron);
                                    }
                                }
                                    else
                                {
                                    if (hero.bron == 0 && (Npc.co[colNpcLog.a] < 4 || Npc.co[colNpcLog.a] == 5))//jesli strzelba
                                        Npc.hp[colNpcLog.a] = 0;
                                    else
                                        Npc.hp[colNpcLog.a]--;
                                }


                                if(Npc.co[colNpcLog.a]!=4) {
                                    if (hero.pos.x < Npc.pos[colNpcLog.a].x && Npc.vec[colNpcLog.a].x < 0.045f) {
                                        if(Npc.co[colNpcLog.a]!=6){
                                            Npc.vec[colNpcLog.a].x += 0.15f;
                                            if(Npc.co[colNpcLog.a]==9){Npc.vec[colNpcLog.a].x+=0.7f; Npc.vec[colNpcLog.a].y+=0.3f;}
                                            }
                                        Npc.shake[colNpcLog.a] = 30.0f;
                                    }
                                    if (hero.pos.x > Npc.pos[colNpcLog.a].x && Npc.vec[colNpcLog.a].x > -0.045f) {
                                        if(Npc.co[colNpcLog.a]!=6){
                                            Npc.vec[colNpcLog.a].x -= 0.15f;
                                            if(Npc.co[colNpcLog.a]==9){Npc.vec[colNpcLog.a].x-=0.7f; Npc.vec[colNpcLog.a].y+=0.3f;}
                                            }
                                        Npc.shake[colNpcLog.a] = -30.0f;
                                    }
                                }
                            }
                            if(Npc.co[colNpcLog.a]!=4 && shieldflag==0) {
                                Effects.addBlood(colNpcLog.x, colNpcLog.y);
                                Effects.addBloodLine(colNpcLog.x+vec.x, colNpcLog.y, Define.katPunkty(vec.x, vec.y));
                            }
                            else{
                                Effects.addSmoke(colPosLog.x, colPosLog.y);
//                                Effects.addBlood(colNpcLog.x, colNpcLog.y);
//                                Effects.addBloodLine(colNpcLog.x, colNpcLog.y, Define.katPunkty(vec.x, vec.y));
                            }
                            smugi.add(hero.pos, new Vec2(colNpcLog.x,colNpcLog.y), hero.bron);
                        }

                        if(colPosLog.a>-1)
                        {
                            Effects.addSmoke(colPosLog.x, colPosLog.y);
                            //Tekstury.shoot(colPosLog.x, colPosLog.y);
                            smugi.add(hero.pos, new Vec2(colPosLog.x,colPosLog.y), hero.bron);
                            //hero.smugaSet(hero.pos, new Vec2(colPosLog.x, colPosLog.y));
                        }

                        if(colPosLog.a==-1 && colNpcLog.a==-1 && colBarrelLog.a==-1 && colNpcLog.a==-1 && colNdPlayerLog.a==-1)//jesli w nic nie trafil
                        {
                            if (hero.bron == 0) {
                                Vec2 tmpvecPos = new Vec2(vecPos.x - hero.pos.x, vecPos.y - hero.pos.y);
                                tmpvecPos.setLength(7.0f);
                                tmpvecPos.set(hero.pos.x + tmpvecPos.x, hero.pos.y + tmpvecPos.y);
                                smugi.add(hero.pos, tmpvecPos, hero.bron);
                            } else {
                                Vec2 tmpvecPos = new Vec2(vecPos.x - hero.pos.x, vecPos.y - hero.pos.y);
                                tmpvecPos.setLength(10.0f);
                                tmpvecPos.set(hero.pos.x + tmpvecPos.x, hero.pos.y + tmpvecPos.y);
                                smugi.add(hero.pos, tmpvecPos, hero.bron);
                                //smugi.add(hero.pos, vecPos, hero.bron);
                            }
                        }
                    }
                }
        }






if(tekstury.secNdPlayer_flag>0) {
    if (tekstury.ndhitPlayerEnemy > tekstury.hitPlayerEnemy)
        {
        tekstury.hitPlayerEnemy++;
        hero.hpMulti--;
        }
    multiReset();
}



for(int i=0;i<Npc.ile;i++) {
    if (Npc.nodeactiv[i]==0 && Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y) > 15.0f && Npc.co[i]!=7)
        Npc.activ[i] = 0.0f;
    if (Npc.hp[i] < 1) {
        if (Npc.co[i] == 0 || Npc.co[i] == 5 || Npc.co[i] == 7) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 0);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 1);
        }
        if (Npc.co[i] == 1) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.25f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 2);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 1.0f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 3);
        }
        if (Npc.co[i] == 2) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 4);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 5);
        }
        if (Npc.co[i] == 3) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 6);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 7);
        }
        if(Npc.co[i] ==8 ) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y-0.25f, vec.x*0.8f, vec.y*0.8f-0.2f, 16);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+1.0f, vec.x*1.5f, vec.y*1.5f+0.8f, 17);
        }
        if(Npc.co[i] ==9)
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y-0.25f, vec.x*0.8f, vec.y*0.8f+0.5f, 18);
        if(Npc.co[i] ==10 ) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y-0.25f, vec.x*0.8f, vec.y*0.8f-0.2f, 19);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+1.5f, vec.x*0.2f, vec.y*2.0f+0.8f, 20);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+1.0f, -vec.x*0.5f, vec.y*1.5f+0.8f, 21);
        }
        if (Npc.co[i] == 4) {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x * 0.7f, vec.y * 0.8f, 10);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x * 0.55f, vec.y * 0.5f, 11);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x * 0.4f, vec.y * 0.8f, 12);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.5f, vec.x * 0.2f, vec.y * 0.2f, 13);
            bang.add(Npc.pos[i].x, Npc.pos[i].y);
            sp.play(soundIdBang, 1, 1, 0, 0, 1);
        }
        if(Npc.co[i] ==11 )
        {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x*0.8f, vec.y*0.8f-0.2f, 22);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.3f, vec.x*1.5f, vec.y*1.5f+0.8f, 23);
            Npc.add(Npc.pos[i].x-0.75f, Npc.pos[i].y, 9, 1);Npc.hp[Npc.ile-1]--;
            Npc.add(Npc.pos[i].x, Npc.pos[i].y+2.0f, 9, 1);Npc.hp[Npc.ile-1]--;
            Npc.add(Npc.pos[i].x+0.75f, Npc.pos[i].y+0.75f, 9, 1);Npc.hp[Npc.ile-1]--;
        }

        if(Npc.co[i] ==12 )
        {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 24);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 25);
        }
        if(Npc.co[i] ==13 )
        {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y - 0.2f, vec.x * 0.8f, vec.y * 0.8f - 0.2f, 26);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y + 0.3f, vec.x * 1.5f, vec.y * 1.5f + 0.8f, 27);
            BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y, 0.0f, 1.0f, 1);
            BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y, 0.2f, 0.9f, 1);
            BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y, 0.1f, 0.9f, 1);
            BulletEnemy.add(Npc.pos[i].x, Npc.pos[i].y,-0.3f, 0.85f, 1);
        }

        if(Npc.co[i] ==14 )
        {
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y, vec.x*0.8f, vec.y*0.8f-0.2f, 5);
            BoxWorld.add(Npc.pos[i].x, Npc.pos[i].y+0.3f, vec.x*1.5f, vec.y*1.5f+0.8f, 28);
        }

        Npc.del(i);
        if(gui2d.survivemodeflag==1)
            Menager.you_kill++;
        tekstury.bulletZasob=tekstury.bulletZasob+0.5f;
        if (hero.emotion == 0) {
            hero.emotion = 2;
            if(gui2d.actualLevel>0)Ally.DialogAdd(2);
            hero.emotionWave = 1.0f;
        }
    }
}



    for(int i=0;i<guns.ile;i++)
    {
        if(Define.distanc(guns.pos[i].x, guns.pos[i].y, hero.pos.x, hero.pos.y)<1.4f)
            {
            hero.bronPosiadana[guns.co[i]]=1;
            hero.bron=guns.co[i];
            guns.del(i);
            }
    }


//        if(rope.its==0)
            hero.control(fpsTime.loop);

//        else
//        {
//            hero.controlRope(fpsTime.loop, rope.its, rope.posBegin.x, rope.posBegin.y, rope.lenght);
//            rope.posEnd.set(hero.pos.x, hero.pos.y);
//        }

//        if(hero.ropeShort==1)
//        {
//            rope.shorten(fpsTime.loop);
//            hero.ropeShort=0;
//        }
//        if(hero.ropeShort==-1)
//        {
//            rope.longer(fpsTime.loop);
//            hero.ropeShort=0;
//        }


        Vec2 posTmp = new Vec2();



        float camXBonusTranslation=0.5f;
        if(tekstury.secNdPlayer_flag==2)
            camXBonusTranslation=1.7f;

        if( tekstury.secNdPlayer_flag==0 )
            Define.CamPosGoTo.set(-hero.pos.x + camXBonusTranslation, -hero.pos.y - 1.0f);
        else
            Define.CamPosGoTo.set( Math.max(-hero.pos.x + camXBonusTranslation, -Modelstatic.EndPos.x)  , -hero.pos.y - 1.0f);

        int closeri=-1;
        float closerf=10000.0f;
        for(int i=0;i<Npc.ile;i++)
        {
            if(Define.distanc(Npc.pos[i].x,Npc.pos[i].y,hero.pos.x, hero.pos.y)<closerf)
            {
                closerf=(float)Define.distanc(Npc.pos[i].x,Npc.pos[i].y,hero.pos.x, hero.pos.y);
                closeri=i;
            }
        }

        if(closerf<10.0f)
        Define.CamPosGoTo.set(-(hero.pos.x + Npc.pos[closeri].x)*0.5f, -(hero.pos.y + Npc.pos[closeri].y)*0.5f - 1.0f);




        Menager.logic(hero.pos, fpsTime.loop);

        for(int i=0;i<Menager.ile;i++)
        {
            if(Menager.activ[i]==1 )
            {
                Define.CamPosGoTo.set(-Menager.pos[i].x, Define.CamPosGoTo.y);
                if(Menager.addNPC==1)
                {
                    Random random=new Random();
                    Menager.addNPC=0;
                    Vec2 vectmp = new Vec2(Menager.pos[i].x+9.0f-(random.nextInt(2))*18.0f , Menager.pos[i].y+5.0f);

                    if(Menager.co[i]==0) {
                        Npc.add(vectmp.x, vectmp.y, random.nextInt(2) + 2, 1);
                        Npc.activ[Npc.ile-1]=1.0f;
                    }
                    if(Menager.co[i]==1)
                    {
                        Npc.add(vectmp.x, vectmp.y, 9, 1);
                        Npc.activ[Npc.ile-1]=1.0f;
                    }
                    if(Menager.co[i]==2)
                    {
                        vectmp = new Vec2(Menager.pos[i].x+8.0f-(random.nextInt(2))*16.0f , Menager.pos[i].y+5.0f);
                        int jaki=Math.min(Menager.ladunek[i]/10, 8);
                        jaki = random.nextInt(jaki+1);
                        jaki = Menager.mena3map[jaki];
                        Npc.add(vectmp.x, vectmp.y, jaki, 1);
                        Npc.activ[Npc.ile-1]=1.0f;
                    }
                }
            }
        }

        if(gui2d.actualLevel==0 && Npc.ile>0)
            if(Npc.activ[0]>0.9f)
        Define.CamPosGoTo.set(-28.0f, Define.CamPosGoTo.y);


        Define.CamGo(fpsTime.loop);
        posTmp.set(Define.CamPos.x, Define.CamPos.y);


        if(Effects.jump>0.0f)//trzesienie kamera
        {
            posX=(float) (posTmp.x+Math.cos(hero.reload*5.0f)/8.0f);
            posY=(float) (posTmp.y+Math.sin(hero.reload*5.0f)/8.0f);
            posZ=(float) (-5.5f+Math.cos(hero.reload*5.0f)/8.0f);//5.5f
            Effects.jump-=0.1f*fpsTime.loop;
        }
        else
        {
            posX=posTmp.x;
            posY=posTmp.y;
            posZ=-5.5f;//5.5f
        }
        //posY=Math.max(-8.0f,posY);


        GLES20.glUseProgram(ShaderInv.ShaderProgramChar);
        hero.Draw(ShaderInv.ShaderProgramChar, Define.katPunkty(vec.x, vec.y));
        if(tekstury.secNdPlayer_flag!=0)
            hero.DrawGhost(ShaderInv.ShaderProgramChar);

        if(Globalne.Menu_Flaga==0)
            Npc.logic(fpsTime.loop, hero.pos, gui2d.survivemodeflag);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramChar, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x, Gui2d.lvlTheme[gui2d.actualLevel].y, Gui2d.lvlTheme[gui2d.actualLevel].z, Gui2d.lvlTheme[Gui2d.actualLevel].w);

        Ally.Draw(ShaderInv.ShaderProgramChar, hero.pos);
        Ally.logic(fpsTime.loop, hero.pos);


        GLES20.glUseProgram(ShaderInv.ShaderProgramChar);
        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramChar, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS,  1.0f+bulletEffect, 1.0f+bulletEffect, 1.0f+bulletEffect, 1.0f);


        //GLES20.glUseProgram(ShaderInv.ShaderProgramCharBlood);
        Npc.Draw(ShaderInv.ShaderProgramChar, hero.pos);

        GLES20.glUseProgram(ShaderInv.ShaderProgramFlag);
        Npc.DrawFlag(ShaderInv.ShaderProgramFlag, fpsTime.loop, hero.pos);
        //if(tekstury.secNdPlayer_flag!=0)
        //    hero.DrawMultiFlag(ShaderInv.ShaderProgramFlag, fpsTime.loop);


        lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramFlag, "lvlTheme");
        GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[gui2d.actualLevel].x, Gui2d.lvlTheme[gui2d.actualLevel].y, Gui2d.lvlTheme[gui2d.actualLevel].z, Gui2d.lvlTheme[Gui2d.actualLevel].w);
        Ally.DrawFlag(ShaderInv.ShaderProgramFlag, fpsTime.loop);
        //lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramChar, "lvlTheme");
        //GLES20.glUniform4f(lvlThemeFS, 1.0f, 1.0f, 1.0f, 1.0f);

        GLES20.glUseProgram(ShaderInv.ShaderProgramFlagH);
        if(tekstury.secNdPlayer_flag==0)
            Modelstatic.DrawEndFlag(ShaderInv.ShaderProgramFlagH);

        if(Globalne.Menu_Flaga==0)
            for(int i=0;i<Npc.ile;i++) {
                if (Modelstatic.colision(Npc.pos[i].x + Npc.vec[i].x * fpsTime.loop, Npc.pos[i].y, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0 && Modelstatic.barrel.colision(Npc.pos[i].x + Npc.vec[i].x * fpsTime.loop, Npc.pos[i].y, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0 && Modelstatic.cage.colision(Npc.pos[i].x + Npc.vec[i].x * fpsTime.loop, Npc.pos[i].y, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0)
                    Npc.pos[i].set(Npc.pos[i].x + Npc.vec[i].x * fpsTime.loop * tekstury.bulletTime, Npc.pos[i].y);
                else
                    Npc.jump[i] = 1;
                if (Modelstatic.colision(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0 && Modelstatic.barrel.colision(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0 && Modelstatic.cage.colision(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0)
                { Npc.pos[i].set(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop * tekstury.bulletTime);
                    Npc.onground[i]=0;}else {
                    Npc.onground[i]=1;
                    if (Modelstatic.colision(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop * 0.3f, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0 && Modelstatic.barrel.colision(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop * 0.3f, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0 && Modelstatic.cage.colision(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop * 0.3f, Npc.shape[Npc.co[i]].x, Npc.shape[Npc.co[i]].y) == 0)
                        Npc.pos[i].set(Npc.pos[i].x, Npc.pos[i].y + Npc.vec[i].y * fpsTime.loop * 0.2f * tekstury.bulletTime);
                }
            }

        for(int i=0;i<Ally.ile;i++)
        {
            if( Define.distanc(Ally.pos[i].x, Ally.pos[i].y, hero.pos.x, hero.pos.y)>12.0f )
                Ally.pos[i].set(hero.pos.x, hero.pos.y+10.0f);

            if( Modelstatic.colision(Ally.pos[i].x+Ally.vec[i].x*fpsTime.loop, Ally.pos[i].y, 0.45f, 0.5f) == 0 && Modelstatic.barrel.colision(Ally.pos[i].x+Ally.vec[i].x*fpsTime.loop, Ally.pos[i].y, 0.45f, 0.5f)==0  && Modelstatic.cage.colision(Ally.pos[i].x+Ally.vec[i].x*fpsTime.loop, Ally.pos[i].y, 0.45f, 0.5f)==0 )
                Ally.pos[i].set(Ally.pos[i].x+Ally.vec[i].x*fpsTime.loop*tekstury.bulletTime, Ally.pos[i].y);
            else
                Ally.jump[i]=1;
            if( Modelstatic.colision(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop, 0.45f, 0.5f) == 0 && Modelstatic.barrel.colision(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop, 0.45f, 0.5f)==0 && Modelstatic.cage.colision(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop, 0.45f, 0.5f)==0 )
                Ally.pos[i].set(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop*tekstury.bulletTime);else
            if( Modelstatic.colision(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop*0.3f, 0.45f, 0.5f) == 0 && Modelstatic.barrel.colision(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop*0.3f, 0.45f, 0.5f)==0  && Modelstatic.cage.colision(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop*0.3f, 0.45f, 0.5f)==0 )
                Ally.pos[i].set(Ally.pos[i].x, Ally.pos[i].y+Ally.vec[i].y*fpsTime.loop*0.2f*tekstury.bulletTime);
        }

        if( Modelstatic.colision(hero.pos.x, hero.pos.y, 0.45f, 0.5f) != 0 )
            hero.pos.y=hero.pos.y+0.1f;
//	if(rope.its==1)
//	{
        if( Modelstatic.colision(hero.pos.x+hero.tmpPos.x, hero.pos.y, 0.45f, 0.5f) == 0 && hero.pos.x+hero.tmpPos.x>5.0f && Modelstatic.barrel.colision(hero.pos.x+hero.tmpPos.x, hero.pos.y, 0.45f, 0.5f)==0 && hero.pos.x+hero.tmpPos.x>5.0f && (hero.pos.x+hero.tmpPos.x<Modelstatic.EndPos.x+7.0f || tekstury.secNdPlayer_flag==0) && Modelstatic.cage.colision(hero.pos.x+hero.tmpPos.x, hero.pos.y, 0.45f, 0.5f)==0 )
            if(hero.hp>0)
                {
                    hero.pos.set(hero.pos.x+hero.tmpPos.x*tekstury.bulletTime, hero.pos.y);
                }
        if( Modelstatic.colision(hero.pos.x, hero.pos.y+hero.tmpPos.y, 0.45f, 0.5f) == 0 && Npc.colision(hero.pos.x, hero.pos.y-0.5f) == -1 && Modelstatic.barrel.colision(hero.pos.x, hero.pos.y+hero.tmpPos.y, 0.45f, 0.5f)==0  && Modelstatic.cage.colision(hero.pos.x, hero.pos.y+hero.tmpPos.y, 0.45f, 0.5f)==0  )

        {
            if(hero.hp>0)hero.pos.set(hero.pos.x, hero.pos.y+hero.tmpPos.y*tekstury.bulletTime);hero.jumpFlag=1;
        }
        else
        {
            //hero.tmpPos.y=-hero.tmpPos.y*0.1f;
            hero.jumpFlag=0;
        }
        if(Npc.colision(hero.pos.x, hero.pos.y-0.5f) != -1 )
            hero.pos.y=hero.pos.y+0.3f;


//	}
//	else
//	{
//		if( Modelstatic.colision(hero.tmpPos.x, hero.pos.y, 0.45f, 0.5f) == 0 )
//			{
//			hero.pos.set(hero.tmpPos.x, hero.pos.y);
//			}
//		if( Modelstatic.colision(hero.pos.x, hero.tmpPos.y, 0.45f, 0.5f) == 0 )
//			{
//			hero.pos.set(hero.pos.x, hero.tmpPos.y);hero.jumpFlag=1;
//			}
//		else
//			{
//			hero.jumpFlag=0;
//			if( Modelstatic.colision(hero.pos.x, (hero.tmpPos.y+hero.pos.y*3.0f)/4.0f, 0.45f, 0.5f) ==0 )
//				hero.pos.set(hero.pos.x, (hero.tmpPos.y+hero.pos.y*3.0f)/4.0f);
//			}
//	}


        GLES20.glUseProgram(ShaderInv.ShaderProgramSimpleA);
        BulletEnemy.Draw(ShaderInv.ShaderProgramSimpleA, fpsTime.loop);
        Bullet.Draw(ShaderInv.ShaderProgramSimpleA);

        if(gui2d.actualLevel==0)// && Npc.ile>0)
        {
            Effects.wavepostutorial=Effects.wavepostutorial+0.025f;
            //if(Effects.wavepostutorial>1.0f)
            //{
                Effects.wavepostutorial=0.0f;
                Random random=new Random();
                int close=Npc.closest(hero.pos);
                int closeA=Modelstatic.cage.closest(hero.pos);

            float closeNpc  = (float)Define.distanc(Npc.pos[close].x ,Npc.pos[close].y, hero.pos.x, hero.pos.y);
            float closeCage = (float)Define.distanc(Modelstatic.cage.pos[closeA].x ,Modelstatic.cage.pos[closeA].y, hero.pos.x, hero.pos.y);

            if(Modelstatic.cage.ile<1)closeCage=100000.0f;
            if(Npc.ile<1)closeNpc=100000.0f;

                if(  closeNpc  <  closeCage  )
                    Effects.postutorialtmp.set(Npc.pos[close].x ,Npc.pos[close].y );
                else
                    Effects.postutorialtmp.set(Modelstatic.cage.pos[closeA].x ,Modelstatic.cage.pos[closeA].y );

            if(Modelstatic.cage.ile<1 && Npc.ile<1) Effects.postutorialtmp.set(0.0f ,100.0f );
        }


        Npc.DrawLifebar(ShaderInv.ShaderProgramSimpleA);
        if(tekstury.secNdPlayer_flag>0) {
            hero.Lifebar(ShaderInv.ShaderProgramSimpleA);
            hero.NdPlayerLifebar(ShaderInv.ShaderProgramSimpleA);
        }
//        if(rope.its==1)
//            rope.Draw(ShaderInv.ShaderProgramSimpleA,  fpsTime.loop);
//        if(rope.itsFake==1)
//            rope.DrawFake(ShaderInv.ShaderProgramSimpleA,  fpsTime.loop);
//        //Bullet.logic(fpsTime.loop);
//
//        Vec2f1i ropeLogSplit = new Vec2f1i();
//        Vec2f1i ropeLog = new Vec2f1i();
//
//        if(rope.splitIle>0)
//            ropeLogSplit = Modelstatic.colisionOdcinekRope(rope.posEnd, rope.splitPos[rope.splitIle - 1]);
//        ropeLog = Modelstatic.colisionOdcinekRope(rope.posEnd, rope.posBegin);
//
//        if( ropeLog.a > -1 && rope.its==1)
//            rope.split(ropeLog.x, ropeLog.y);
//        else
//            if( ropeLogSplit.a == -1 && rope.its==1 && rope.splitIle>0)
//                rope.join();


        GLES20.glEnable(GLES20.GL_BLEND);



        //float HeroVisaionAngle=Define.katVector(new Vec2(vec.x, vec.y));

        //GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        //GLES20.glDisable(GLES20.GL_CULL_FACE);

        //GLES20.glUseProgram(ShaderInv.ShaderProgramVision);
            for(int i=0;i<Npc.ile;i++) {
                if (Npc.co[i] == 7)
                    if (hero.pos.x>27.0f)
                        Npc.activ[i] = 1.0f;


//                if (Npc.co[i] == 2 || Npc.co[i] == 3 || Npc.co[i] == 5 || Npc.co[i] ==8 || Npc.co[i] ==9  || Npc.co[i] ==11 || Npc.co[i] ==12)
                if(Npc.co[i]>1)
                    if (Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y) < 5.0f && gui2d.actualLevel>0)
                        Npc.activ[i] = 1.0f;
                if (Npc.co[i] == 0 || Npc.co[i] == 1  || Npc.co[i] == 6)
                    if (Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y) < 15.0f) {
//                        GLES20.glUseProgram(ShaderInv.ShaderProgramVision);
//                        if(Npc.co[i] == 6)
//                            Npc.DrawVision(Npc.activ[i], ShaderInv.ShaderProgramVision, Modelstatic.coliCone(Npc.visionAngle[i], new Vec2(Npc.pos[i].x, Npc.pos[i].y + 0.3f)));
//                        else
//                            Npc.DrawVision(Npc.activ[i], ShaderInv.ShaderProgramVision, Modelstatic.coliCone(Npc.visionAngle[i], new Vec2(Npc.pos[i].x, Npc.pos[i].y + 0.3f + 0.8f * Npc.co[i])));

                        //Npc.DrawVisionTest(ShaderInv.ShaderProgramSimple, Modelstatic.coliCone(Npc.visionAngle[i], new Vec2(Npc.pos[i].x, Npc.pos[i].y+0.75f)), Npc.visionAngle[i] );
                        if (Math.abs(Define.katPunkty(Npc.pos[i].x - hero.pos.x, Npc.pos[i].y - hero.pos.y) - Npc.visionAngle[i]) < 30.0f && Modelstatic.colisionRayRope(Npc.pos[i], hero.pos, (float) Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y)).a == -1 && Define.distanc(Npc.pos[i].x, Npc.pos[i].y, hero.pos.x, hero.pos.y) < 5.0f)
                            Npc.activ[i] = Npc.activ[i] + 0.025f * fpsTime.loop;
                    }
            }
        //GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);
                  //Npc.DrawVision(ShaderInv.ShaderProgramVision, Modelstatic.coliCone(HeroVisaionAngle, hero.pos));


        //GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);
//        //    for(int i=0;i<Npc.ile;i++)
//
//
                //Npc.DrawVisionTest(ShaderInv.ShaderProgramSimple, Modelstatic.coliCone(HeroVisaionAngle, hero.pos), HeroVisaionAngle );

//        GLES20.glDisable(GLES20.GL_BLEND);
//                FontInv.DrawText3D(hero.pos, ShaderInv.ShaderProgramSimple, tekstury.texture[4], "123abcd");
//        GLES20.glEnable(GLES20.GL_BLEND);


        //GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        //GLES20.glEnable(GLES20.GL_CULL_FACE);



        GLES20.glUseProgram(ShaderInv.ShaderProgramSmoke);
        Effects.DrawSmoke(ShaderInv.ShaderProgramSmoke, fpsTime.loop);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);



        Effects.DrawBlood(ShaderInv.ShaderProgramSmoke, fpsTime.loop);
        Effects.DrawBloodLine(ShaderInv.ShaderProgramSmoke, fpsTime.loop);
        Effects.DrawBloodShootGun(ShaderInv.ShaderProgramSmoke, fpsTime.loop);
        smugi.Draw(ShaderInv.ShaderProgramSmoke, fpsTime.loop);
        BulletEnemy.DrawDym(ShaderInv.ShaderProgramSmoke);
        BulletEnemy.DrawPuke(ShaderInv.ShaderProgramSmoke, fpsTime.loop);
        bang.Draw(ShaderInv.ShaderProgramSmoke, fpsTime.loop);


        if(gui2d.actualLevel==0)
        {
        if(hero.pos.x<8.0f) Ally.DialogAdd(7);
        if(hero.pos.x>13.0f && hero.pos.x<14.0f) Ally.DialogAdd(8);
        if(hero.pos.x>19.0f && hero.pos.x<20.0f) Ally.DialogAdd(9);
        if(hero.pos.x>28.0f && hero.pos.x<29.0f) Ally.DialogAdd(10);
        if(hero.pos.x>36.0f && hero.pos.x<37.0f) Ally.DialogAdd(11);
        }


        if(Ally.dialogWave>0.1f && Ally.ile>0) {
            if(Ally.dialogCo>6)
                Ally.DrawDialog(ShaderInv.ShaderProgramSmoke);
//            if(Ally.dialogCo==0)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.GLHF));
//            if(Ally.dialogCo==1)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.Noooo));
//            if(Ally.dialogCo==2)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.IZINOOB));
//            if(Ally.dialogCo==3)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.HAHAHA));
//            if(Ally.dialogCo==4)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.Die));
//            if(Ally.dialogCo==5)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.Die));
//            if(Ally.dialogCo==6)
//                FontInv.DrawText3D(new Vec2(Ally.pos[0].x-0.5f, Ally.pos[0].y + 1.6f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.Headshot));

            if(Ally.dialogCo==7)
                FontInv.DrawText3DBig(new Vec2(Ally.pos[0].x - 0.8f, Ally.pos[0].y + 1.8f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.arrowMove));
            if(Ally.dialogCo==8)
                FontInv.DrawText3DBig(new Vec2(Ally.pos[0].x - 0.8f, Ally.pos[0].y + 1.8f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.arrowjump));
            if(Ally.dialogCo==9)
                FontInv.DrawText3DBig(new Vec2(Ally.pos[0].x - 0.8f, Ally.pos[0].y + 1.8f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.arrowShoot));
            if(Ally.dialogCo==10)
                FontInv.DrawText3DBig(new Vec2(Ally.pos[0].x - 0.8f, Ally.pos[0].y + 1.8f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.killBalon));
            if(Ally.dialogCo==11)
                FontInv.DrawText3DBig(new Vec2(Ally.pos[0].x - 0.8f, Ally.pos[0].y + 1.8f), 0.5f, ShaderInv.ShaderProgramSmoke, tekstury.texture[4], res.getString(R.string.killbarrel));
        }

        GLES20.glUseProgram(ShaderInv.ShaderProgramSimpleA);
        Effects.DrawTutorial(ShaderInv.ShaderProgramSimpleA, fpsTime.loop);

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        GLES20.glUseProgram(ShaderInv.ShaderProgramSmoke);
        Effects.DrawBloodFog(ShaderInv.ShaderProgramSmoke, fpsTime.loop);


        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        GLES20.glUseProgram(ShaderInv.ShaderProgramBulletTime);
        if(Globalne.Menu_Flaga==0)
            Gui2d.drawBulletTime(ShaderInv.ShaderProgramBulletTime, fpsTime.loop, Ally.ile);


        GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);



        if(Globalne.Menu_Flaga==0)
            Gui2d.draw(ShaderInv.ShaderProgramSimple, fpsTime.loop, Ally.ile);
        Gui2d.DrawDymek(ShaderInv.ShaderProgramSimple, hero.bron);


        if(Gui2d.YouAreDeadwave>0.5f)
            Gui2d.YouAreDead(ShaderInv.ShaderProgramSimple, fpsTime.loop);
        if(Gui2d.YouKillEnemywave>0.5f)
            Gui2d.YouKillEnemy(ShaderInv.ShaderProgramSimple, fpsTime.loop);

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glDisable(GLES20.GL_BLEND);

        //res


        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        //GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);




        if(gui2d.survivemodeflag==1) {
            FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], "you kill " + Menager.you_kill + " your record " + Menager.reckord_kill);//fpsTime.fps + " loop " + Define.dupa.x);
            if(Menager.you_kill>Menager.reckord_kill) {
                Menager.reckord_kill = Menager.you_kill;
                preferences = appContext.getSharedPreferences("danesharedpref", appContext.MODE_PRIVATE);
                SharedPreferences.Editor preferencesEditor = preferences.edit();
                preferencesEditor.putInt("reckord", Menager.reckord_kill);
                preferencesEditor.commit();
            }
        }
//        else

        //if( gui2d.survivemodeflag == 1)

            if(tekstury.secNdPlayer_flag>0) {
                //FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], "my: " + hero.HeroFlagaStatic + "tex: " + tekstury.HeroFlagaStatic + " enemy: " + tekstury.secNdPlayer_flag);
                FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], "you: " + hero.MyScore + " enemy: " + hero.ndPlayerScore);
                if (Blutacz.nazwyUrzadzen.size() > 0)
                    if(Blutacz.STATE==1)
                        FontInv.DrawTextnd(ShaderInv.ShaderProgramSimple, tekstury.texture[4], tekstury.komunikat);// +Blutacz.nazwyUrzadzen.get(0));
                    if(Blutacz.STATE==0)
                        FontInv.DrawTextnd(ShaderInv.ShaderProgramSimple, tekstury.texture[4], tekstury.komunikat);//+Blutacz.BlutaczServer.Clientname);
            }
            //else
                //FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], "fps: " + fpsTime.fps + " loop "+fpsTime.loop);
//            if(Blutacz.STATE==1) {
//                if(Blutacz.nazwyUrzadzen.size()>0)
//                    FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], Blutacz.nazwyUrzadzen.size() + " " + Blutacz.nazwyUrzadzen.get(0) + " " + tekstury.NdPlayerPosStatic.x + " " + tekstury.NdPlayerPosStatic.y );//fpsTime.fps + " loop "
//                if(Blutacz.nazwyUrzadzen.size()>1)
//                    FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4]," ,.................................................. " + Blutacz.nazwyUrzadzen.get(1)  );
//                if(Blutacz.nazwyUrzadzen.size()>2)
//                    FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4],".............................................................. " + Blutacz.nazwyUrzadzen.get(2)  );
//                if(Blutacz.nazwyUrzadzen.size()>3)
//                    FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4],".......................................................................... " + Blutacz.nazwyUrzadzen.get(3)  );
//            }
//            else
//                FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], " " + tekstury.HeroPosStatic.x + " " + tekstury.HeroPosStatic.y );


        tekstury.synhronizedBlueThread=1;
        //if(gui2d.survivemodeflag==1) {
        //    Blutacz.logic(hero.pos);
        //}




        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
    }









    @Override
    public void onDrawFrame(GL10 unused) {

        fpsTime.loopCal();
        fpsTime.FpsCal();

        if(Globalne.Menu_Flaga==4)
        {
            Gui2d.buttonsLag=Gui2d.buttonsLag-0.05f*fpsTime.loop;
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
            GLES20.glClearDepthf(1.0f);

            matrixInv.camMatrix(0.0f, 0.0f, 0.0f, -0.0f, -5.5f);

            //GLES20.glUseProgram(ShaderInv.ShaderProgramChar);
            //Gui2d.SovietDraw(ShaderInv.ShaderProgramChar,  fpsTime.loop);

            //GLES20.glUseProgram(ShaderInv.ShaderProgramGrass);
            //Gui2d.grassDraw(ShaderInv.ShaderProgramGrass,  fpsTime.loop);

            //GLES20.glUseProgram(ShaderInv.ShaderProgramTree);
            //Gui2d.DrawTree(ShaderInv.ShaderProgramTree);
            //GLES20.glUseProgram(ShaderInv.ShaderProgramLef);
            //Gui2d.DrawLeaf(ShaderInv.ShaderProgramLef);

            GLES20.glUseProgram(ShaderInv.ShaderProgram);
            Gui2d.DrawStatic(ShaderInv.ShaderProgram);

            GLES20.glUseProgram(ShaderInv.ShaderProgramSky);
            Modelstatic.DrawSky(ShaderInv.ShaderProgramSky,0.0f);

            GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);
            int iWaveVS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramSimple, "pulse");
            GLES20.glUniform1f(iWaveVS, (float) Math.sin(1.0f));

            Gui2d.drawBlue(ShaderInv.ShaderProgramSimple);
        }

        if(Globalne.Menu_Flaga==0)
            DrawGame();
        if(Globalne.Menu_Flaga==2)
        {
            DrawGame();
            GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);
            Gui2d.drawReset(ShaderInv.ShaderProgramSimple);
        }



        if(Globalne.Menu_Flaga==3) {
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
            GLES20.glClearDepthf(1.0f);

            //GLES20.glClearColor(  (float) Math.sin(Gui2d.GrassWave*0.2f)*0.7f, (float) Math.sin(Gui2d.GrassWave*0.4f)*0.9f, (float) Math.sin(Gui2d.GrassWave*0.7f), 0.0f );

            Gui2d.touchPos3d.set(MatrixInv.unproject(Gui2d.ShottingPoint.x, Gui2d.ShottingPoint.y, 800.0f, 480.0f));


            matrixInv.camMatrix(0.0f, 0.0f, Gui2d.scrollMap / 100.0f, -2.5f, -4.5f);

            GLES20.glUseProgram(ShaderInv.ShaderProgram);
            int lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgram, "lvlTheme");
            GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[0].x, Gui2d.lvlTheme[0].y, Gui2d.lvlTheme[0].z, Gui2d.lvlTheme[0].w);
            Gui2d.DrawMap(ShaderInv.ShaderProgram);

//            GLES20.glUseProgram(ShaderInv.ShaderProgramGrass);
//            lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramGrass, "lvlTheme");
//            GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[0].x, Gui2d.lvlTheme[0].y, Gui2d.lvlTheme[0].z, Gui2d.lvlTheme[0].w);
//            Gui2d.DrawMapGras(ShaderInv.ShaderProgramGrass, fpsTime.loop);
//
//            GLES20.glUseProgram(ShaderInv.ShaderProgramLef);
//            lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramLef, "lvlTheme");
//            GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[0].x, Gui2d.lvlTheme[0].y, Gui2d.lvlTheme[0].z, Gui2d.lvlTheme[0].w);
//            Gui2d.DrawMapLeaf(ShaderInv.ShaderProgramLef);
//
//            GLES20.glUseProgram(ShaderInv.ShaderProgramTree);
//            lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramTree, "lvlTheme");
//            GLES20.glUniform4f(lvlThemeFS, Gui2d.lvlTheme[0].x, Gui2d.lvlTheme[0].y, Gui2d.lvlTheme[0].z, Gui2d.lvlTheme[0].w);
//            Gui2d.DrawMapTree(ShaderInv.ShaderProgramTree);


            GLES20.glUseProgram(ShaderInv.ShaderProgramSky);
            lvlThemeFS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramSky, "lvlTheme");
            GLES20.glUniform3f(lvlThemeFS, Gui2d.lvlTheme[0].x, Gui2d.lvlTheme[0].y, Gui2d.lvlTheme[0].z);
            Modelstatic.DrawSky(ShaderInv.ShaderProgramSky, posX);


            if(Gui2d.upTouch==1)
            {
                if(Gui2d.levelState[0]>0.75f && Gui2d.levelStateInt[0]==1) {
                    reset();
                    load("0.map");
                    Ally.add(hero.pos.x, hero.pos.y);
                    Gui2d.levelState[0]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=0;
                    tekstury.loadlandscape(0);//clear
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }

                if(Gui2d.levelState[1]>0.75f && Gui2d.levelStateInt[1]==1) {//2
                    reset();
                    load("05.map");
                    Gui2d.levelState[1]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=1;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }

                if(Gui2d.levelState[2]>0.75f && Gui2d.levelStateInt[2]==1) {
                    reset();
                    load("1.map");
                    Gui2d.levelState[2]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=2;
                    tekstury.loadlandscape(2);//night
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }

                if(Gui2d.levelState[3]>0.75f && Gui2d.levelStateInt[3]==1) {//4
                    reset();
                    load("2.map");
                    Gui2d.levelState[3]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=3;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }

                if(Gui2d.levelState[4]>0.75f && Gui2d.levelStateInt[4]==1) {//5
                    reset();
                    load("3.map");
                    Gui2d.levelState[4]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=4;
                    tekstury.loadlandscape(2);//sunfall
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[5]>0.75f && Gui2d.levelStateInt[5]==1) {//6
                    reset();
                    load("4.map");
                    Gui2d.levelState[5]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=5;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[6]>0.75f && Gui2d.levelStateInt[6]==1) {
                    reset();
                    load("5.map");
                    Gui2d.levelState[6]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=6;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[7]>0.75f && Gui2d.levelStateInt[7]==1) {
                    reset();
                    load("6.map");
                    Gui2d.levelState[7]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=7;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[8]>0.75f && Gui2d.levelStateInt[8]==1) {
                    reset();
                    load("7.map");
                    Gui2d.levelState[8]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=8;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[9]>0.75f && Gui2d.levelStateInt[9]==1) {//10
                    reset();
                    load("8.map");
                    Gui2d.levelState[9]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=9;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[10]>0.75f && Gui2d.levelStateInt[10]==1) {
                    reset();
                    load("9.map");
                    Gui2d.levelState[10]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=10;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[11]>0.75f && Gui2d.levelStateInt[11]==1) {//12
                    reset();
                    load("10.map");
                    Gui2d.levelState[11]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=11;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[12]>0.75f && Gui2d.levelStateInt[12]==1) {
                    reset();
                    load("11.map");
                    Gui2d.levelState[12]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=12;
                    tekstury.loadlandscape(0);//
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[13]>0.75f && Gui2d.levelStateInt[13]==1) {
                    reset();
                    load("12.map");
                    Gui2d.levelState[13]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=13;
                    tekstury.loadlandscape(2);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[14]>0.75f && Gui2d.levelStateInt[14]==1) {//15
                    reset();
                    load("13.map");
                    Gui2d.levelState[14]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=14;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[15]>0.75f && Gui2d.levelStateInt[15]==1) {
                    reset();
                    load("14.map");
                    Gui2d.levelState[15]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=15;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[16]>0.75f && Gui2d.levelStateInt[16]==1) {
                    reset();
                    load("15.map");
                    Gui2d.levelState[16]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=16;
                    tekstury.loadlandscape(2);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[17]>0.75f && Gui2d.levelStateInt[17]==1) {
                    reset();
                    load("16.map");
                    Gui2d.levelState[17]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=17;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[18]>0.75f && Gui2d.levelStateInt[18]==1) {
                    reset();
                    load("17.map");
                    Gui2d.levelState[18]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=18;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[19]>0.75f && Gui2d.levelStateInt[19]==1) {
                    reset();
                    load("18.map");
                    Gui2d.levelState[19]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=19;
                    tekstury.loadlandscape(1);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[20]>0.75f && Gui2d.levelStateInt[20]==1) {
                    reset();
                    load("19.map");
                    Gui2d.levelState[20]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=20;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[21]>0.75f && Gui2d.levelStateInt[21]==1) {
                    reset();
                    load("20.map");
                    Gui2d.levelState[21]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=21;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[22]>0.75f && Gui2d.levelStateInt[22]==1) {
                    reset();
                    load("21.map");
                    Gui2d.levelState[22]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=22;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
                if(Gui2d.levelState[23]>0.75f && Gui2d.levelStateInt[23]==1) {
                    reset();
                    load("22.map");
                    Gui2d.levelState[23]=0.0f;
                    Globalne.Menu_Flaga=0;//gra
                    gui2d.actualLevel=23;
                    tekstury.loadlandscape(0);
                    Bullet.ShotingId=-1;
                    tekstury.secNdPlayer_flag=0;
                    mL = (MyInterstitialListener)activity;
                    mL.callback();
                }
//                if(Gui2d.levelState[22]>0.75f && Gui2d.levelStateInt[22]==1) {
//                    reset();
//                    load("21.map");
//                    Gui2d.levelState[22]=0.0f;
//                    Gui2d.Menu_Flaga=0;//gra
//                    Gui2d.actualLevel=22;
//                    tekstury.loadlandscape(0);
//                    Bullet.ShotingId=-1;
//                }
            }
            Gui2d.MapLogic(fpsTime.loop);



            GLES20.glUseProgram(ShaderInv.ShaderProgramFlagH);
            Gui2d.DrawMapFlag(ShaderInv.ShaderProgramFlagH);

            GLES20.glDisable(GLES20.GL_DEPTH_TEST);
            GLES20.glUseProgram(ShaderInv.ShaderProgramFlagHFontGui);
            Gui2d.DrawLvlNumber(ShaderInv.ShaderProgramFlagHFontGui);
            GLES20.glEnable(GLES20.GL_DEPTH_TEST);


            //Define.dupa.x= Gui2d.ShottingPoint.x/800.0f;
            //GLES20.glDisable(GLES20.GL_DEPTH_TEST);
            //GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);
            //if(Blutacz.nazwyUrzadzen.size()>0)
            //FontInv.DrawText(ShaderInv.ShaderProgramSimple, tekstury.texture[4], Blutacz.nazwyUrzadzen.get(0));
///            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        }

        if(Globalne.Menu_Flaga==1){


            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
            GLES20.glClearDepthf(1.0f);

            matrixInv.camMatrix(0.0f, 0.0f, 0.0f, -0.0f, -5.5f);

//            GLES20.glUseProgram(ShaderInv.ShaderProgramChar);
//            Gui2d.SovietDraw(ShaderInv.ShaderProgramChar,  fpsTime.loop);
//
//            GLES20.glUseProgram(ShaderInv.ShaderProgramGrass);
//            Gui2d.grassDraw(ShaderInv.ShaderProgramGrass,  fpsTime.loop);
//
//            GLES20.glUseProgram(ShaderInv.ShaderProgramTree);
//            Gui2d.DrawTree(ShaderInv.ShaderProgramTree);
//            GLES20.glUseProgram(ShaderInv.ShaderProgramLef);
//            Gui2d.DrawLeaf(ShaderInv.ShaderProgramLef);

            GLES20.glUseProgram(ShaderInv.ShaderProgram);
            Gui2d.DrawStatic(ShaderInv.ShaderProgram);

            GLES20.glUseProgram(ShaderInv.ShaderProgramSky);
            Modelstatic.DrawSky(ShaderInv.ShaderProgramSky, 0.0f);

            GLES20.glUseProgram(ShaderInv.ShaderProgramSimple);
            int iWaveVS = GLES20.glGetUniformLocation(ShaderInv.ShaderProgramSimple, "pulse");
            GLES20.glUniform1f(iWaveVS, (float) Math.sin(1.0f));
            Gui2d.drawMenu(ShaderInv.ShaderProgramSimple);
        }

    }









    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) //przy zmianie surface (na orientacji ekranu)
    {
        hero.with=width;
        hero.high=height;
//GLES20.glViewport(0, 0, width, height);
        float aspectratio=(float)(width)/(float)(height);
        matrixInv.perspectiveINV(aspectratio);
    }





    public void reset() {
    Npc.reset();
    Ally.reset();
    Grass.reset();
    Modelstatic.reset();
    bang.reset();
    Bullet.reset();
    BulletEnemy.reset();
    Effects.reset();
    hero.reset();
    Menager.reset();
    Treespeed.reset();
    spikes.reset();
    smugi.reset();
    rope.del();
    Modelstatic.barrel.reset();
    Modelstatic.cage.reset();
    guns.reset();
    Gui2d.reset();
    BoxWorld.reset();
    //tekstury.destroy();

        preferences = appContext.getSharedPreferences("danesharedpref", Activity.MODE_PRIVATE);
        Gui2d.levelStateInt[0] = 1;//preferences.getInt("level"+0, 1);
        for(int i=1;i<24;i++)
        {
            Gui2d.levelStateInt[i] = preferences.getInt("level"+i, 0); // do finala
        }

    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {//inicjacja
        GLES20.glClearColor(1.0f, 0.4f, 7.0f, 0.0f ); //(0.375f, 0.325f, 0.3f, 1.0f); (0.8f, 0.9f, 0.6f, 1.0f)
        res = appContext.getResources();


        //initialize a triangle
//GLES20.glEnable(GLES20.GL_CULL_FACE);
//GLES20.glCullFace(GLES20.GL_BACK);
//GLES20.glFrontFace(GLES20.GL_CCW);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        GLES20.glDepthFunc(GLES20.GL_LEQUAL);
        GLES20.glDepthMask(true);

        //GLES20.glDisable(GLES20.GL_LIGHTING);
//GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        sp = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);

        soundId = sp.load(appContext, R.raw.crossbow, 1);
        mPlayer = MediaPlayer.create(appContext, R.raw.crossbow);

        soundId2 = sp.load(appContext, R.raw.ak, 1);
        mPlayer = MediaPlayer.create(appContext, R.raw.ak);

        soundIdBang = sp.load(appContext, R.raw.bang, 1);
        mPlayer = MediaPlayer.create(appContext, R.raw.bang);

        soundId3 = sp.load(appContext, R.raw.hit, 1);
        mPlayer = MediaPlayer.create(appContext, R.raw.hit);

        soundId4 = sp.load(appContext, R.raw.m16, 1);
        mPlayer = MediaPlayer.create(appContext, R.raw.m16);

        soundIdCrack = sp.load(appContext, R.raw.crack, 1);
        mPlayer = MediaPlayer.create(appContext, R.raw.crack);

        soundIdPuke = sp.load(appContext, R.raw.puk, 1);
        mPlayer = MediaPlayer.create(appContext, raw.puk);

        mPlayer = MediaPlayer.create(appContext, R.raw.muslow);//te 3 to odpalenie muzyki musbullet
        mPlayer.start();
        mPlayer.setLooping(true);

        mPlayer.pause();
//        mPlayer = MediaPlayer.create(appContext, R.raw.mushi);//te 3 to odpalenie muzyki
//        mPlayer.start();
//        mPlayer.setLooping(true);

//sp.play(soundIdMusic, 1, 1, 0, 1, 1);

        MatrixInv = new matrixInv();
//back = new Back(appContext);

        BulletEnemy = new bulletEnemy(appContext);
        rope = new Rope(appContext);

        F3ds = new f3ds(appContext);
        f3ds.load();

        A3df = new a3df(appContext);
        a3df.load();

        Tekstury=new tekstury(appContext);
        tekstury.loadTekstury();
        tekstury.loadlandscape(0);

        Gui2d = new gui2d(appContext);
        //Gui2d.


        hero = new Hero(appContext);
        smugi = new Smugi(appContext);

        Grass = new grass(appContext);
        Bullet=new bullet(appContext);
        fpsTime=new FpsTime();
        guns=new Guns(appContext);
        colPosLog= new Vec2f1i();
        colNpcLog= new Vec2f1i();
        //colAllyLog= new Vec2f1i();
        colBarrelLog= new Vec2f1i();
        colCageLog= new Vec2f1i();
        Npc = new npc(appContext);
        FontInv = new fontInv(appContext);
        FontInv.init();


        Treespeed = new treespeed(appContext);
        Modelstatic = new modelstatic(appContext);


        ShaderInv = new shaderInv(appContext);
        ShaderInv.loadShaderS();


        Define=new define();
        Effects = new effects(appContext);
        bang=new Bang(appContext);
        BoxWorld = new boxWorld(appContext);
        Menager = new menager();

        Ally = new ally(appContext);

        spikes = new Spikes(appContext);


        preferences = appContext.getSharedPreferences("danesharedpref", Activity.MODE_PRIVATE);
        Gui2d.levelStateInt[0] = 1;//preferences.getInt("level"+0, 1);
        for(int i=1;i<24;i++)
        {
            Gui2d.levelStateInt[i] = preferences.getInt("level"+i, 0); // do finala
        }

        int last_openlvl=-2;
        for(int i=0;i<24;i++)
            if(Gui2d.levelStateInt[i]==1)last_openlvl++;
        Gui2d.scrollMap = -(2.0f*last_openlvl+2.0f)*100.0f;
//load();


        Blutacz=new blutacz(appContext, activity);
        globalne=new Globalne();


    }












    void load(String nazwa)
    {
        mPlayer.start();
        mPlayer.setLooping(true);



        DataInputStream is = null;
        int iii;
        float x;
        float y;
        float z;
        Vector3f pos3V=new Vector3f();


        //for(int i=0;i<tekstury.ile_tekstur;i++)
         //   tekstury.ActualtexList[i]=tekstury.ActualtexListGame[i];
        //tekstury.loadTekstury();


        try {


            is = new DataInputStream (appContext.getAssets().open(nazwa));


            iii = is.readInt();


            for(int i=0;i<iii;i++)
            {
                x = is.readFloat();
                y = is.readFloat();
                //Ally.add(x, y);
                Modelstatic.cage.add(x,y,0);
            }



            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1,itmp2,itmp3;
                x = is.readFloat();
                y = is.readFloat();

                itmp1 = is.readInt();
                itmp2 = is.readInt();
                itmp3 = is.readInt();
                Modelstatic.add( x, y, itmp1, itmp2, itmp3);
            }



            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1;
                x = is.readFloat();
                y = is.readFloat();

                itmp1 = is.readInt();
                Npc.add( x,y, itmp1,0);
            }





            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1;
                x = is.readFloat();
                y = is.readFloat();

                itmp1 = is.readInt();
                Menager.add( x,y, itmp1);
            }


            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1,itmp2,itmp3,itmp4;

                x = is.readFloat();
                y = is.readFloat();
                z = is.readFloat();


                itmp1 = is.readInt();
                itmp2 = is.readInt();
                itmp3 = is.readInt();
                itmp4 = is.readInt();

                Treespeed.add( x,y,z, itmp1, itmp2, itmp3, itmp4);
            }



            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1,itmp2;

                itmp1 = is.readInt();
                itmp2 = is.readInt();
                x = is.readFloat();
                y = is.readFloat();
                z = is.readFloat();

                Grass.add( x,y,z, itmp1, itmp2);
            }




            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1,itmp2;

                itmp1 = is.readInt();
                itmp2 = is.readInt();
                x = is.readFloat();
                y = is.readFloat();

                int shape=0;
                if(itmp1==57)
                    shape=0;
                spikes.add( x,y, itmp1, itmp2, shape);
            }


            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1;
                x = is.readFloat();
                y = is.readFloat();
                itmp1 = is.readInt();
                Modelstatic.barrel.add(x,y,itmp1);
            }

            iii = is.readInt();
            for(int i=0;i<iii;i++)
            {
                int itmp1;
                x = is.readFloat();
                y = is.readFloat();
                itmp1 = is.readInt();
                guns.add(x,y,itmp1);
            }


            Modelstatic.EndPos.x = is.readFloat();
            Modelstatic.EndPos.y = is.readFloat();

            //spikes.add(70.0f, 5.25f, 57, 29, 0);
            //spikes.add(80.0f, 5.25f, 57, 29, 0);


            rope.del();
            smugi.reset();

            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }





void multiReset()
{
if(hero.hpMulti<0)
{
    Random random=new Random();
    hero.HeroFlagaStatic=random.nextInt(8);
hero.ndPlayerScore++;
Gui2d.YouAreDeadwave=1.0f;
hero.hpMulti=3;
if(tekstury.NdPlayerPosStatic.x>31.0f)
    hero.pos.set(7.0f, 9.0f);
else
    hero.pos.set(Modelstatic.EndPos.x, 9.0f);
}

}













}

