package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class treespeed {
	static int ILE_MODEL = 20;
	Vector3f pos[];
	int co[];
	int col[];
	int tex[];
	int texl[];
	int ile;
	float wave,wave2;
	
	matrixInv MatrixInv;
	f3ds F3ds;
	
	
	void reset()
	{
	ile=0;
	}
	
	
	treespeed(Context aC)
	{
		 pos=new Vector3f[ILE_MODEL];
		 co=new int[ILE_MODEL];
		 col=new int[ILE_MODEL];
		 tex=new int[ILE_MODEL];
		 texl=new int[ILE_MODEL];
		 MatrixInv = new matrixInv();
		
		
	F3ds  = new f3ds(aC);
	ile=0;
	wave=wave2=0.0f;
	for(int i=0;i<ILE_MODEL;i++)
		{
		co[i]=-1;
		col[i]=-1;
		pos[i]=new Vector3f();
		}	
	}
	
	

	
	void add(float x, float y, float z, int c, int cl, int t, int tl)
	{
		texl[ile]=tl;
		pos[ile].set(x, y, z);
		co[ile]=c;
		col[ile]=cl;
		tex[ile]=t;
		ile++;
	}
	
	
	
	
	
	public void Draw(int ShaderProgram, float loop, Vec2 shadowPos)
	{
		loop=loop*tekstury.bulletTime;
		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);


		float[] tmpMatrix = new float[16];
		
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");


		int iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		GLES20.glUniform1f(iVwave, wave2); 
		wave2+=0.07f*loop;
		
		for(int i=0;i<ile;i++)
		{
			
			
		Matrix.setIdentityM(tmpMatrix, 0);

		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, pos[i].z);

		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);


		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);








			F3ds.DrawModel(co[i], tekstury.texture[tex[i]], ShaderProgram );
		
		}
	}
	
	
	public void DrawLef(int ShaderProgram, float loop, Vec2 shadowPos)
	{
		loop=loop*tekstury.bulletTime;
		float[] tmpMatrix = new float[16];

		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);


		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");

		int iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		GLES20.glUniform1f(iVwave, wave); 
		wave+=0.14f*loop;
		
		iVwave = GLES20.glGetUniformLocation(ShaderProgram, "wave2");
		GLES20.glUniform1f(iVwave, wave2); 
		
		for(int i=0;i<ile;i++)
		{
			
			
		Matrix.setIdentityM(tmpMatrix, 0);

		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, pos[i].z);
		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);

		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
		
	
		
		
		F3ds.DrawModel(col[i], tekstury.texture[texl[i]], ShaderProgram );
		}
	}
	

}
