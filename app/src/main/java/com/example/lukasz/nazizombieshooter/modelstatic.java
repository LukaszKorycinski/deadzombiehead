package com.example.lukasz.nazizombieshooter;

//import org.jbox2d.collision.PolygonDef;
//import org.jbox2d.common.Vec2;
//import org.jbox2d.dynamics.Body;
//import org.jbox2d.dynamics.BodyDef;
import android.content.Context;
//import android.content.SharedPreferences;
import android.opengl.Matrix;
import android.opengl.GLES20;
//import android.preference.Preference;
//import android.preference.PreferenceManager;

public class modelstatic 
{
//private static Context appContext;
 static int ILE_MODEL = 25;
 int co[];
 int chapeIndex[];
 int tex[];
 Vec2 pos[];
 Vec2 shape[];
 int ile;
 float waveSky;
 matrixInv MatrixInv;
 f3ds F3ds;
 define Define;
 Barrel barrel;
 Cage cage;
 Vec2 EndPos;

//SharedPreferences preferences;

 void reset()
 {
 ile=0;	 
 }

 
modelstatic(Context aC) {
	//preferences = aC.getSharedPreferences("SHAROWANE_PREFERENCJE", aC.MODE_PRIVATE);
	co =new int[ILE_MODEL];
	 chapeIndex=new int[ILE_MODEL];
	 tex=new int[ILE_MODEL];
	 pos=new Vec2[ILE_MODEL];
	 shape=new Vec2[10];
	barrel = new Barrel(aC);
	cage = new Cage(aC);
	
MatrixInv = new matrixInv();
Define = new define();

EndPos=new Vec2(0.0f, 0.0f);


shape[0]=new Vec2(2.5f, 1.0f);
shape[1]=new Vec2(30.0f, 1.3f);
shape[2]=new Vec2(2.2f, 1.5f);
shape[3]=new Vec2(0.45f, 0.7f);
shape[4]=new Vec2(2.8f, 1.8f);
shape[5]=new Vec2(1.6f, 2.15f);
shape[6]=new Vec2(2.7f, 0.1f);
shape[7]=new Vec2(0.2f, 1.7f);
shape[8]=new Vec2(0.7f, 0.5f);
shape[9]=new Vec2(1.7f, 1.3f);

F3ds  = new f3ds(aC);
ile=0;
waveSky=0.0f;

for(int i=0;i<ILE_MODEL;i++)
{
chapeIndex[i]=0;
co[i]=-1;
pos[i]=new Vec2();
}	

}



void DrawEnd(int ShaderProgram, Vec2 shadowPos)
{
	float[] tmpMatrix = new float[16];

	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");


	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y);

		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, EndPos.x, EndPos.y, 0.0f);
		Matrix.scaleM(tmpMatrix, 0, 0.7f, 0.7f, 0.7f);

		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


		F3ds.DrawModel(79, tekstury.texture[23], ShaderProgram);
}

void DrawEndFlag(int ShaderProgram)
{
	float[] tmpMatrix = new float[16];

	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");

	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, 1.0f, 0.1f);
		Matrix.setIdentityM(tmpMatrix, 0);


		GLES20.glUniform1f(iWaveVS, waveSky * 70.0f);
		Matrix.translateM(tmpMatrix, 0, EndPos.x, EndPos.y, 0.0f);
		//Matrix.scaleM(tmpMatrix, 0, 0.5f+levelState[i]*0.25f, 0.5f+levelState[i]*0.25f, 0.5f+levelState[i]*0.25f);
		Matrix.scaleM(tmpMatrix, 0, 0.7f, 0.7f, 0.7f);

		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

		F3ds.DrawModel(80, tekstury.texture[26], ShaderProgram);
}








public int colision(float x, float y, float sx, float sy)
{
if( y-sy<-1.0f)
	return 1;
	
for(int i=0;i<ile;i++)	
	{
	if( pos[i].x+shape[chapeIndex[i]].x > x-sx && pos[i].x-shape[chapeIndex[i]].x < x+sx 
			&&
		pos[i].y+shape[chapeIndex[i]].y > y-sy && pos[i].y-shape[chapeIndex[i]].y < y+sy )
		return 1;
	}	
	
return 0;	
}




public int colisionCircle(float x, float y, float r)
{
for(int i=0;i<ile;i++)	
	{
	float l1=Math.abs(pos[i].y-y);
	float l2=Math.abs(pos[i].x-x);
	
	if( l1<r+shape[chapeIndex[i]].y   &&  l2<r+shape[chapeIndex[i]].x  )
		return 1;
	}

return 0;	
}







//
//Vec2f1i colisionRayRope(Vec2 x, Vec2 y)
//{
//Vec2f1i w;//=new Vec2f1i();
////w.set(0.0f, 0.0f, -1);
//
//
//
//w = colisionOdcinek(x, y);
//
//
//return w;
//}















	public Vec2f1i colisionRayAlvaysTrue(Vec2 x, Vec2 y, float length)

	{
		Vec2f1i w=new Vec2f1i();
		w.set(10000.0f, 10000.0f,-1);




		float a1 = x.y-y.y;
		float a2 = x.x-y.x;
		float a =  a1/a2;
		float b = y.y-(a*y.x);

		//y=ax+b
		//x=(y-b)/a

float tolerancja = 0.001f;

		for(int i=0;i<ile;i++)
			if(chapeIndex[i]!=1)
			if( Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y) )
			{
				float MinY = a * (pos[i].x - (shape[chapeIndex[i]].x + tolerancja)) + b ;
				float MaxY = a * (pos[i].x + (shape[chapeIndex[i]].x + tolerancja)) + b ;

				float MinX = ((pos[i].y - (shape[chapeIndex[i]].y+tolerancja)) - b) / a ;
				float MaxX = ((pos[i].y + (shape[chapeIndex[i]].y+tolerancja)) - b) / a ;



				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, pos[i].x-shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (pos[i].x-shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x-shape[chapeIndex[i]].x, MinY,i);

				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, pos[i].x+shape[chapeIndex[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (pos[i].x+shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x+shape[chapeIndex[i]].x, MaxY,i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, pos[i].y-shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y-shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MinX, pos[i].y-shape[chapeIndex[i]].y,i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, pos[i].y+shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y+shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MaxX, pos[i].y+shape[chapeIndex[i]].y,i);
			}

		if( Define.distanc(x.x, x.y, w.x, w.y) < length )
			return w;
		else {
			Vec2f1i tmpVY=new Vec2f1i (y.x-x.x, y.y-x.y,-1);
			tmpVY.normalize();
			tmpVY.set(x.x+tmpVY.x*length, x.y+tmpVY.y*length,-1);

			return tmpVY;
		}
	}












	public Vec2f1i colisionRayForCone(Vec2 x, Vec2 yIn, float length)
	{
		Vec2 y=new Vec2();
		y.set(yIn);

		Vec2f1i w=new Vec2f1i();
		w.set(y.x, y.y,-1);



			float a1 = x.y - y.y;
			float a2 = x.x - y.x;
			float a = a1 / a2;
			float b = y.y - (a * y.x);
			float tolerancja = 0.001f;

			for (int i = 0; i < ile; i++)
				if ( chapeIndex[i] != 1 && Define.distanc(x.x,x.y,pos[i].x,pos[i].y)<length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y)+5.0f) {

					float MinY = a * (pos[i].x - (shape[chapeIndex[i]].x + tolerancja)) + b ;
					float MaxY = a * (pos[i].x + (shape[chapeIndex[i]].x + tolerancja)) + b ;

					float MinX = ((pos[i].y - (shape[chapeIndex[i]].y+tolerancja)) - b) / a ;
					float MaxX = ((pos[i].y + (shape[chapeIndex[i]].y+tolerancja)) - b) / a ;


					if (shape[chapeIndex[i]].y >= Math.abs(pos[i].y - MinY))
						if (Define.distanc(x.x, x.y, pos[i].x - shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
							if (((y.x - x.x > 0) == (pos[i].x - shape[chapeIndex[i]].x - x.x > 0)))
								w.set(pos[i].x - shape[chapeIndex[i]].x, MinY, i);

					if (shape[chapeIndex[i]].y >= Math.abs(pos[i].y - MaxY))
						if (Define.distanc(x.x, x.y, pos[i].x + shape[chapeIndex[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
							if (((y.x - x.x > 0) == (pos[i].x + shape[chapeIndex[i]].x - x.x > 0)))
								w.set(pos[i].x + shape[chapeIndex[i]].x, MaxY, i);

					if (shape[chapeIndex[i]].x >= Math.abs(pos[i].x - MinX))
						if (Define.distanc(x.x, x.y, MinX, pos[i].y - shape[chapeIndex[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
							if (((y.y - x.y > 0) == (pos[i].y - shape[chapeIndex[i]].y - x.y > 0)))
								w.set(MinX, pos[i].y - shape[chapeIndex[i]].y, i);

					if (shape[chapeIndex[i]].x >= Math.abs(pos[i].x - MaxX))
						if (Define.distanc(x.x, x.y, MaxX, pos[i].y + shape[chapeIndex[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
							if (((y.y - x.y > 0) == (pos[i].y + shape[chapeIndex[i]].y - x.y > 0)))
								w.set(MaxX, pos[i].y + shape[chapeIndex[i]].y, i);
				}


		for (int i = 0; i < barrel.ile; i++)
			if ( Define.distanc(x.x,x.y,barrel.pos[i].x,barrel.pos[i].y)<length+Define.Length(barrel.shape[barrel.co[i]].x, barrel.shape[barrel.co[i]].y)+5.0f) {

				float MinY = a * (barrel.pos[i].x - (barrel.shape[barrel.co[i]].x + tolerancja)) + b ;
				float MaxY = a * (barrel.pos[i].x + (barrel.shape[barrel.co[i]].x + tolerancja)) + b ;

				float MinX = ((barrel.pos[i].y - (barrel.shape[barrel.co[i]].y+tolerancja)) - b) / a ;
				float MaxX = ((barrel.pos[i].y + (barrel.shape[barrel.co[i]].y+tolerancja)) - b) / a ;


				if (barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y - MinY))
					if (Define.distanc(x.x, x.y, barrel.pos[i].x - barrel.shape[barrel.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if (((y.x - x.x > 0) == (barrel.pos[i].x - barrel.shape[barrel.co[i]].x - x.x > 0)))
							w.set(barrel.pos[i].x - barrel.shape[barrel.co[i]].x, MinY, i);

				if (barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y - MaxY))
					if (Define.distanc(x.x, x.y, barrel.pos[i].x + barrel.shape[barrel.co[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (barrel.pos[i].x + barrel.shape[barrel.co[i]].x - x.x > 0)))
							w.set(barrel.pos[i].x + barrel.shape[barrel.co[i]].x, MaxY, i);

				if (barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x - MinX))
					if (Define.distanc(x.x, x.y, MinX, barrel.pos[i].y - barrel.shape[barrel.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (barrel.pos[i].y - barrel.shape[barrel.co[i]].y - x.y > 0)))
							w.set(MinX, barrel.pos[i].y - barrel.shape[barrel.co[i]].y, i);

				if (barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x - MaxX))
					if (Define.distanc(x.x, x.y, MaxX, barrel.pos[i].y + barrel.shape[barrel.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (barrel.pos[i].y + barrel.shape[barrel.co[i]].y - x.y > 0)))
							w.set(MaxX, barrel.pos[i].y +barrel.shape[barrel.co[i]].y, i);
			}





		for (int i = 0; i < cage.ile; i++)
			if ( Define.distanc(x.x,x.y,cage.pos[i].x,cage.pos[i].y)<length+Define.Length(cage.shape[cage.co[i]].x, cage.shape[cage.co[i]].y)+5.0f) {

				float MinY = a * (cage.pos[i].x - (cage.shape[cage.co[i]].x + tolerancja)) + b ;
				float MaxY = a * (cage.pos[i].x + (cage.shape[cage.co[i]].x + tolerancja)) + b ;

				float MinX = ((cage.pos[i].y - (cage.shape[cage.co[i]].y+tolerancja)) - b) / a ;
				float MaxX = ((cage.pos[i].y + (cage.shape[cage.co[i]].y+tolerancja)) - b) / a ;


				if (cage.shape[cage.co[i]].y >= Math.abs(cage.pos[i].y - MinY))
					if (Define.distanc(x.x, x.y, cage.pos[i].x - cage.shape[cage.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if (((y.x - x.x > 0) == (cage.pos[i].x - cage.shape[cage.co[i]].x - x.x > 0)))
							w.set(cage.pos[i].x - cage.shape[cage.co[i]].x, MinY, i);

				if (cage.shape[cage.co[i]].y >= Math.abs(cage.pos[i].y - MaxY))
					if (Define.distanc(x.x, x.y, cage.pos[i].x + cage.shape[cage.co[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (cage.pos[i].x + cage.shape[cage.co[i]].x - x.x > 0)))
							w.set(cage.pos[i].x + cage.shape[cage.co[i]].x, MaxY, i);

				if (cage.shape[cage.co[i]].x >= Math.abs(cage.pos[i].x - MinX))
					if (Define.distanc(x.x, x.y, MinX, cage.pos[i].y - cage.shape[cage.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (cage.pos[i].y - cage.shape[cage.co[i]].y - x.y > 0)))
							w.set(MinX, cage.pos[i].y - cage.shape[cage.co[i]].y, i);

				if (cage.shape[cage.co[i]].x >= Math.abs(cage.pos[i].x - MaxX))
					if (Define.distanc(x.x, x.y, MaxX, cage.pos[i].y + cage.shape[cage.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (cage.pos[i].y + cage.shape[cage.co[i]].y - x.y > 0)))
							w.set(MaxX, cage.pos[i].y +cage.shape[cage.co[i]].y, i);
			}



		if(Define.distanc(x.x, x.y, w.x, w.y)>length ) {
				y.set(y.x-x.x, y.y-x.y);
				y.normalize();
				y.set(y.x*length, y.y*length);
				w.set(x.x+y.x, x.y+y.y, -1);
			}

		if(Define.distanc(y.x, y.y, w.x, w.y)<0.02f)
			w.set(w.x,w.y,-100);


			return w;
	}



	public Vec2f1i colisionRayForCone2(Vec2 x, Vec2 yIn, float length)
	{
		Vec2 y=new Vec2();
		y.set(yIn);

		Vec2f1i w=new Vec2f1i();
		w.set(10000.0f, 10000.0f,-1);



		float a1 = x.y - y.y;
		float a2 = x.x - y.x;
		float a = a1 / a2;
		float b = y.y - (a * y.x);
		float tolerancja = 0.001f;

		for (int i = 0; i < ile; i++)
			if ( chapeIndex[i] != 1 && Define.distanc(x.x,x.y,pos[i].x,pos[i].y)<length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y)+5.0f) {

				float MinY = a * (pos[i].x - (shape[chapeIndex[i]].x + tolerancja)) + b ;
				float MaxY = a * (pos[i].x + (shape[chapeIndex[i]].x + tolerancja)) + b ;

				float MinX = ((pos[i].y - (shape[chapeIndex[i]].y+tolerancja)) - b) / a ;
				float MaxX = ((pos[i].y + (shape[chapeIndex[i]].y+tolerancja)) - b) / a ;


				if (shape[chapeIndex[i]].y >= Math.abs(pos[i].y - MinY))
					if (Define.distanc(x.x, x.y, pos[i].x - shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if (((y.x - x.x > 0) == (pos[i].x - shape[chapeIndex[i]].x - x.x > 0)))
							w.set(pos[i].x - shape[chapeIndex[i]].x, MinY, i);

				if (shape[chapeIndex[i]].y >= Math.abs(pos[i].y - MaxY))
					if (Define.distanc(x.x, x.y, pos[i].x + shape[chapeIndex[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (pos[i].x + shape[chapeIndex[i]].x - x.x > 0)))
							w.set(pos[i].x + shape[chapeIndex[i]].x, MaxY, i);

				if (shape[chapeIndex[i]].x >= Math.abs(pos[i].x - MinX))
					if (Define.distanc(x.x, x.y, MinX, pos[i].y - shape[chapeIndex[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (pos[i].y - shape[chapeIndex[i]].y - x.y > 0)))
							w.set(MinX, pos[i].y - shape[chapeIndex[i]].y, i);

				if (shape[chapeIndex[i]].x >= Math.abs(pos[i].x - MaxX))
					if (Define.distanc(x.x, x.y, MaxX, pos[i].y + shape[chapeIndex[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (pos[i].y + shape[chapeIndex[i]].y - x.y > 0)))
							w.set(MaxX, pos[i].y + shape[chapeIndex[i]].y, i);
			}

		for (int i = 0; i < barrel.ile; i++)
			if ( Define.distanc(x.x,x.y,barrel.pos[i].x,barrel.pos[i].y)<length+Define.Length(barrel.shape[barrel.co[i]].x, barrel.shape[barrel.co[i]].y)+5.0f) {

				float MinY = a * (barrel.pos[i].x - (barrel.shape[barrel.co[i]].x + tolerancja)) + b ;
				float MaxY = a * (barrel.pos[i].x + (barrel.shape[barrel.co[i]].x + tolerancja)) + b ;

				float MinX = ((barrel.pos[i].y - (barrel.shape[barrel.co[i]].y+tolerancja)) - b) / a ;
				float MaxX = ((barrel.pos[i].y + (barrel.shape[barrel.co[i]].y+tolerancja)) - b) / a ;


				if (barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y - MinY))
					if (Define.distanc(x.x, x.y, barrel.pos[i].x - barrel.shape[barrel.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if (((y.x - x.x > 0) == (barrel.pos[i].x - barrel.shape[barrel.co[i]].x - x.x > 0)))
							w.set(barrel.pos[i].x - barrel.shape[barrel.co[i]].x, MinY, i);

				if (barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y - MaxY))
					if (Define.distanc(x.x, x.y, barrel.pos[i].x + barrel.shape[barrel.co[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (barrel.pos[i].x + barrel.shape[barrel.co[i]].x - x.x > 0)))
							w.set(barrel.pos[i].x + barrel.shape[barrel.co[i]].x, MaxY, i);

				if (barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x - MinX))
					if (Define.distanc(x.x, x.y, MinX, barrel.pos[i].y - barrel.shape[barrel.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (barrel.pos[i].y - barrel.shape[barrel.co[i]].y - x.y > 0)))
							w.set(MinX, barrel.pos[i].y - barrel.shape[barrel.co[i]].y, i);

				if (barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x - MaxX))
					if (Define.distanc(x.x, x.y, MaxX, barrel.pos[i].y + barrel.shape[barrel.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (barrel.pos[i].y + barrel.shape[barrel.co[i]].y - x.y > 0)))
							w.set(MaxX, barrel.pos[i].y +barrel.shape[barrel.co[i]].y, i);
			}


		for (int i = 0; i < cage.ile; i++)
			if ( Define.distanc(x.x,x.y,cage.pos[i].x,cage.pos[i].y)<length+Define.Length(cage.shape[cage.co[i]].x, cage.shape[cage.co[i]].y)+5.0f) {

				float MinY = a * (cage.pos[i].x - (cage.shape[cage.co[i]].x + tolerancja)) + b ;
				float MaxY = a * (cage.pos[i].x + (cage.shape[cage.co[i]].x + tolerancja)) + b ;

				float MinX = ((cage.pos[i].y - (cage.shape[cage.co[i]].y+tolerancja)) - b) / a ;
				float MaxX = ((cage.pos[i].y + (cage.shape[cage.co[i]].y+tolerancja)) - b) / a ;


				if (cage.shape[cage.co[i]].y >= Math.abs(cage.pos[i].y - MinY))
					if (Define.distanc(x.x, x.y, cage.pos[i].x - cage.shape[cage.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if (((y.x - x.x > 0) == (cage.pos[i].x - cage.shape[cage.co[i]].x - x.x > 0)))
							w.set(cage.pos[i].x - cage.shape[cage.co[i]].x, MinY, i);

				if (cage.shape[cage.co[i]].y >= Math.abs(cage.pos[i].y - MaxY))
					if (Define.distanc(x.x, x.y, cage.pos[i].x + cage.shape[cage.co[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (cage.pos[i].x + cage.shape[cage.co[i]].x - x.x > 0)))
							w.set(cage.pos[i].x + cage.shape[cage.co[i]].x, MaxY, i);

				if (cage.shape[cage.co[i]].x >= Math.abs(cage.pos[i].x - MinX))
					if (Define.distanc(x.x, x.y, MinX, cage.pos[i].y - cage.shape[cage.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (cage.pos[i].y - cage.shape[cage.co[i]].y - x.y > 0)))
							w.set(MinX, cage.pos[i].y - cage.shape[cage.co[i]].y, i);

				if (cage.shape[cage.co[i]].x >= Math.abs(cage.pos[i].x - MaxX))
					if (Define.distanc(x.x, x.y, MaxX, cage.pos[i].y + cage.shape[cage.co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (cage.pos[i].y + cage.shape[cage.co[i]].y - x.y > 0)))
							w.set(MaxX, cage.pos[i].y +cage.shape[cage.co[i]].y, i);
			}



		if(w.a==-1 || Define.distanc(x.x, x.y, w.x, w.y)>length) {
				y.set(y.x-x.x, y.y-x.y);
				y.normalize();
				y.set(y.x*length, y.y*length);
				w.set(x.x+y.x, x.y+y.y, -1);
			}




		return w;
	}




	public Vec2f1i colisionRay(Vec2 x, Vec2 y, float length)
	{
		Vec2f1i w=new Vec2f1i();
		w.set(10000.0f, 10000.0f, -1);

		float a1 = x.y-y.y;
		float a2 = x.x-y.x;
		float a =  a1/a2;
		float b = y.y-(a*y.x);
		//y=ax+b
		//x=(y-b)/a
	for(int i=0;i<ile;i++)
		if( Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y) )
		{
			float MinY = a*(pos[i].x-shape[chapeIndex[i]].x)+b;
			float MaxY = a*(pos[i].x+shape[chapeIndex[i]].x)+b;

			float MinX = ((pos[i].y-shape[chapeIndex[i]].y)-b)/a;
			float MaxX = ((pos[i].y+shape[chapeIndex[i]].y)-b)/a;



			if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MinY) )
				if( Define.distanc( x.x, x.y, pos[i].x-shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
					if(  ((y.x-x.x > 0) == (pos[i].x-shape[chapeIndex[i]].x-x.x > 0))  )
						w.set( pos[i].x-shape[chapeIndex[i]].x, MinY, i);

			if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MaxY) )
				if( Define.distanc( x.x, x.y, pos[i].x+shape[chapeIndex[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
					if(  ((y.x-x.x > 0) == (pos[i].x+shape[chapeIndex[i]].x-x.x > 0))  )
						w.set( pos[i].x+shape[chapeIndex[i]].x, MaxY, i);

			if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MinX) )
				if( Define.distanc( x.x, x.y, MinX, pos[i].y-shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
					if(  ((y.y-x.y > 0) == (pos[i].y-shape[chapeIndex[i]].y-x.y > 0))  )
						w.set( MinX, pos[i].y-shape[chapeIndex[i]].y, i);

			if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MaxX) )
				if( Define.distanc( x.x, x.y, MaxX, pos[i].y+shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
					if(  ((y.y-x.y > 0) == (pos[i].y+shape[chapeIndex[i]].y-x.y > 0))  )
						w.set( MaxX, pos[i].y+shape[chapeIndex[i]].y, i);
		}

		if( Define.distanc(x.x, x.y, w.x, w.y) < length )
			return w;
		else
			return new Vec2f1i(0.0f, 0.0f, -1);
	}









	public Vec2f1i colisionOdcinek(Vec2 x, Vec2 y)
	{
		Vec2f1i w=new Vec2f1i();
		w.set(10000.0f, 10000.0f, -1);
		double length = Define.distanc(x.x, x.y, y.x, y.y);



		float a1 = x.y-y.y;
		float a2 = x.x-y.x;
		float a =  a1/a2;
		float b = y.y-(a*y.x);

		//y=ax+b
		//x=(y-b)/a



		for(int i=0;i<ile;i++)
			if( Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y) )
			{
				float MinY = a*(pos[i].x-shape[chapeIndex[i]].x)+b;
				float MaxY = a*(pos[i].x+shape[chapeIndex[i]].x)+b;

				float MinX = ((pos[i].y-shape[chapeIndex[i]].y)-b)/a;
				float MaxX = ((pos[i].y+shape[chapeIndex[i]].y)-b)/a;



				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, pos[i].x-shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (pos[i].x-shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x-shape[chapeIndex[i]].x, MinY, i);

				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, pos[i].x+shape[chapeIndex[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (pos[i].x+shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x+shape[chapeIndex[i]].x, MaxY, i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, pos[i].y-shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y-shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MinX, pos[i].y-shape[chapeIndex[i]].y, i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, pos[i].y+shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y+shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MaxX, pos[i].y+shape[chapeIndex[i]].y, i);
			}

		if( Define.distanc(x.x, x.y, w.x, w.y) < length )
			return w;
		else
			return new Vec2f1i(0.0f, 0.0f, -1);

	}



	public Vec2f1i colisionRayRope(Vec2 x, Vec2 y, float length)
	{
		Vec2f1i w=new Vec2f1i();
		w.set(10000.0f, 10000.0f, -1);

		float a1 = x.y-y.y;
		float a2 = x.x-y.x;
		float a =  a1/a2;
		float b = y.y-(a*y.x);
		//y=ax+b
		//x=(y-b)/a
		for(int i=0;i<ile;i++)
			if( Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y) )
			{
				float MinY = a*(pos[i].x-shape[chapeIndex[i]].x)+b;
				float MaxY = a*(pos[i].x+shape[chapeIndex[i]].x)+b;

				float MinX = ((pos[i].y-shape[chapeIndex[i]].y)-b)/a;
				float MaxX = ((pos[i].y+shape[chapeIndex[i]].y)-b)/a;



				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, pos[i].x-shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (pos[i].x-shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x-shape[chapeIndex[i]].x, MinY, i);

				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, pos[i].x+shape[chapeIndex[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (pos[i].x+shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x+shape[chapeIndex[i]].x, MaxY, i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, pos[i].y-shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y-shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MinX, pos[i].y-shape[chapeIndex[i]].y, i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, pos[i].y+shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y+shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MaxX, pos[i].y+shape[chapeIndex[i]].y, i);
			}

		for(int i=0;i<barrel.ile;i++)
			if( Define.distanc(x.x, x.y, barrel.pos[i].x, barrel.pos[i].y) < length+Define.Length(barrel.shape[barrel.co[i]].x, barrel.shape[barrel.co[i]].y) )
			{
				float MinY = a*(barrel.pos[i].x-barrel.shape[barrel.co[i]].x)+b;
				float MaxY = a*(barrel.pos[i].x+barrel.shape[barrel.co[i]].x)+b;

				float MinX = ((barrel.pos[i].y-barrel.shape[barrel.co[i]].y)-b)/a;
				float MaxX = ((barrel.pos[i].y+barrel.shape[barrel.co[i]].y)-b)/a;



				if( barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, barrel.pos[i].x-barrel.shape[barrel.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (barrel.pos[i].x-barrel.shape[barrel.co[i]].x-x.x > 0))  )
							w.set( barrel.pos[i].x-barrel.shape[barrel.co[i]].x, MinY, i);

				if( barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, barrel.pos[i].x+barrel.shape[barrel.co[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (barrel.pos[i].x+barrel.shape[barrel.co[i]].x-x.x > 0))  )
							w.set( barrel.pos[i].x+barrel.shape[barrel.co[i]].x, MaxY, i);

				if( barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, barrel.pos[i].y-barrel.shape[barrel.co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (barrel.pos[i].y-barrel.shape[barrel.co[i]].y-x.y > 0))  )
							w.set( MinX, barrel.pos[i].y-barrel.shape[barrel.co[i]].y, i);

				if( barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, barrel.pos[i].y+barrel.shape[barrel.co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (barrel.pos[i].y+barrel.shape[barrel.co[i]].y-x.y > 0))  )
							w.set( MaxX, barrel.pos[i].y+barrel.shape[barrel.co[i]].y, i);
			}


		for(int i=0;i<cage.ile;i++)
			if( Define.distanc(x.x, x.y, cage.pos[i].x, cage.pos[i].y) < length+Define.Length(cage.shape[cage.co[i]].x, cage.shape[cage.co[i]].y) )
			{
				float MinY = a*(cage.pos[i].x-cage.shape[cage.co[i]].x)+b;
				float MaxY = a*(cage.pos[i].x+cage.shape[cage.co[i]].x)+b;

				float MinX = ((cage.pos[i].y-cage.shape[cage.co[i]].y)-b)/a;
				float MaxX = ((cage.pos[i].y+cage.shape[cage.co[i]].y)-b)/a;



				if( cage.shape[cage.co[i]].y >= Math.abs(cage.pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, cage.pos[i].x-cage.shape[cage.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (cage.pos[i].x-cage.shape[cage.co[i]].x-x.x > 0))  )
							w.set( cage.pos[i].x-cage.shape[cage.co[i]].x, MinY, i);

				if( cage.shape[cage.co[i]].y >= Math.abs(cage.pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, cage.pos[i].x+cage.shape[cage.co[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (cage.pos[i].x+cage.shape[cage.co[i]].x-x.x > 0))  )
							w.set( cage.pos[i].x+cage.shape[cage.co[i]].x, MaxY, i);

				if( cage.shape[cage.co[i]].x >= Math.abs(cage.pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, cage.pos[i].y-cage.shape[cage.co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (cage.pos[i].y-cage.shape[cage.co[i]].y-x.y > 0))  )
							w.set( MinX, cage.pos[i].y-cage.shape[cage.co[i]].y, i);

				if( cage.shape[cage.co[i]].x >= Math.abs(cage.pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, cage.pos[i].y+cage.shape[cage.co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (cage.pos[i].y+cage.shape[cage.co[i]].y-x.y > 0))  )
							w.set( MaxX, cage.pos[i].y+cage.shape[cage.co[i]].y, i);
			}



		if( Define.distanc(x.x, x.y, w.x, w.y) < length )
			return w;
		else
			return new Vec2f1i(0.0f, 0.0f, -1);
	}




	public Vec2f1i colisionOdcinekRope(Vec2 x, Vec2 y)
	{
		Vec2f1i w=new Vec2f1i();
		w.set(10000.0f, 10000.0f, -1);
		double length = Define.distanc(x.x, x.y, y.x, y.y);



		float a1 = x.y-y.y;
		float a2 = x.x-y.x;
		float a =  a1/a2;
		float b = y.y-(a*y.x);

		//y=ax+b
		//x=(y-b)/a



		for(int i=0;i<ile;i++)
			if( Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length+Define.Length(shape[chapeIndex[i]].x, shape[chapeIndex[i]].y) )
			{
				float MinY = a*(pos[i].x-shape[chapeIndex[i]].x)+b;
				float MaxY = a*(pos[i].x+shape[chapeIndex[i]].x)+b;

				float MinX = ((pos[i].y-shape[chapeIndex[i]].y)-b)/a;
				float MaxX = ((pos[i].y+shape[chapeIndex[i]].y)-b)/a;



				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, pos[i].x-shape[chapeIndex[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (pos[i].x-shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x-shape[chapeIndex[i]].x, MinY, i);

				if( shape[chapeIndex[i]].y >= Math.abs(pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, pos[i].x+shape[chapeIndex[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (pos[i].x+shape[chapeIndex[i]].x-x.x > 0))  )
							w.set( pos[i].x+shape[chapeIndex[i]].x, MaxY, i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, pos[i].y-shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y-shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MinX, pos[i].y-shape[chapeIndex[i]].y, i);

				if( shape[chapeIndex[i]].x >= Math.abs(pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, pos[i].y+shape[chapeIndex[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (pos[i].y+shape[chapeIndex[i]].y-x.y > 0))  )
							w.set( MaxX, pos[i].y+shape[chapeIndex[i]].y, i);
			}


		for(int i=0;i<barrel.ile;i++)
			if( Define.distanc(x.x, x.y, barrel.pos[i].x, barrel.pos[i].y) < length+Define.Length(barrel.shape[barrel.co[i]].x, barrel.shape[barrel.co[i]].y) )
			{
				float MinY = a*(barrel.pos[i].x-barrel.shape[barrel.co[i]].x)+b;
				float MaxY = a*(barrel.pos[i].x+barrel.shape[barrel.co[i]].x)+b;

				float MinX = ((barrel.pos[i].y-barrel.shape[barrel.co[i]].y)-b)/a;
				float MaxX = ((barrel.pos[i].y+barrel.shape[barrel.co[i]].y)-b)/a;



				if( barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y-MinY) )
					if( Define.distanc( x.x, x.y, barrel.pos[i].x-barrel.shape[barrel.co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y) )
						if(  ((y.x-x.x > 0) == (barrel.pos[i].x-barrel.shape[barrel.co[i]].x-x.x > 0))  )
							w.set( barrel.pos[i].x-barrel.shape[barrel.co[i]].x, MinY, i);

				if( barrel.shape[barrel.co[i]].y >= Math.abs(barrel.pos[i].y-MaxY) )
					if( Define.distanc( x.x, x.y, barrel.pos[i].x+barrel.shape[barrel.co[i]].x, MaxY) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.x-x.x > 0) == (barrel.pos[i].x+barrel.shape[barrel.co[i]].x-x.x > 0))  )
							w.set( barrel.pos[i].x+barrel.shape[barrel.co[i]].x, MaxY, i);

				if( barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x-MinX) )
					if( Define.distanc( x.x, x.y, MinX, barrel.pos[i].y-barrel.shape[barrel.co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (barrel.pos[i].y-barrel.shape[barrel.co[i]].y-x.y > 0))  )
							w.set( MinX, barrel.pos[i].y-barrel.shape[barrel.co[i]].y, i);

				if( barrel.shape[barrel.co[i]].x >= Math.abs(barrel.pos[i].x-MaxX) )
					if( Define.distanc( x.x, x.y, MaxX, barrel.pos[i].y+barrel.shape[barrel.co[i]].y) < Define.distanc( x.x, x.y , w.x, w.y ) )
						if(  ((y.y-x.y > 0) == (barrel.pos[i].y+barrel.shape[barrel.co[i]].y-x.y > 0))  )
							w.set( MaxX, barrel.pos[i].y+barrel.shape[barrel.co[i]].y, i);
			}



		if( Define.distanc(x.x, x.y, w.x, w.y) < length )
			return w;
		else
			return new Vec2f1i(0.0f, 0.0f, -1);

	}














void add(float x, float y, int c, int t, int si)
{
	tex[ile]=t;
	pos[ile].set(x,y);
	co[ile]=c;
	chapeIndex[ile]=si;
	ile++;
}



void del(int j)
{
for(int i=j; i<ile-1; i++)
{
tex[i]=tex[i+1];
pos[i].set(pos[i+1].x,pos[i+1].y);
co[i]=co[i+1];
chapeIndex[i]=chapeIndex[i+1];
}
ile--;
}





public void Draw(int ShaderProgram, Vec2 shadowPos)
{
	float[] tmpMatrix = new float[16];

	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	

	
	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);
	
	for(int i=0;i<ile;i++)
		if(Define.distanc(pos[i].x, pos[i].y, shadowPos.x, shadowPos.y)<15.0f || chapeIndex[i]==1)
		{
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
		
		GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0); 
		
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
		

		int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture");
		GLES20.glUniform1i(texLoc, 0);
		
		texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texshoot");
		GLES20.glUniform1i(texLoc, 1);


			GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[tex[i]]);
		
			//GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
			//GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.dynamicTexture[0]);
			
			//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

		F3ds.DrawModel(co[i], ShaderProgram );
		}
}






void DrawSky(int ShaderProgram, float xPosCam)
{
	float[] tmpMatrix = new float[16];
	
	

	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

	waveSky+=0.001f;
	int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	GLES20.glUniform1f(iWaveVS, waveSky);
	
	Matrix.setIdentityM(tmpMatrix, 0);

	Matrix.translateM(tmpMatrix, 0, 0.0f, -5.8f, -12.0f);
	Matrix.scaleM(tmpMatrix, 0, 2.5f, 2.25f, 1.0f);
	
	//Matrix.multiplyMM(tmpMatrix, 0, ViewProjMatrix, 0, tmpMatrix, 0);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);




	int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture");
	GLES20.glUniform1i(texLoc, 0);

	texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture2");
	GLES20.glUniform1i(texLoc, 1);

	texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texture3");
	GLES20.glUniform1i(texLoc, 2);

	texLoc = GLES20.glGetUniformLocation(ShaderProgram, "xpos");
	GLES20.glUniform1f(texLoc, xPosCam*0.015f);

	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[6]);

	GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.Skytex[0]);

	GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[20]);

	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);



	F3ds.DrawModel(3, ShaderProgram );//
}
//
//void DrawBack(int ShaderProgram)
//{
//	float[] tmpMatrix = new float[16];
//	
//	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
//	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
//
//	
//	
//	Matrix.setIdentityM(tmpMatrix, 0);
//
//	Matrix.translateM(tmpMatrix, 0, 0.0f, 0.0f, 0.0f);
//
//	//Matrix.multiplyMM(tmpMatrix, 0, ViewProjMatrix, 0, tmpMatrix, 0);
//	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
//	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
//	
//	Matrix.setIdentityM(tmpMatrix, 0);
//	Matrix.translateM(tmpMatrix, 0, 0.0f, 0.0f, 0.0f);
//	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.ViewMatrix, 0, tmpMatrix, 0);
//	GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0); 
//	
//	
//
//int texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texmap");
//GLES20.glUniform1i(texLoc, 0);
//
//texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texred");
//GLES20.glUniform1i(texLoc, 1);
//
//texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texgray");
//GLES20.glUniform1i(texLoc, 2);
//	
//texLoc = GLES20.glGetUniformLocation(ShaderProgram, "texgreen");
//GLES20.glUniform1i(texLoc, 3);
//	
//	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[1]);
//
//	GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
//	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[4]);
//
//	GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
//	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[5]); 
//	
//	GLES20.glActiveTexture(GLES20.GL_TEXTURE3);
//	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[7]); 
//	
//	GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//	
//	F3ds.DrawModel(4, ShaderProgram );
//}


//int NpcActiv()
//{
//
//}



//	Vector<Vec2> coliCone(float angle, Vec2 A)
//	{
//		float zasieg=5.0f;
//		Vector<Vec2> wynik;
//		wynik=new Vector<Vec2>();
//
//		Vec2 B = new Vec2();
//		Vec2 C = new Vec2();
//		Vec2 Front = new Vec2();
//
//		B.x=zasieg;
//		C.x=zasieg;
//		Front.x=zasieg;
//
//		B.set(Define.rotatePoint(B, angle+30.0f));//punkty skrajne
//		C.set(Define.rotatePoint(C, angle-30.0f));//punkty skrajne
//		Front.set(Define.rotatePoint(Front, angle));//punkty skrajne
//
//		B.set(B.x+A.x, B.y+A.y);//punkty skrajne
//		C.set(C.x + A.x, C.y + A.y);//punkty skrajne
//		//Front.set(Front.x + A.x, Front.y + A.y);//punkty skrajne
//		wynik.add(A);//pozycja oka
//		//wynik.add(new Vec2(Front.x+A.x, Front.y+A.y));
//
//
//		Vec2f1i ABEdge = colisionRayAlvaysTrue(A, B, zasieg);//puntky skrajne po kolizji
//		Vec2f1i ACEdge = colisionRayAlvaysTrue(A, C, zasieg);//puntky skrajne po kolizji
//
//		wynik.add(new Vec2(ABEdge.x,ABEdge.y));
//		wynik.add(new Vec2(ACEdge.x, ACEdge.y));
//
//
//		Vec2 Btmp = new Vec2();
//
//
//		for(int i=0;i<ile;i++)
//		{
//			int plusMinus=1;
//			int plusMinus2=1;
//			for(int j=0;j<4;j++)
//			{
//				plusMinus=-plusMinus;
//				if(j>1)plusMinus2=-1;
//				if(Define.distanc(A.x, A.y, pos[i].x+shape[chapeIndex[i]].x*plusMinus, pos[i].y+shape[chapeIndex[i]].y*plusMinus2)<zasieg+5.0f
//						&&
//					chapeIndex[i] != 1
//						&&
//				   Define.kat2Vector(Front, new Vec2((pos[i].x + shape[chapeIndex[i]].x * plusMinus) - A.x, (pos[i].y + shape[chapeIndex[i]].y * plusMinus2) - A.y))<30.0f
//				   )
//				{
//
//					B.set(pos[i].x+shape[chapeIndex[i]].x*plusMinus, pos[i].y+shape[chapeIndex[i]].y*plusMinus2);//punkty skrajne
//					ABEdge = colisionRayForCone(A, B, zasieg);//puntky skrajne po kolizji
//					wynik.add(new Vec2(ABEdge.x,ABEdge.y));
//
//					if(ABEdge.a==-100)
//					{
//						Btmp.set(B.x-A.x, B.y-A.y);//punkty skrajne
//						Btmp.set(Define.rotatePoint(Btmp, 4.0f));
//						Btmp.set(Btmp.x+A.x, Btmp.y+A.y);//punkty skrajne
//						ABEdge = colisionRayForCone2(A, Btmp, zasieg);//puntky skrajne po kolizji
//						wynik.add(new Vec2(ABEdge.x,ABEdge.y));
//
//						Btmp.set(B.x-A.x, B.y-A.y);//punkty skrajne
//						Btmp.set(Define.rotatePoint(Btmp, -4.0f));
//						Btmp.set(Btmp.x+A.x, Btmp.y+A.y);//punkty skrajne
//						ABEdge = colisionRayForCone2(A, Btmp, zasieg);//puntky skrajne po kolizji
//						wynik.add(new Vec2(ABEdge.x,ABEdge.y));
//					}
//				}
//			}
//		}
//
//
//
////sortowanie BEGIN
//float angleVec[];
//angleVec = new float[wynik.size()];
//
//
////Vec2 vecAngle=new Vec2(zasieg, 0.0f);
////		vecAngle.set(Define.rotatePoint(vecAngle, angle));
////		vecAngle.normalize();
//
//for(int i=0; i<wynik.size(); i++) {
//	Vec2 tmp = new Vec2(zasieg, 0.0f);
//	tmp.set(Define.rotatePoint(tmp, angle-35.0f));//punkty skrajne
//	//tmp.normalize();
//
//	Vec2 tmp2 = new Vec2(wynik.get(i).x - A.x, wynik.get(i).y - A.y);
//
//	angleVec[i] = Define.kat2Vector(tmp,tmp2);
//}
//
//
//
//
//int sortDone=1;
//while(sortDone!=0)
//{
//	sortDone=0;
//
//	for(int i=2; i<wynik.size(); i++)//od 2 bo pomija A
//		if(angleVec[i]>angleVec[i-1])
//		{
//			sortDone=1;
//			float tmp=angleVec[i];
//			angleVec[i]=angleVec[i-1];
//			angleVec[i-1]=tmp;
//
//			Collections.swap(wynik,i,i-1);
//
//		}
//
//}
//
//
//	return wynik;
//	}

}










