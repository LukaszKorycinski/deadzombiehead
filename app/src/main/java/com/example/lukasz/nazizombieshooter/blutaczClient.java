package com.example.lukasz.nazizombieshooter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.bluetooth.BluetoothAdapter;

/**
 * Created by Lukasz on 2016-08-16.
 */
public class blutaczClient extends Thread {
    private  BluetoothSocket mmSocket;
    private  BluetoothDevice mmDevice;
    int state;

    public blutaczClient(BluetoothDevice device) {
        state=0;
        BluetoothSocket tmp = null;
        mmDevice = device;
        try {
            UUID uuid = UUID.fromString("550e8400-e29b-41d4-a716-446677880000");
            tmp = device.createRfcommSocketToServiceRecord(uuid);
        } catch (Exception e) { }
        mmSocket = tmp;
    }


    BufferedReader in;
    PrintWriter out;

    public void run() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothAdapter.cancelDiscovery();
        while (true) {


                if (state == 0) {
                    //Log.d("INFO", "Proba poczenia....");
                    try {
                    mmSocket.connect();


                    //Log.d("INFO", "Poczono z serwerem!");
                    state = 1;

                    } catch (Exception ce) {
                        //Log.d("INFO", "nie polaczono, status="+state);
                        try {mmSocket.close();}catch (Exception ca) {Log.d("INFO", "zamknieto socket");}}

                    try {
                    in = new BufferedReader(new InputStreamReader(mmSocket.getInputStream()));
                    //Log.d("INFO", "utwrzono strumien klienta");
                    } catch (Exception ce) {
                        Log.d("INFO", "fail przy tworzeniu strumien klienta");}

                    try {
                    out = new PrintWriter(mmSocket.getOutputStream(),true);
                    } catch (Exception ce) {
                        Log.d("INFO", "fail strumieniu IN klienta");}

                }

                    try {

                if(state==1 && tekstury.synhronizedBlueThread==1) {
                    //String input = in.readLine();
                    out.println(tekstury.HeroPosStatic.x+" "+tekstury.HeroPosStatic.y+" "+tekstury.HeroAngleStatic+" "+tekstury.HeroreloadStatic+" "+tekstury.HeroAnimStatic+" "
                            +tekstury.HeroShootingStatic+" "+tekstury.HeroShotStatic.x+" "+tekstury.HeroShotStatic.y+" "+tekstury.hitPlayerEnemy+" "+tekstury.hpMulti+" "+tekstury.HeroFlagaStatic);
                    //Log.d("INFO", "Wyslalem dane");

                    String input = in.readLine();
                    //tekstury.NdPlayerPosStatic.x = Float.parseFloat(input);

                    String delims = " ";
                    String[] tokens = input.split(delims);

                    //Log.d("INFO", "odczytuje....");

                    //tekstury.dupastring = input;
                    tekstury.NdPlayerPosStatic.x = Float.parseFloat(tokens[0]);
                    tekstury.NdPlayerPosStatic.y = Float.parseFloat(tokens[1]);
                    tekstury.NdPlayerAngleStatic = Float.parseFloat(tokens[2]);
                    //tekstury.NdPlayerBronStatic = Integer.parseInt(tokens[3]);
                    tekstury.NdPlayerreloadStatic = Float.parseFloat(tokens[3]);
                    tekstury.NdPlayerAnimStatic = Float.parseFloat(tokens[4]);
                    tekstury.NdPlayerShootingStatic = Integer.parseInt(tokens[5]);
                    tekstury.NdPlayerShotStatic.x = Float.parseFloat(tokens[6]);
                    tekstury.NdPlayerShotStatic.y = Float.parseFloat(tokens[7]);
                    tekstury.ndhitPlayerEnemy = Integer.parseInt(tokens[8]);
                    tekstury.ndhpMulti = Integer.parseInt(tokens[9]);
                    tekstury.NdPlayerFlagaStatic = Integer.parseInt(tokens[10]);

                    tekstury.synhronizedBlueThread=0;
                    //in.close();
                }
                //input = in.readLine();
                //Log.d("INFO", "odczytano2");
                //tekstury.NdPlayerPosStatic.y = Float.parseFloat(input);


            } catch (Exception ce) {
               // Log.d("INFO", "cos nie dziala w odczycie");
//                try {
//                    mmSocket.close();
//                } catch (Exception cle) {}
            }
        }

    }


}
