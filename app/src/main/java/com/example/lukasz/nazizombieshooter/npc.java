package com.example.lukasz.nazizombieshooter;

//import org.jbox2d.common.Vec2;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

public class npc {
	private static Context appContext;
	matrixInv MatrixInv;
	a3df A3df;
	f3ds F3ds;

	public float animf[];
	static int ILE_NPC = 50;
	Vec2 pos[];
	Vec2 posConst[];
	Vec2 vec[];
	Vec2 shape[];
	int maxHp[];
	int co[];
	int onground[];
	int unieruchomiony[];
	int ile;
	float reload[];
	float posNoactiv[];
	define Define;
	int playBang;
	Vec2 posBang;
	Vec2 vecBang;
	int jump[];
	float activ[];
	int nodeactiv[];
	float shake[];
	//float angle;
	float smigloWave[];
	float jumpLeninWave;
	//fontInv FontInv;//do wywalenia


	int hp[];
	float waveFlag;
	float visionAngle[];
	float visionAngleConst[];
	float visionAngleWave[];

	Vec3[][] arrow;
	int ileArrow[];



	void reset() {
		ile = 0;
	}


	npc(Context aC) {
		//FontInv= new fontInv(aC);//do wywalenia
		//FontInv.init();



		jumpLeninWave = 0.0f;
		waveFlag = 0.0f;
		Define = new define();
		reload = new float[ILE_NPC];
		visionAngle = new float[ILE_NPC];
		visionAngleConst = new float[ILE_NPC];
		visionAngleWave = new float[ILE_NPC];
		unieruchomiony = new int[ILE_NPC];
		nodeactiv = new int[ILE_NPC];
		co = new int[ILE_NPC];
		onground = new int[ILE_NPC];
		pos = new Vec2[ILE_NPC];
		posConst = new Vec2[ILE_NPC];
		vec = new Vec2[ILE_NPC];
		shape = new Vec2[15];
		maxHp = new int[15];
		hp = new int[ILE_NPC];
		MatrixInv = new matrixInv();
		posNoactiv = new float[ILE_NPC];

		playBang = 0;
		shape[0] = new Vec2(0.45f, 0.5f);
		shape[1] = new Vec2(0.4f, 1.2f);
		shape[2] = new Vec2(0.4f, 0.8f);
		shape[3] = new Vec2(0.4f, 1.0f);
		shape[4] = new Vec2(1.6f, 0.7f);
		shape[5] = new Vec2(0.5f, 1.4f);
		shape[6] = new Vec2(0.67f, 0.8f);
		shape[7] = new Vec2(0.5f, 1.4f);
		shape[8] = new Vec2(0.5f, 1.45f);
		shape[9] = new Vec2(0.5f, 0.6f);
		shape[10] = new Vec2(1.0f, 1.45f);
		shape[11] = new Vec2(0.625f, 1.25f);
		shape[12] = new Vec2(0.4f, 0.8f);
		shape[13] = new Vec2(0.5f, 1.0f);
		shape[14] = new Vec2(0.7f, 0.8f);

		for(int i=0;i<15;i++)
		shape[i].scale(2.0f);

		maxHp[0] = 2;
		maxHp[1] = 5;
		maxHp[2] = 3;
		maxHp[3] = 2;
		maxHp[4] = 5;
		maxHp[5] = 2;
		maxHp[6] = 8;//lenin
		maxHp[7] = 3;//balon test
		maxHp[8] = 5;//balon test
		maxHp[9] = 2;//balon test
		maxHp[10] = 4;//balon test
		maxHp[11] = 3;//balon test
		maxHp[12] = 3;//puke
		maxHp[13] = 4;//puke
		maxHp[14] = 5;//puke

		posBang = new Vec2();
		vecBang = new Vec2();

		A3df = new a3df(aC);
		F3ds = new f3ds(aC);
		jump = new int[ILE_NPC];
		activ = new float[ILE_NPC];
		animf = new float[ILE_NPC];
		smigloWave = new float[ILE_NPC];
		shake = new float[ILE_NPC];

		arrow = new Vec3[ILE_NPC][4];
		ileArrow = new int[ILE_NPC];

		for (int i = 0; i < ILE_NPC; i++) {
			onground[i] =0;
			nodeactiv[i] = 0;
			hp[i] = 0;
			pos[i] = new Vec2();
			posConst[i] = new Vec2();
			vec[i] = new Vec2();
			co[i] = -1;
			reload[i] = 0.0f;
			activ[i] = 0.0f;
			jump[i] = 0;
			animf[i] = 0.0f;
			smigloWave[i] = 3.0f * i;
			unieruchomiony[i]=0;

			shake[i] = 0.0f;

			arrow[i][0] = new Vec3();
			arrow[i][1] = new Vec3();
			arrow[i][2] = new Vec3();
			arrow[i][3] = new Vec3();

			arrow[i][0].set(-100000.0f, 0.0f, 0.0f);
			arrow[i][1].set(-100000.0f, 0.0f, 0.0f);
			arrow[i][2].set(-100000.0f, 0.0f, 0.0f);
			arrow[i][3].set(-100000.0f, 0.0f, 0.0f);

			ileArrow[i] = 0;
		}


		ile = 0;
		appContext = aC;
	}


	void addArrow(int i, Vec3 pa) {
		if (ileArrow[i] == 3) {
			arrow[i][0].set(arrow[i][1]);
			arrow[i][1].set(arrow[i][2]);
			arrow[i][2].set(arrow[i][3]);

			ileArrow[i]--;
		}

		arrow[i][ileArrow[i]].set(new Vec3(pa.x - pos[i].x, pa.y - pos[i].y, pa.z));
		ileArrow[i]++;
	}


	void add(float x, float y, int c, int nodeac) {
		if(ile<59) {
			hp[ile] = maxHp[c];
			animf[ile] = 0.0f;
			pos[ile].set(x, y);
			posConst[ile].set(x, y);
			jump[ile] = 0;
			vec[ile].set(0.03f, 0.0f);
			if (c == 4) vec[ile].set(0.14f, 0.0f);
			co[ile] = c;
			reload[ile] = 0.0f;
			activ[ile] = 0.0f;
			ileArrow[ile] = 0;
			shake[ile] = 0.0f;
			unieruchomiony[ile] = 0;
			nodeactiv[ile] = nodeac;
			posNoactiv[ile]=10.0f;
			ile++;

			Log.d("debuggowanie programu", "dodano zombiaka, ilosc "+ile);
		}
	}


	void del(int j) {
		for (int i = j; i < ile - 1; i++) {
			hp[i] = hp[i + 1];
			animf[i] = animf[i + 1];
			pos[i].set(pos[i + 1].x, pos[i + 1].y);
			posConst[i].set(posConst[i + 1].x, posConst[i + 1].y);
			vec[i].set(vec[i + 1].x, vec[i + 1].y);
			co[i] = co[i + 1];
			nodeactiv[i] = nodeactiv[i + 1];
			activ[i] = activ[i + 1];
			reload[i] = reload[i + 1];
			jump[i] = jump[i + 1];
			ileArrow[i] = ileArrow[i + 1];
			shake[i] = shake[i + 1];
			unieruchomiony[i]=unieruchomiony[i+1];
		}
		ile--;
	}


	public int colision(float xc, float yc) {
		int wynik = -1;
		for (int i = 0; i < ile; i++) {
			if (Define.distanc(xc, yc, pos[i].x, pos[i].y) < 0.8f)
				wynik = i;
		}
		return wynik;
	}

	public int colisionBoxForArrow(float x, float y, float sx, float sy) {
//		if( y-sy<-1.0f)
//			return -1;

		for (int i = 0; i < ile; i++) {
			if (pos[i].x + shape[co[i]].x - 0.1f > x - sx && pos[i].x - shape[co[i]].x - 0.1f < x + sx
					&&
					pos[i].y + shape[co[i]].y - 0.1f > y - sy && pos[i].y - shape[co[i]].y - 0.1f < y + sy)
				return i;
		}

		return -1;
	}


	public Vec2f1i colisionRay(Vec2 x, Vec2 y, float length) {
		Vec2f1i w = new Vec2f1i();
		w.set(10000.0f, 10000.0f, -1);

		float a1 = x.y - y.y;
		float a2 = x.x - y.x;
		float a = a1 / a2;
		float b = y.y - (a * y.x);

		for (int i = 0; i < ile; i++)
			if (Define.distanc(x.x, x.y, pos[i].x, pos[i].y) < length + Define.Length(shape[co[i]].x, shape[co[i]].y)) {
				float MinY = a * (pos[i].x - shape[co[i]].x) + b;
				float MaxY = a * (pos[i].x + shape[co[i]].x) + b;

				float MinX = ((pos[i].y - shape[co[i]].y) - b) / a;
				float MaxX = ((pos[i].y + shape[co[i]].y) - b) / a;


				if (shape[co[i]].y >= Math.abs(pos[i].y - MinY))
					if (Define.distanc(x.x, x.y, pos[i].x - shape[co[i]].x, MinY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (pos[i].x - shape[co[i]].x - x.x > 0)))
							w.set(pos[i].x - shape[co[i]].x, MinY, i);

				if (shape[co[i]].y >= Math.abs(pos[i].y - MaxY))
					if (Define.distanc(x.x, x.y, pos[i].x + shape[co[i]].x, MaxY) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.x - x.x > 0) == (pos[i].x + shape[co[i]].x - x.x > 0)))
							w.set(pos[i].x + shape[co[i]].x, MaxY, i);

				if (shape[co[i]].x >= Math.abs(pos[i].x - MinX))
					if (Define.distanc(x.x, x.y, MinX, pos[i].y - shape[co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (pos[i].y - shape[co[i]].y - x.y > 0)))
							w.set(MinX, pos[i].y - shape[co[i]].y, i);

				if (shape[co[i]].x >= Math.abs(pos[i].x - MaxX))
					if (Define.distanc(x.x, x.y, MaxX, pos[i].y + shape[co[i]].y) < Define.distanc(x.x, x.y, w.x, w.y))
						if (((y.y - x.y > 0) == (pos[i].y + shape[co[i]].y - x.y > 0)))
							w.set(MaxX, pos[i].y + shape[co[i]].y, i);
			}

		if (Define.distanc(x.x, x.y, w.x, w.y) < length)
			return w;
		else
			return new Vec2f1i(0.0f, 0.0f, -1);
	}






	int closest(Vec2 heropos)
	{
		int w=0;
		for(int i=1; i<ile; i++)
		{
			if(Define.distanc(heropos.x, heropos.y, pos[i].x, pos[i].y)<Define.distanc(heropos.x, heropos.y, pos[w].x, pos[w].y))
				w=i;
		}
		return w;
	}






	void logic(float loop, Vec2 HeroPos, int survivemodeflag) {

		for (int i = 0; i < ile; i++) {

		//activ[i]=0.0f;

			if(co[i]==12)
			{
				//posConst[i].set(HeroPos);

				if (vec[i].y > -0.3f)//grawitacja
					vec[i].y -= 0.02f * loop;
				animf[i] += 0.065f*loop*tekstury.bulletTime;



				if (jump[i] == 1) {//skok
					if (co[i] == 2 || co[i] == 3 || co[i]==8)
						vec[i].y = 0.3f;
					else
						vec[i].y = 0.2f;
					jump[i] = 0;
				}

				if (reload[i] < 0.05f && playBang == 0 && activ[i] > 0.9f) {
					playBang = 2;
					reload[i] = 1.0f;
					posBang.set(pos[i]);

					vecBang.set(HeroPos.x - pos[i].x, 25.0f);
					vecBang.normalize();

				}

				if (posConst[i].x - pos[i].x < -2.5f) {
					if (vec[i].x > -0.15f)
						vec[i].x -= 0.04f;
					animf[i] += 0.5f*loop*tekstury.bulletTime;
					}
				if (posConst[i].x - pos[i].x > 2.5f) {
					if (vec[i].x < 0.15f)
						vec[i].x += 0.04f;
					animf[i] += 0.5f*loop*tekstury.bulletTime;
					}


				if (reload[i] > 0.0f)//przeladowanie
					reload[i] -= 0.015f * loop;
				else
					reload[i] = 0.0f;


				if (animf[i] > 4.0f)
					animf[i] = 0.0f;
			}




			if (co[i] == 4) {
				vec[i].set(Define.rotatePoint(vec[i], 4.5f * loop*tekstury.bulletTime));


				if (reload[i] < 0.05f && playBang == 0 && Define.kat2Vector(vec[i], new Vec2(HeroPos.x - pos[i].x, HeroPos.y - pos[i].y)) < 45.0f && Define.distanc(pos[i].x, pos[i].y, HeroPos.x, HeroPos.y) < 8.0f) {
					playBang = 1;
					reload[i] = 1.0f;
					posBang.set(pos[i]);
					vecBang.set(vec[i]);
					//vecBang.normalize();
					vecBang.setLength(1.7f);
				}
				if (reload[i] > 0.0f)//przeladowanie
					reload[i] -= 0.05f * loop;
				else
					reload[i] = 0.0f;
			}





				if (co[i] == 5 || co[i]==7) {

					if (posConst[i].x - pos[i].x < -1.5f)
						if (vec[i].x > -0.04f) {
							//if(co[i]==7)
								vec[i].x -= 0.01f;
							//else
							//	vec[i].x -= 0.0025f;
							shake[i]=-vec[i].x*500.0f;
						}
					if (posConst[i].x - pos[i].x > 1.5f)
						if (vec[i].x < 0.04f) {
							//if(co[i]==7)
								vec[i].x += 0.01f;
							//else
							//	vec[i].x += 0.0025f;
							shake[i]=-vec[i].x*500.0f;
						}

						vec[i].y=0.0f;





					if (co[i] == 5 && reload[i] < 0.05f && playBang == 0 && activ[i]>0.9f) {
						playBang = 1;
						reload[i] = 1.0f;
						posBang.set(pos[i]);
						vecBang.set(HeroPos.x - posBang.x, HeroPos.y - posBang.y);
						//vecBang.normalize();
						vecBang.setLength(1.7f);
					}
					if (reload[i] > 0.0f)//przeladowanie
						reload[i] -= 0.018f * loop;
					else
						reload[i] = 0.0f;

					if(co[i]==7)// && )
					{
						if(activ[i]>0.9f)
							posBang.x=(float)Math.max(0.0f,posBang.x-0.1*loop);
						else
							posBang.x=15.0f;

						pos[i].y = posConst[i].y + (float) Math.sin(pos[i].x * 2.0f) + posBang.x;
					}
					else {
						if(activ[i]>0.9f)
							if(posNoactiv[i]>0.0f)posNoactiv[i]=posNoactiv[i]-0.1f*loop;else posNoactiv[i]=0.0f;
						else {
							posNoactiv[i] = 10.0f;
						}
						pos[i].y = posConst[i].y + (float) Math.sin(pos[i].x * 2.0f);
					}
				}









				if(co[i]<4 || co[i]==6 || co[i]==8  || co[i]==9 || co[i]==10 || co[i]==11 || co[i]==13 || co[i]==14){
						if (vec[i].y > -0.3f)//grawitacja
							vec[i].y -= 0.02f * loop;


						if (jump[i] == 1) {//skok
							if (co[i] == 2 || co[i] == 3 || co[i]==8 || co[i]==14)
								vec[i].y = 0.3f;
							else
								vec[i].y = 0.2f;
							jump[i] = 0;
						}

						if((co[i]==6 && jumpLeninWave>1.0f && hp[i]<11 && onground[i]==1) || (co[i]==14 && jumpLeninWave>1.0f && onground[i]==1) )//skakanie lenina
						{
						vec[i].y = 0.28f;
						jumpLeninWave=0.0f;
						}
						jumpLeninWave=jumpLeninWave+0.05f*loop;


						if (activ[i] < 0.96f) {
							visionAngleWave[i] = visionAngleWave[i] + 0.025f * loop;
							visionAngle[i] = visionAngleConst[i] + (float) Math.sin(visionAngleWave[i]) * 20.0f;

							if (posConst[i].x - pos[i].x < -1.5f) {
								visionAngleConst[i] = 180.0f;//chodzenie jak nieaktywny
								vec[i].x = -0.01f;
								if(co[i]==6)
									vec[i].x = -0.02f;
							}
							if (posConst[i].x - pos[i].x > 1.5f) {
								visionAngleConst[i] = 0.0f;
								vec[i].x = 0.01f;
								if(co[i]==6)
									vec[i].x = 0.02f;
							}
							if(unieruchomiony[i]==1)vec[i].set(0.0f,0.0f);


							if(co[i]==8)animf[i] += 0.02f*loop*tekstury.bulletTime;else animf[i] += 0.045f*loop*tekstury.bulletTime;
						}
						else {
							if (co[i] == 2 || co[i] == 3 || co[i]==8 || co[i]==9 || co[i]==11 || co[i]==13 || co[i]==14 || survivemodeflag==1)// biegna na gracza
								posConst[i].set(HeroPos);

							if (co[i] == 0 || co[i] == 1 || co[i] == 6 || co[i] == 10) {//strzelanie
								visionAngle[i] = Define.katPunkty(pos[i].x - HeroPos.x, pos[i].y - HeroPos.y);
								animf[i] += 0.065f*loop*tekstury.bulletTime;
								if (reload[i] < 0.05f && playBang == 0 && activ[i] > 0.9f) {
									playBang = 1;
									reload[i] = 1.0f;
									posBang.set(pos[i]);
									if(co[i]==10) {
										int side=-1;if((pos[i].x-HeroPos.x)<0.0f)side=1;
										posBang.x = posBang.x + 1.0f *side;}
									vecBang.set(HeroPos.x - pos[i].x, HeroPos.y - pos[i].y);
									vecBang.normalize();
								}

								if (reload[i] > 0.0f)//przeladowanie
								{
									if(co[i]==6)
									reload[i] -= 0.022f * loop;
										else
									reload[i] -= 0.011f * loop;
								}
								else
									reload[i] = 0.0f;
							}


							if (posConst[i].x - pos[i].x < -1.5f) {
								if(co[i]==6)
								{
									if (vec[i].x > -0.1f)
										vec[i].x -= 0.04f;
									animf[i] += 0.4f*loop*tekstury.bulletTime;
								}
								if (co[i] == 0 || co[i] == 1 ) {
									if (vec[i].x > -0.03f)
										vec[i].x -= 0.01f;
									animf[i] += 0.15f*loop*tekstury.bulletTime;
								}
								if ( co[i] == 10) {
									if (vec[i].x > -0.05f)
										vec[i].x -= 0.02f;
									animf[i] += 0.25f*loop*tekstury.bulletTime;
								}
								if (co[i] == 2) {
									if (vec[i].x > -0.08f)
										vec[i].x -= 0.02f;
									animf[i] += 0.4f*loop*tekstury.bulletTime;
								}
								if (co[i] == 3 || co[i]==14) {
									if (vec[i].x > -0.135f)
										vec[i].x -= 0.025f;
									animf[i] += 0.4f*loop*tekstury.bulletTime;
								}
								if (co[i] == 8) {
									if (vec[i].x > -0.155f)
										vec[i].x -= 0.029f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 9) {
									if (vec[i].x > -0.14f)
										vec[i].x -= 0.03f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 11) {
									if (vec[i].x > -0.1f)
										vec[i].x -= 0.02f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 13) {
									if (vec[i].x > -0.08f)
										vec[i].x -= 0.02f;
									animf[i] += 0.35f*loop*tekstury.bulletTime;
								}
							} else if (posConst[i].x - pos[i].x > 1.5f) {
								if(co[i]==6)
								{
									if (vec[i].x < 0.1f)
										vec[i].x += 0.04f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 0 || co[i] == 1) {
									if (vec[i].x < 0.03f)
										vec[i].x += 0.01f;
									animf[i] += 0.15f*loop*tekstury.bulletTime;
								}
								if (co[i] == 10) {
									if (vec[i].x < 0.05f)
										vec[i].x += 0.02f;
									animf[i] += 0.27f*loop*tekstury.bulletTime;
								}
								if (co[i] == 2) {
									if (vec[i].x < 0.075f)
										vec[i].x += 0.02f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 3 || co[i]==14) {
									if (vec[i].x < 0.12f)
										vec[i].x += 0.025f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 8) {
									if (vec[i].x < 0.135f)
										vec[i].x += 0.029f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 9) {
									if (vec[i].x < 0.135f)
										vec[i].x += 0.03f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 11) {
									if (vec[i].x < 0.1f)
										vec[i].x += 0.02f;
									animf[i] += 0.45f*loop*tekstury.bulletTime;
								}
								if (co[i] == 13) {
									if (vec[i].x < 0.075f)
										vec[i].x += 0.02f;
									animf[i] += 0.35f*loop*tekstury.bulletTime;
								}
							}
							if(unieruchomiony[i]==1)vec[i].set(0.0f,0.0f);


				}
				if (animf[i] > 4.0f)
					animf[i] = 0.0f;
				if (shake[i] > 2.0f) shake[i] = shake[i] - 2.0f;
				if (shake[i] < 2.0f) shake[i] = shake[i] + 2.0f;
			}
		}
	}


//
//	void DrawVisionTest(int ShaderProgram, Vector<Vec2> kolizje, float angle) {
//		float[] tmpMatrix = new float[16];
//		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
//
//
//		for(int i = 0; i<kolizje.size();i++)
//		{
////			Matrix.setIdentityM(tmpMatrix, 0);
////			Matrix.translateM(tmpMatrix, 0, kolizje.get(i).x, kolizje.get(i).y, 0.0f);
////
////
//////			if(  Math.abs(Define.kat2Vector(new Vec2(kolizje.get(1).x-kolizje.get(0).x, kolizje.get(1).y*kolizje.get(0).y), new Vec2(kolizje.get(i).x-kolizje.get(0).x, kolizje.get(i).y-kolizje.get(0).y)))<30.0f   )
////			Matrix.scaleM(tmpMatrix, 0, 0.5f, 0.5f, 0.125f);
//////			else
//////			Matrix.scaleM(tmpMatrix, 0, 0.25f, 0.25f, 0.25f);
////
////			//Matrix.scaleM(tmpMatrix, 0, 0.125f * (i+1), 0.125f * (i+1), 0.125f);
////			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
////			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
//			//F3ds.DrawModel(60, tekstury.texture[18], ShaderProgram);
//
//			Vec2 vecAngle=new Vec2();
//			vecAngle.set(5.0f,0.0f);
//
//			vecAngle.set(Define.rotatePoint(vecAngle, angle+30.0f));
//
//			float ang = Define.kat2Vector( vecAngle , new Vec2(kolizje.get(i).x - kolizje.get(0).x, kolizje.get(i).y - kolizje.get(0).y));
//			float dis = (float) Define.distanc(kolizje.get(i).x, kolizje.get(i).y, kolizje.get(0).x, kolizje.get(0).y);
//			FontInv.DrawText3D(new Vec2(kolizje.get(i).x, kolizje.get(i).y), ShaderProgram, tekstury.texture[4], i+" "+dis);
//
//
//		}
//	}
//
//
//
//	void DrawVision(float actywny, int ShaderProgram, Vector<Vec2> kolizje) {
//		float[] tmpMatrix = new float[16];
//		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
//
//
//		Matrix.setIdentityM(tmpMatrix, 0);
//		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
//		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
//
//
//		float points[] = new float[kolizje.size() * 3];
//
//
//		int j = 0;
//		for (int i = 0; i < kolizje.size(); i++) {
//			points[j] = kolizje.get(i).x;
//			points[j + 1] = kolizje.get(i).y;
//			points[j + 2] = -0.05f;
//			j = j + 3;
//		}
//
//		ByteBuffer vbb = ByteBuffer.allocateDirect(points.length * 4);
//		vbb.order(ByteOrder.nativeOrder());
//		FloatBuffer vertexBuffer = vbb.asFloatBuffer();
//
//		vertexBuffer.put(points);
//		vertexBuffer.position(0);
//
//
//		int iActivFS = GLES20.glGetUniformLocation(ShaderProgram, "activ");
//		GLES20.glUniform1f(iActivFS, actywny);
//
//
//		int iAposFS = GLES20.glGetUniformLocation(ShaderProgram, "Apos");
//		GLES20.glUniform2f(iAposFS, kolizje.get(0).x, kolizje.get(0).y);
//
//
//		int positionVSchandle = GLES20.glGetAttribLocation(ShaderProgram, "vPosition");
//
//		GLES20.glVertexAttribPointer(positionVSchandle, 3, GLES20.GL_FLOAT, false, 0, vertexBuffer);
//		GLES20.glEnableVertexAttribArray(positionVSchandle);
//
//		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, kolizje.size());
//
//
//	}


	void DrawSamolot(int ShaderProgram, Vec2 shadowPos, float loop) {

		float[] tmpMatrix = new float[16];


		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
		int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
		GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y - 0.5f);


		for (int i = 0; i < ile; i++)
			if (Define.distanc(pos[i].x, pos[i].y, shadowPos.x, shadowPos.y) < 15.0f)
				if (co[i] == 4) {
					Matrix.setIdentityM(tmpMatrix, 0);
					Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);

					GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);

					float angle = Define.katPunkty(-vec[i].x, -vec[i].y);

					Matrix.setIdentityM(tmpMatrix, 0);
					Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
					Matrix.rotateM(tmpMatrix, 0, angle, 0, 0, 1);
					Matrix.rotateM(tmpMatrix, 0, angle, 1, 0, 0);
					Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
					GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);


					//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
					GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[18]);
					F3ds.DrawModel(63, ShaderProgram);

					smigloWave[i] = smigloWave[i] + 30.0f * loop*tekstury.bulletTime;
					Matrix.setIdentityM(tmpMatrix, 0);
					Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
					Matrix.rotateM(tmpMatrix, 0, angle, 0, 0, 1);
					Matrix.rotateM(tmpMatrix, 0, smigloWave[i], 1, 0, 0);

					Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
					GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
					F3ds.DrawModel(64, ShaderProgram);

					for(int ii=0;ii<ileArrow[i];ii++)
					{
						Matrix.setIdentityM(tmpMatrix, 0);
						Matrix.translateM(tmpMatrix, 0, arrow[i][ii].x + pos[i].x, arrow[i][ii].y + pos[i].y, 0.0f);
						Matrix.rotateM(tmpMatrix, 0, angle, 0, 0, 1);
						Matrix.rotateM(tmpMatrix, 0, arrow[i][ii].z, 0, 0, 1);
						Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
						GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
						F3ds.DrawModel(69, tekstury.texture[2], ShaderProgram);//kalach
					}
				}

	}


void DrawBalon(int ShaderProgram, Vec2 shadowPos)
{
	float[] tmpMatrix = new float[16];

	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");
	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);

	for(int i=0;i<ile;i++)
		if(Define.distanc(pos[i].x, pos[i].y, shadowPos.x, shadowPos.y)<15.0f)
			if(co[i]==5 || co[i]==7)
		{
			Matrix.setIdentityM(tmpMatrix, 0);
			Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
			GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);

			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

			//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[26]);
			F3ds.DrawModel(75, ShaderProgram);//ruski pod balonem



			Matrix.setIdentityM(tmpMatrix, 0);
			Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
			Matrix.rotateM(tmpMatrix, 0, shake[i], 0, 0, 1);
			GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);

			Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

			//GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tekstury.texture[26]);
			F3ds.DrawModel(74, ShaderProgram);//balon



			float angle = Define.katPunkty (vecBang.x-pos[i].x, vecBang.y-pos[i].y);


				Matrix.setIdentityM(tmpMatrix, 0);
				Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y-1.0f, 0.05f);
				Matrix.rotateM(tmpMatrix, 0, angle, 0, 0, 1);
				Matrix.rotateM(tmpMatrix, 0, shake[i], 0, 0, 1);

				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
				F3ds.DrawModel(4, tekstury.texture[1], ShaderProgram);//kalach


			if(reload[i]>0.9f)
				F3ds.DrawModel(6, tekstury.texture[2], ShaderProgram );

			for(int ii=0;ii<ileArrow[i];ii++)
			{
				Matrix.setIdentityM(tmpMatrix, 0);
				Matrix.translateM(tmpMatrix, 0, arrow[i][ii].x + pos[i].x, arrow[i][ii].y + pos[i].y, 0.0f);
				Matrix.rotateM(tmpMatrix, 0, arrow[i][ii].z, 0, 0, 1);
				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
				F3ds.DrawModel(69, tekstury.texture[2], ShaderProgram);//kalach
			}

		}
}





void Draw(int ShaderProgram, Vec2 shadowPos)
{
	 float[] tmpMatrix = new float[16];

	int shadowposFS = GLES20.glGetUniformLocation(ShaderProgram, "shadowpos");
	GLES20.glUniform2f(shadowposFS, shadowPos.x, shadowPos.y-0.5f);


	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	int iVMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VMatrix");

	//int iFbloodpos = GLES20.glGetUniformLocation(ShaderProgram, "bloodpos");
	int iFhppercent = GLES20.glGetUniformLocation(ShaderProgram, "hp");

	for(int i=0;i<ile;i++)
		if(Define.distanc(pos[i].x, pos[i].y, shadowPos.x, shadowPos.y)<15.0f)
	if(co[i]<4 || co[i]==6 || co[i]==8 || co[i]==9 || co[i]==10 || co[i]==11 || co[i]==12 || co[i]==13 || co[i]==14)
	 {
	 Matrix.setIdentityM(tmpMatrix, 0);
	 Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);


		 //GLES20.glUniform2f(iFbloodpos, arrow[i][0].x + pos[i].x, arrow[i][0].y + pos[i].y);

		 GLES20.glUniform1f(iFhppercent, 1.0f-(hp[i]/maxHp[co[i]]) );

		 GLES20.glUniformMatrix4fv(iVMatrix, 1, false, tmpMatrix, 0);
//	 if(   (vec[i].x>0.0f && (activ[i]<0.9f || co[i]==2 || co[i]==3)) || (activ[i]>0.9f && vecBang.x>0.0f))
//		 Matrix.scaleM(tmpMatrix, 0, -1, 1, 1);

		if((co[i]==2 || co[i]==3 || co[i]==8 || co[i]==9 || co[i]==12 || co[i]==13)&&     vec[i].x>0.0f)
			Matrix.scaleM(tmpMatrix, 0, -1, 1, 1);
		if((co[i]==0 || co[i]==1  || co[i]==10)&&     (vec[i].x>0.0f&&activ[i]<0.9f ||  activ[i]>0.9f && vecBang.x>0.0f )  )
			Matrix.scaleM(tmpMatrix, 0, -1, 1, 1);

		 if(co[i]==6)
			 Matrix.scaleM(tmpMatrix, 0, 1.5f, 1.5f, 1.5f);
		 if(co[i]==9)
			 Matrix.scaleM(tmpMatrix, 0, 2.1f, 2.1f, 2.1f);
		 if(co[i]==11)
			 Matrix.scaleM(tmpMatrix, 0, 1.25f, 1.25f, 1.25f);

		 Matrix.scaleM(tmpMatrix, 0,2.0f, 2.0f, 1.0f);


		 Matrix.rotateM(tmpMatrix, 0, shake[i]*2.0f, 0, 0, 1);
	 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);




	 if(co[i]==2 || co[i]==3)
	 	A3df.DrawAnimModel(co[i],tekstury.texture[23],tekstury.texture[24],ShaderProgram,animf[i]);//zomb //
	 if(co[i]==1 || co[i]==0)
	 	A3df.DrawAnimModel(co[i],tekstury.texture[1],tekstury.texture[16],ShaderProgram,animf[i]);//rudy i maly
	if(co[i]==6)
		A3df.DrawAnimModel(0,tekstury.texture[3],tekstury.texture[16],ShaderProgram,animf[i]);//hitler
	if(co[i]==8)
		A3df.DrawAnimModel(4,tekstury.texture[21],tekstury.texture[9],ShaderProgram,animf[i]);//big zomb
	if(co[i]==9)
		 A3df.DrawAnimModel(5,tekstury.texture[21],tekstury.texture[9],ShaderProgram,animf[i]);//baby
	if(co[i]==10)
		A3df.DrawAnimModel(6,tekstury.texture[21],tekstury.texture[9],ShaderProgram,animf[i]);//ss
	if(co[i]==11)
		A3df.DrawAnimModel(7,tekstury.texture[21],tekstury.texture[9],ShaderProgram,animf[i]);//pregnant
	if(co[i]==12) {
		if(reload[i]>0.75f && activ[i]>0.9f)
		A3df.DrawAnimModel(2, tekstury.texture[26], tekstury.texture[24], ShaderProgram, animf[i]);//puke,
		else
		A3df.DrawAnimModel(2, tekstury.texture[18], tekstury.texture[24], ShaderProgram, animf[i]);//puke
	}
	if(co[i]==13)
		A3df.DrawAnimModel(8,tekstury.texture[28],tekstury.texture[16],ShaderProgram,animf[i]);
	if(co[i]==14)
		A3df.DrawAnimModel(9,tekstury.texture[26],tekstury.texture[16],ShaderProgram,animf[i]);
//	 if(co[i]==0)
//		 F3ds.DrawModel(29, tekstury.texture[1], ShaderProgram );//flaga
//	 if(co[i]==2)
//		 F3ds.DrawModel(35, tekstury.texture[23], ShaderProgram );//flaga


	 float angle = Define.katPunkty (vecBang.x-pos[i].x, vecBang.y-pos[i].y);


	 if(co[i]==0 || co[i]==1 || co[i]==6)
		 {
		 Matrix.setIdentityM(tmpMatrix, 0);
//		 if(co[i]==6)
//		 	 Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y-0.2f, 0.0f);
//		 else
			 Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
		 Matrix.rotateM(tmpMatrix, 0, angle, 0, 0, 1);
		 Matrix.rotateM(tmpMatrix, 0, shake[i], 0, 0, 1);

		 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		 F3ds.DrawModel(4, tekstury.texture[1], ShaderProgram);//kalach
		 }


	if(reload[i]>0.9f && activ[i]>0.9f)
		F3ds.DrawModel(6, tekstury.texture[2], ShaderProgram );


	for(int ii=0;ii<ileArrow[i];ii++)
	{
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0, arrow[i][ii].x + pos[i].x, arrow[i][ii].y + pos[i].y, 0.0f);
		Matrix.rotateM(tmpMatrix, 0, arrow[i][ii].z, 0, 0, 1);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		F3ds.DrawModel(69, tekstury.texture[2], ShaderProgram);//kalach
	}

	}

	GLES20.glUniform1f(iFhppercent, 0.0f);
}




public void DrawFlag(int ShaderProgram, float loop, Vec2 shadowPos)
{
	 float[] tmpMatrix = new float[16];
	 	
	 int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	 	
	 
	 int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
		GLES20.glUniform1f(iWaveVS, waveFlag);
		waveFlag=waveFlag-0.3f*loop;
		
	 for(int i=0;i<ile;i++)
		 if(Define.distanc(pos[i].x, pos[i].y, shadowPos.x, shadowPos.y)<15.0f && (co[i]==0 || co[i]==6 || co[i]==2))
	 {
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0,pos[i].x, pos[i].y, 0.0f);
		 if(vec[i].x>0.0f)
			 Matrix.scaleM(tmpMatrix, 0, -1, 1, 1);
		Matrix.rotateM(tmpMatrix, 0, (float)Math.sin(animf[i]/4.0f*3.1515f*2.0f)*7.0f, 0.0f, 0.0f, 1.0f);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		
		
		 if(co[i]==0 || co[i]==6)
			 F3ds.DrawModel(29, tekstury.texture[1], ShaderProgram );//flaga
		 if(co[i]==2)
			 F3ds.DrawModel(35, tekstury.texture[23], ShaderProgram );//flaga
	 }
}




void DrawLifebar(int ShaderProgram)
{
	 float[] tmpMatrix = new float[16];
	 	
	 int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	 
	 for(int i=0;i<ile;i++)

	 {
	 Matrix.setIdentityM(tmpMatrix, 0);
	 Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y + shape[co[i]].y + 0.7f, 0.0f);
	 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	 
	 float hpbar = (float)hp[i]/(float)maxHp[co[i]];
	 
	 F3ds.DrawModel(53, tekstury.texture[1], ShaderProgram);
	 
	 Matrix.setIdentityM(tmpMatrix, 0);
	 Matrix.translateM(tmpMatrix, 0, pos[i].x - 0.51f * (1.0f - hpbar), pos[i].y + shape[co[i]].y + 0.7f, 0.0f);
	 Matrix.scaleM(tmpMatrix, 0, hpbar + 0.1f, 1, 1);
	 
	 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 
	 F3ds.DrawModel(54, tekstury.texture[1], ShaderProgram );
	 }
}

//
//void DrawBullet()
//{
//
//}




}
