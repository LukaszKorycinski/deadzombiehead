package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

/**
 * Created by Lukasz on 2016-04-28.
 */
public class Smugi {
    static int ILE = 15;

    Vec2 pos[];
    Vec2 posEnd[];
    Vec2 posBegin[];
    Vec2 vec[];
    float angle[];

    int co[];
    int ile;
    f3ds F3ds;
    matrixInv MatrixInv;
    define Define;

void reset() {ile = 0; }

Smugi(Context aC) {
    Define=new define();
    pos = new Vec2[ILE];
    posEnd = new Vec2[ILE];
    posBegin = new Vec2[ILE];
    vec = new Vec2[ILE];
    angle = new float[ILE];
    co = new int[ILE];
    MatrixInv = new matrixInv();

    ile = 0;
    F3ds = new f3ds(aC);
    for(int i = 0; i < ILE; i++) {
        angle[i] = 0.0f;
        pos[i] = new Vec2();
        posEnd[i] = new Vec2();
        posBegin[i] = new Vec2();
        vec[i] = new Vec2();
        co[i] = -1;
        }
    }


void add(Vec2 p, Vec2 v, int c) {
        if (ile == ILE - 1)
            del(0);

        pos[ile].set(p);
        posEnd[ile].set(v);
        posBegin[ile].set(p);
        vec[ile].set(v.x-p.x, v.y-p.y);
        vec[ile].setLength(1.2f);
        co[ile] = c;
        angle[ile] = Define.katPunkty(v.x-p.x, v.y-p.y);
        ile++;

    }


    void del(int j)
    {
        for(int i=j; i<ile-1; i++)
        {
            pos[i].set(pos[i+1].x, pos[i+1].y);
            posEnd[i].set(posEnd[i+1].x, posEnd[i+1].y);
            posBegin[i].set(posBegin[i+1].x, posBegin[i+1].y);
            vec[i].set(vec[i+1].x, vec[i+1].y);
            angle[i]=angle[i+1];
            co[i]=co[i+1];
        }
        ile--;
    }


    void logic(int i, float loop)
    {
        //Vec2 vectmp=new Vec2(vec[i].x-pos[i].x, vec[i].y-pos[i].y);
        //vectmp.setLength(0.5f);
        pos[i].set(pos[i].x+vec[i].x*loop, pos[i].y+vec[i].y*loop);
        if(Define.distanc(posBegin[i].x, posBegin[i].y, pos[i].x, pos[i].y)>Define.distanc(posBegin[i].x, posBegin[i].y, posEnd[i].x, posEnd[i].y ))
            del(i);
    }

void Draw(int ShaderProgram, float loop)
    {
        float[] tmpMatrix = new float[16];

        int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
        int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
        GLES20.glUniform1f(iWaveVS, 1.0f);

        for(int i=0;i<ile;i++)
        {
            logic(i, loop);
            Matrix.setIdentityM(tmpMatrix, 0);
            Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
            Matrix.rotateM(tmpMatrix, 0, angle[i], 0, 0, 1);
            Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
            GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

            F3ds.DrawModel(68+co[i]*8, tekstury.texture[19], ShaderProgram );//0 jinx, 1 mf
        }
    }



}