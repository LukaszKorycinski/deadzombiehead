package com.example.lukasz.nazizombieshooter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class Rope {
Vec2 posEnd,posBegin;
float lenght;
matrixInv MatrixInv;
f3ds F3ds;
define Define;
float shootWave;
float shootWaveFake;
int its,itsFake;
Vec2[] splitPos;
//float[] splitSide = new float[20];
Vec2 posFake;
Vec2 endFake;

int splitIle;
int ropeDelay;


Rope(Context aC)
{
	splitPos = new Vec2[20];
	
	
	endFake=new Vec2();
	posFake=new Vec2();
	ropeDelay=0;
	F3ds  = new f3ds(aC);
	MatrixInv = new matrixInv();
	Define = new define();
	posEnd=new Vec2();
	posBegin=new Vec2();
	float lenght=2.0f;
	its=0;
	itsFake=0;
	shootWave=0.0f;
	shootWaveFake=0.0f;
	for(int i=0;i<20;i++)
	{
		splitPos[i] = new Vec2();
		//splitSide[i] = 0.0f;
	}
	splitIle=0;
}


void add(float xb, float yb, float xe, float ye)
{
	shootWave=0.0f;
	posEnd.set(xb, yb);
	posBegin.set(xe, ye);
	lenght=(float) Define.distanc(posBegin.x, posBegin.y, posEnd.x, posEnd.y);
	its=1;
	ropeDelay=1;
}

void addFake(float xb, float yb, float xe, float ye)
{
	shootWaveFake=0.0f;
	endFake.set(xb, yb);
	posFake.set(xb+xe*7.0f, yb+ye*7.0f);
	//lenght=(float) Define.distanc(posBegin.x, posBegin.y, posEnd.x, posEnd.y);
	itsFake=1;
	ropeDelay=1;
}






void split(float xe, float ye)
{
splitPos[splitIle].set(posBegin.x, posBegin.y);
//splitSide[splitIle]=posBegin.kat(v1, v2);
		
splitIle++;

posBegin.set(xe, ye);
//lenght=(float) Define.distanc(posBegin.x, posBegin.y, posEnd.x, posEnd.y);
lenght=(float) (lenght - Define.distanc(posBegin.x, posBegin.y, splitPos[splitIle-1].x, splitPos[splitIle-1].y));
}



void join()
{
lenght=(float) (lenght + Define.distanc(posBegin.x, posBegin.y, splitPos[splitIle-1].x, splitPos[splitIle-1].y));
	
	
splitIle--;
posBegin.set(splitPos[splitIle].x, splitPos[splitIle].y);

}





void del()
{
its=0;
splitIle=0;
ropeDelay=1;
shootWave=0.0f;
}



void shorten(float loop)
{
if( (lenght-0.01f*loop)>0.1f )
lenght=lenght-0.05f*loop;
}

void longer(float loop)
{
if( (lenght-0.01f*loop)<7.0f )
lenght=lenght+0.05f*loop;
}



void DrawFake(int ShaderProgram, float loop)
{
	Vec2 pos = new Vec2();
	float angle=0.0f;
	float Xscale=0.0f;

	if(shootWaveFake>1.0f)
		itsFake=0;
	
		shootWaveFake+=0.1f*loop;
		
		Vec2 HookPos = new Vec2();
		Vec2 HookEnd = new Vec2();

		HookEnd.set(endFake.x, endFake.y);
		HookPos.set( (posFake.x*shootWaveFake+endFake.x*(1.0f-shootWaveFake)), (posFake.y*shootWaveFake+endFake.y*(1.0f-shootWaveFake)) );

		angle = Define.katPunkty(HookPos.x-HookEnd.x, HookPos.y-HookEnd.y);
		

		float[] tmpMatrix = new float[16];
		
		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0,HookPos.x, HookPos.y, -0.15f);
		Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		
		F3ds.DrawModel(45, tekstury.texture[2], ShaderProgram );

		
		pos.set((HookEnd.x+HookPos.x)/2, (HookEnd.y+HookPos.y)/2);
		

		Xscale = (float) (Define.distanc(HookEnd.x, HookEnd.y, HookPos.x, HookPos.y)*0.5f);
		
		iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0,pos.x, pos.y, -0.15f);
		Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
		Matrix.scaleM(tmpMatrix, 0, Xscale, 0.03f, 1);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
		
		F3ds.DrawModel(44, tekstury.texture[14], ShaderProgram );
}





void Draw(int ShaderProgram, float loop)
{

Vec2 pos = new Vec2();
float angle=0.0f;
float Xscale=0.0f;


if(shootWave<1.0)
{
	
	shootWave+=0.7f*loop/lenght;
	
	Vec2 HookPos = new Vec2();
	Vec2 HookEnd = new Vec2();

	HookEnd.set(posEnd.x, posEnd.y);
	HookPos.set( (posBegin.x*shootWave+posEnd.x*(1.0f-shootWave)), (posBegin.y*shootWave+posEnd.y*(1.0f-shootWave)) );

	
		
	//pos.set(HookPos.x, HookPos.y);
	
	angle = Define.katPunkty(HookPos.x-HookEnd.x, HookPos.y-HookEnd.y);
	if(Math.abs(angle)<0.05f)angle=0.05f;
	
	///Xscale = (float) (Define.distanc(posBegin.x, posBegin.y, posEnd.x, posEnd.y)*0.5f);
	
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0,HookPos.x, HookPos.y, -0.15f);
	Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
	//Matrix.scaleM(tmpMatrix, 0, Xscale, 0.03f, 1);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
	
	F3ds.DrawModel(45, tekstury.texture[2], ShaderProgram );
	
	
	
	
	
	
	pos.set((HookEnd.x+HookPos.x)/2, (HookEnd.y+HookPos.y)/2);
	
	//angle = Define.katPunkty(posEnd.x-posBegin.x, posEnd.y-posBegin.y);
	
	Xscale = (float) (Define.distanc(HookEnd.x, HookEnd.y, HookPos.x, HookPos.y)*0.5f);
	
	//float[] tmpMatrix = new float[16];
	
	//int iVPMatrix;
	iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0,pos.x, pos.y, -0.15f);
	Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
	Matrix.scaleM(tmpMatrix, 0, Xscale, 0.03f, 1);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
	
	F3ds.DrawModel(44, tekstury.texture[14], ShaderProgram );
	
	
	
	
	
	
	
	
	
	
	
	
	
}
else
{
	for(int i=0;i<splitIle;i++)
	{
		if(i==splitIle-1)
			{
			pos.set ( (posBegin.x+splitPos[i].x)/2, (posBegin.y+splitPos[i].y)/2 );
			angle = Define.katPunkty( posBegin.x-splitPos[i].x, posBegin.y-splitPos[i].y );
			Xscale = (float) (Define.distanc( splitPos[i].x, splitPos[i].y, posBegin.x, posBegin.y)*0.5f);
			}
		else
			{
			pos.set((splitPos[i].x+splitPos[i+1].x)/2, (splitPos[i].y+splitPos[i+1].y)/2);
			angle = Define.katPunkty( splitPos[i].x-splitPos[i+1].x, splitPos[i].y-splitPos[i+1].y );
			Xscale = (float) (Define.distanc( splitPos[i+1].x, splitPos[i+1].y, splitPos[i].x, splitPos[i].y)*0.5f);
			}
		
		
	
	
		float[] tmpMatrix = new float[16];
	
		int iVPMatrix;
		iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
		Matrix.setIdentityM(tmpMatrix, 0);
		Matrix.translateM(tmpMatrix, 0,pos.x, pos.y, -0.15f);
		Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
		Matrix.scaleM(tmpMatrix, 0, Xscale, 0.03f, 1);
		Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
	
		F3ds.DrawModel(44, tekstury.texture[14], ShaderProgram );	
	}
	
	
	
	
	
	pos.set((posEnd.x+posBegin.x)/2, (posEnd.y+posBegin.y)/2);
	
	angle = Define.katPunkty(posEnd.x-posBegin.x, posEnd.y-posBegin.y);
	
	Xscale = (float) (Define.distanc(posBegin.x, posBegin.y, posEnd.x, posEnd.y)*0.5f);
	
	float[] tmpMatrix = new float[16];
	
	int iVPMatrix;
	iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0,pos.x, pos.y, -0.15f);
	Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
	Matrix.scaleM(tmpMatrix, 0, Xscale, 0.03f, 1);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
	
	F3ds.DrawModel(44, tekstury.texture[14], ShaderProgram );
	
	
	
	
	Vec2 HookPos = new Vec2();
	Vec2 HookEnd = new Vec2();
	if(splitIle==0)
	{
		HookPos.set(posBegin.x, posBegin.y);
		HookEnd.set(posEnd.x, posEnd.y);
	}
	else
	{
		HookPos.set(splitPos[0].x, splitPos[0].y);
		if(splitIle==1)
			HookEnd.set(posEnd.x, posEnd.y);
		else
			HookEnd.set(splitPos[1].x, splitPos[1].y);
	}
	
		
	//pos.set(HookPos.x, HookPos.y);
	
	angle = Define.katPunkty(HookPos.x-HookEnd.x, HookPos.y-HookEnd.y);
	
	///Xscale = (float) (Define.distanc(posBegin.x, posBegin.y, posEnd.x, posEnd.y)*0.5f);
	
	//float[] tmpMatrix = new float[16];
	
	//int iVPMatrix;
	iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	Matrix.setIdentityM(tmpMatrix, 0);
	Matrix.translateM(tmpMatrix, 0,HookPos.x, HookPos.y, -0.15f);
	Matrix.rotateM(tmpMatrix, 0, angle, 0,0,1);
	//Matrix.scaleM(tmpMatrix, 0, Xscale, 0.03f, 1);
	Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
	GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);
	
	F3ds.DrawModel(45, tekstury.texture[2], ShaderProgram );
}






}


	
}
