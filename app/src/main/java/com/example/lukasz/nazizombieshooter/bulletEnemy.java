package com.example.lukasz.nazizombieshooter;

import java.util.Random;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

public class bulletEnemy {
	
static int ILE = 20;
Vec2 pos[];
Vec2 vec[];
int co[];
float wave[];
float waveAddDym[];

static int ILE_DYM = 8;
Vec2 posDym[][];
float waveDym[][];
float vecDym[][];
int coDym[][];

float angle[];
int ile;
int dymIle[];
matrixInv MatrixInv;
f3ds F3ds;
define Define;


void reset()
{
ile=0;	
}


bulletEnemy(Context aC)
{
	pos=new Vec2[ILE];
	vec=new Vec2[ILE];
	wave=new float[ILE];
	co=new int[ILE];
	waveAddDym=new float[ILE];

	posDym=new Vec2[ILE][ILE_DYM];
	waveDym=new float[ILE][ILE_DYM];
	vecDym=new float[ILE][ILE_DYM];
	coDym=new int[ILE][ILE_DYM];

	angle=new float[ILE];

	dymIle=new int[ILE];
	MatrixInv = new matrixInv();
	
	
	
	
	
	
Define=new define();
F3ds  = new f3ds(aC);
for(int i=0; i<ILE; i++)
{
for(int j=0;j<ILE_DYM;j++)
	{
	waveDym[i][j]=0.0f;
	vecDym[i][j]=0.0f;
	posDym[i][j]=new Vec2();
	coDym[i][j]=0;
	}
dymIle[i]=0;

pos[i]=new Vec2();
vec[i]=new Vec2();
co[i]=0;
wave[i]=0.0f;
waveAddDym[i]=0.0f;
angle[i]=0.0f;
}
ile=0;

}


void add(float px, float py, float vx, float vy)
{
if(ile<10)
{
pos[ile].set(px,py);
vec[ile].set(vx,vy);
for(int j=0;j<ILE_DYM;j++)
	{
	waveDym[ile][j]=0.0f;
	posDym[ile][j].set(px,py);
	coDym[ile][j]=0;
	}
dymIle[ile]=0;

co[ile]=0;
wave[ile]=0.0f;
waveAddDym[ile]=0.0f;
angle[ile]=Define.katPunkty(vx, vy);

ile++;
}
}


	void add(float px, float py, float vx, float vy, int c)
	{
		if(ile<10)
		{
			pos[ile].set(px,py);
			vec[ile].set(vx,vy);
			for(int j=0;j<ILE_DYM;j++)
			{
				waveDym[ile][j]=0.0f;
				posDym[ile][j].set(px,py);
				coDym[ile][j]=0;
			}
			dymIle[ile]=0;

			co[ile]=c;
			wave[ile]=0.0f;
			waveAddDym[ile]=0.0f;
			angle[ile]=Define.katPunkty(vx, vy);

			ile++;
		}
	}


void del(int j)
{
for(int i=j; i<ile-1; i++)
{
for(int jj=0;jj<ILE_DYM;jj++)
	{
	waveDym[i][jj]=waveDym[i+1][jj];
	posDym[i][jj].set(posDym[i+1][jj].x,posDym[i+1][jj].y);
	coDym[i][j]=coDym[i+1][j];
	}

pos[i].set(pos[i+1].x, pos[i+1].y);
vec[i].set(vec[i+1].x, vec[i+1].y);
angle[i]=angle[i+1];
wave[i]=wave[i+1];
co[i]=co[i+1];
waveAddDym[i]=waveAddDym[i+1];
dymIle[i]=dymIle[i+1];
}
ile--;
}



int coli(float px, float py, float cx, float cy)
{
int w=0;
for(int i=0;i<ile;i++)
{

}
	
return w;
}



void addDym(int i)
{
//for(int j=0;j<4;j++)
//{
Random random=new Random();
waveDym[i][dymIle[i]]=0.0f;
vecDym[i][dymIle[i]]=random.nextFloat()*0.02f - 0.01f;
posDym[i][dymIle[i]].set(pos[i].x,pos[i].y);
//}
dymIle[i]++;
if(dymIle[i]>ILE_DYM-1)
	dymIle[i]=0;
}



void logic(float loop)
{
for(int i=0;i<ile;i++)
	{
		if(co[i]==0) {
			pos[i].set(pos[i].x + vec[i].x * 0.05f * loop, pos[i].y + vec[i].y * 0.05f * loop);
			wave[i] += 0.003f*loop;

			waveAddDym[i] += 0.5f * loop;
			if (waveAddDym[i] > 1.0f) {
				waveAddDym[i] = 0.0f;
				addDym(i);
			}
			for (int j = 0; j < dymIle[i]; j++) {
				waveDym[i][j] += 0.03f * loop;
				posDym[i][j].y += vecDym[i][j] * loop;
			}
			if(wave[i]>1.0f)
				del(i);
		}


		if(co[i]==1) {
			pos[i].set(pos[i].x + vec[i].x * 0.14f * loop, pos[i].y + vec[i].y * 0.14f * loop);
			wave[i] += 0.0025f*loop;
			vec[i].y=vec[i].y-0.01f*loop;

			waveAddDym[i] += 0.5f * loop;
			if (waveAddDym[i] > 1.0f) {
				waveAddDym[i] = 0.0f;
				addDym(i);
			}
			for (int j = 0; j < dymIle[i]; j++) {
				waveDym[i][j] += 0.03f * loop;
				posDym[i][j].y += vecDym[i][j] * loop;
			}
			if(wave[i]>1.0f)
				del(i);
		}
	}
}




void DrawDym(int ShaderProgram)
{
	 float[] tmpMatrix = new float[16];
	 int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");
	 int iWaveVS = GLES20.glGetUniformLocation(ShaderProgram, "wave");
	 
	 for(int i=0;i<ile;i++)
		 for(int j=0;j<ILE_DYM;j++)
		 {
		 GLES20.glUniform1f(iWaveVS, 1.0f - waveDym[i][j]);
		 Matrix.setIdentityM(tmpMatrix, 0);
		 Matrix.translateM(tmpMatrix, 0, posDym[i][j].x, posDym[i][j].y, 0.0f);
		 if(co[i]==0)
		 	Matrix.scaleM(tmpMatrix, 0, waveDym[i][j] * 0.5f, waveDym[i][j] * 0.5f, 1.0f);
		 //if(co[i]==1)
		//	Matrix.scaleM(tmpMatrix, 0, 0.7f, 0.7f, 1.0f);
		 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
		 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0); 

		 if(co[i]==0)
		 	F3ds.DrawModel(26, tekstury.texture[19], ShaderProgram );
		 if(co[i]==1)
			F3ds.DrawModel(101, tekstury.texture[14], ShaderProgram );
		 }	
}



void Draw(int ShaderProgram, float loop)
{
	loop=loop*tekstury.bulletTime;

	logic(loop);
	
	 float[] tmpMatrix = new float[16];
	 	
	 int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");

	 	
	 for(int i=0;i<ile;i++)
	 {
		 if(co[i]==0) {
			 Matrix.setIdentityM(tmpMatrix, 0);
			 Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
			 Matrix.rotateM(tmpMatrix, 0, angle[i], 0, 0, 1);
			 Matrix.scaleM(tmpMatrix, 0, 1.9f, 1.9f, 1.0f);
			 Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
			 GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

			 F3ds.DrawModel(34, tekstury.texture[2], ShaderProgram);
		 }
	 }	
}

	void DrawPuke(int ShaderProgram, float loop)
	{
		loop=loop*tekstury.bulletTime;

		logic(loop);

		float[] tmpMatrix = new float[16];

		int iVPMatrix = GLES20.glGetUniformLocation(ShaderProgram, "u_VPMatrix");


		for(int i=0;i<ile;i++)
		{
			if(co[i]==1) {
				Matrix.setIdentityM(tmpMatrix, 0);
				Matrix.translateM(tmpMatrix, 0, pos[i].x, pos[i].y, 0.0f);
				Matrix.rotateM(tmpMatrix, 0, angle[i], 0, 0, 1);
				Matrix.scaleM(tmpMatrix, 0, 1.3f, 1.3f, 1.3f);
				Matrix.multiplyMM(tmpMatrix, 0, matrixInv.mViewProjectionMatrix, 0, tmpMatrix, 0);
				GLES20.glUniformMatrix4fv(iVPMatrix, 1, false, tmpMatrix, 0);

				F3ds.DrawModel(100, tekstury.texture[14], ShaderProgram);
			}
		}
	}




	
}
