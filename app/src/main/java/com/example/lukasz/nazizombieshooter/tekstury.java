package com.example.lukasz.nazizombieshooter;

//import org.jbox2d.common.Vec2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import com.example.lukasz.style.R;

public class tekstury {
//private static Context appContext;
static int ile_tekstur = 33;

public static int secNdPlayer_flag;
public static int[] texture = new int[ile_tekstur];
public static int[] Skytex = new int[2];
public static float bulletTime, bulletZasob;
public static int[] ActualtexListGame = new int[ile_tekstur];
public static int[] ActualtexListMenu = new int[ile_tekstur];
public static int[] ActualtexList = new int[ile_tekstur];

public static Vec2 HeroPosStatic = new Vec2();
public static Vec2 NdPlayerPosStatic = new Vec2();

public static Vec2 HeroShotStatic = new Vec2();
public static Vec2 NdPlayerShotStatic = new Vec2();
public static int HeroShootingStatic;
public static int NdPlayerShootingStatic;

public static float HeroAngleStatic;
public static float NdPlayerAngleStatic;

public static int HeroBronStatic;
public static int NdPlayerBronStatic;

public static float NdPlayerreloadStatic;
public static float HeroreloadStatic;

public static float NdPlayerAnimStatic;
public static float HeroAnimStatic;

//public static int dupaint;
//	public static int dupaint2;
public static int hitPlayerEnemy;
public static int ndhitPlayerEnemy;
public static int ndhpMulti;
public static int hpMulti;

public static int HeroFlagaStatic;
public static int NdPlayerFlagaStatic;



public static Context appContext;
public static String komunikat;

public static int synhronizedBlueThread;

tekstury(Context aC) {



	komunikat="waiting";
	ndhpMulti=0;
	hpMulti=0;

	secNdPlayer_flag=0;
	ndhitPlayerEnemy=0;
	hitPlayerEnemy=0;
	//dupaint=0;
	//dupaint2=0;
	synhronizedBlueThread=0;
	HeroShootingStatic=0;
	NdPlayerShootingStatic=0;


	NdPlayerAnimStatic=0.0f;
	HeroAnimStatic=0.0f;

	HeroreloadStatic=0.0f;
	NdPlayerreloadStatic=0.0f;

	HeroAngleStatic = 0.0f;
	NdPlayerAngleStatic = 0.0f;

	HeroBronStatic=0;
	NdPlayerBronStatic=1;

	HeroFlagaStatic=0;
	NdPlayerFlagaStatic=0;

	//dupastring="defaul";
	appContext = aC;
	bulletTime = 1.0f;
	bulletZasob = 1.0f;

	for(int i=0;i<ile_tekstur;i++) {
		ActualtexListGame[i] = 1;
		ActualtexListMenu[i] = 1;
	}
	ActualtexListMenu[3]=1;
	ActualtexListMenu[4]=1;
	ActualtexListMenu[6]=1;
	ActualtexListMenu[18]=1;
	ActualtexListMenu[20]=1;
	ActualtexListMenu[25]=1;
	ActualtexListMenu[26]=1;

	for(int i=0;i<ile_tekstur;i++)
		ActualtexList[i]=ActualtexListGame[i];
}

public static void loadlandscape(int i)
{
	bulletZasob = 1.0f;
	bulletTime = 1.0f;
	GLES20.glDeleteTextures(1, Skytex, 0);
	Bitmap bmp;
	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.stars);
	if(i==1)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.stars2);
	if(i==2)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.stars3);
	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, Skytex[0]);

		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);

	GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

	bmp.recycle();
	bmp = null;
}


public static void loadTekstury()
{
	destroy();

		GLES20.glGenTextures(ile_tekstur, texture, 0);
		
	    Bitmap bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.boxh);

	    for(int i=0;i<ile_tekstur;i++)
			if(ActualtexList[i]==1)
	    	{
			//if(i==0)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.boxh);
			if(i==1)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.npc);
			if(i==2)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.har);
			if(i==3)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.buttons);
			if(i==4)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.fonta);
			if(i==5)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.house);
			if(i==6)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.sky);
			if(i==7)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.house2b);
			if(i==8)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.gras);
			if(i==9)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.npc2alfa);
			if(i==10)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.tree);
			if(i==11)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.leaf);
			if(i==12)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.leaf2);
			if(i==13)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.sand);
			if(i==14)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.buttons1);
			if(i==15)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.gras2);
			if(i==16)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.npcalfa);
			if(i==17)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.leafgras);
			if(i==18)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.buttons2);
			if(i==19)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.bang);
			if(i==20)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.mount);
			if(i==21)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.npc2);
			if(i==22)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.boxcity);
			if(i==23)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.zomb);
			if(i==24)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.zombalfa);
			if(i==25)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.logo);
			if(i==26)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.buttons3);
			if(i==27)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.flyrock);
			if(i==28)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.boxv);
			if(i==29)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.npcalfa);//smiec
			if(i==30)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.npcalfa);//smiec
			if(i==31)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.harsad);
			if(i==32)	bmp = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.harsmile);



	    	GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture[i]);
	    	if(i==8)
		    	{
		    	GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		    	GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		    	}
	    	else
		    	{
		    	GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		    	GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		    	}
	    	//GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
	    	
	        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);

			bmp.recycle();
			bmp = null;
	    	}



	    //InitDenamicTexture();
}
	





static void  destroy()
{
	GLES20.glDeleteTextures(ile_tekstur, texture, 0);
}





	
	
}
