package com.example.lukasz.nazizombieshooter;
import android.os.SystemClock;
import android.util.Log;


public class FpsTime {
    long startTime;
    public int fps;
    int frames = 0;

    public void FpsCal() {
        frames++;
        if(System.nanoTime() - startTime >= 1000000000) {
            //Log.d("FPSCounter", "fps: " + frames);
        	fps=frames;
            frames = 0;
            startTime = System.nanoTime();
        }
    }
    
    
    
    
    public float loop, loopStart, loopRealtime;
    
    FpsTime()
    {
    loop=1.0f;
    loopRealtime=1.0f;
    startTime = System.nanoTime();
    loopStart = SystemClock.elapsedRealtime();
    }

    public void loopCal()
    {
        loopRealtime=((float)SystemClock.elapsedRealtime()-loopStart)/30.0f;//1.3f;//(System.nanoTime()-loopStart)/30000000;


        loop=(loop*39.0f+loopRealtime)/40.0f;

    	if(loop>3.0f)
    		loop=3.0f;
    	
    	loopStart = SystemClock.elapsedRealtime();
    }
    

    
    
}
