precision mediump float;

uniform sampler2D texture;
uniform sampler2D texalfa;
uniform vec2 shadowpos;
uniform float hp;
uniform vec4 lvlTheme;

varying vec2 out_TexCoord;   
varying vec3 PosModeView;


void main() 
{

vec4 color  = texture2D( texture, out_TexCoord);
vec4 color2 = texture2D( texalfa, out_TexCoord);
color.xyz = color.xyz * lvlTheme.xyz;

vec3 PosModeView2 = PosModeView;
PosModeView2.z=0.0;
vec3 ShadowPos=vec3(shadowpos.x,shadowpos.y,0.0);

vec3 bloodcolor=vec3(0.8,0.0,0.0);
float red=color2.r*hp;
if(red>0.5)
color.rgb = bloodcolor;
color.a=color.a - color2.r*hp*0.5;


float fog=-0.17+max(min( length (ShadowPos-PosModeView2)*0.055 ,1.2),0.0);
color.rgb = color.rgb - fog;

gl_FragColor.rgb =  color.rgb;

if(color.a<0.5)
discard;
}