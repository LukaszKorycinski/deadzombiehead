precision mediump float;

uniform sampler2D tex;
uniform sampler2D texalfa;
uniform vec4 lvlTheme;
uniform float bulletEffect;

varying vec2 out_TexCoord;   
uniform float wave;
 uniform float col;


void main() 
{
vec4 color = texture2D( tex, out_TexCoord);
vec4 color2 = texture2D( texalfa, out_TexCoord);

vec3 bloodcolor=vec3(0.8*col,0.0,0.0);

color.rgb = color.rgb * lvlTheme.rgb;

//gl_FragColor.rgb = color.rgb;




float grayscale=0.25*color.r + 0.54*color.g + 0.1140*color.b;

color.a=color.a - color2.r*wave;
gl_FragColor.rgb = color.rgb*(1.0-bulletEffect) + grayscale*bulletEffect ;





if(color.a<0.6)
gl_FragColor.rgb = bloodcolor;

if(color.a<0.4)
discard;
}