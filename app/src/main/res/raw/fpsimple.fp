precision mediump float;

uniform sampler2D texture;

varying vec2 out_TexCoord;   
uniform float pulse;
 


void main() 
{

vec4 color = texture2D( texture, out_TexCoord);


color.a=color.a*pulse;

gl_FragColor =  color;

if(color.a<0.05)
discard;
}