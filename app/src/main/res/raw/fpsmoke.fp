precision mediump float;

uniform sampler2D texture;

varying vec2 out_TexCoord;   

uniform float wave;


void main() 
{

vec4 color = texture2D( texture, out_TexCoord);

gl_FragColor =  color;

gl_FragColor.a=gl_FragColor.a*wave;
//if(color.a<0.5)
//discard;
}