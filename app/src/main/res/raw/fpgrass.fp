precision mediump float;

uniform sampler2D texture;
//uniform sampler2D texshoot;

uniform vec2 shadowpos;
uniform vec4 lvlTheme;
uniform float bulletEffect;

varying vec2 out_TexCoord;
varying vec3 PosModeView;


void main()
{
vec4 color = texture2D( texture, out_TexCoord);

//vec2 DynamicTexCoord = vec2(PosModeView.x/10.0/8.0, PosModeView.y/10.0);
//vec4 color2 = texture2D( texshoot, DynamicTexCoord);


vec3 PosModeView2 = PosModeView;
PosModeView2.z=0.0;

vec3 ShadowPos=vec3(shadowpos.x,shadowpos.y,0.0);

float fog=-0.2+max(min( length (ShadowPos-PosModeView2)*0.075*lvlTheme.w ,1.2),0.0);
color.rgb = (color.rgb - fog) * lvlTheme.xyz;



float grayscale=0.25*color.r + 0.54*color.g + 0.1140*color.b;

gl_FragColor.rgb = color.rgb*(1.0-bulletEffect) + grayscale*bulletEffect ;

if(color.a<0.5)
discard;
}