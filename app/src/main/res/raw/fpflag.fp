precision mediump float;

uniform sampler2D texture;
uniform float wave;
uniform vec4 lvlTheme;

varying vec2 out_TexCoord;   
varying float PosX;  




void main() 
{
vec2 texcoord = out_TexCoord;
texcoord.y=texcoord.y+sin(PosX*12.0+wave)*0.01*PosX;


vec4 color = texture2D( texture, texcoord);

color.xyz=color.xyz*lvlTheme.xyz;

color.rgb = color.rgb+sin(PosX*12.0+wave)*0.2*PosX;

gl_FragColor =  color;

if(color.a<0.5)
discard;
}