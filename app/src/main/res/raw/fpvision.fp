precision mediump float;


uniform float activ;
uniform vec2 Apos;

varying vec2 pos;

void main() 
{

float alfa = length(pos-Apos)*0.2;
gl_FragColor=vec4(0.0+activ, 1.0-activ, 0.0, 1.0-alfa);

}