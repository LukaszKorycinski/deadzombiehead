precision mediump float;

uniform sampler2D texture;

varying vec2 out_TexCoord;   

 


void main() 
{
vec4 color = texture2D( texture, out_TexCoord);


gl_FragColor =  color;

if(color.a<0.5)
discard;
}