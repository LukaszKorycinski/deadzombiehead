precision mediump float;
uniform sampler2D texture;
uniform float bulletlevel;
uniform float wave;
uniform float activ;

varying vec3 PosModeView;
varying vec2 out_TexCoord;

void main() 
{

vec4 color = texture2D( texture, out_TexCoord);
color.a=0.75;

color.g=activ* (sin(wave*5.0)*0.5+0.5);

float level = PosModeView.y-0.155;
level=level+sin(PosModeView.x*25.0+wave)*0.003;



if(level<bulletlevel)
    gl_FragColor.rgba = color.rgba ;
else
    discard;
}