precision mediump float;

uniform sampler2D texture;
uniform float wave;
uniform float colorUni;

varying vec2 out_TexCoord;   
varying float PosY;




void main() 
{
vec2 texcoord = out_TexCoord;
texcoord.x=texcoord.x+sin(PosY*5.0+wave)*0.01*PosY;


vec4 color = texture2D( texture, texcoord);

color.rgb = color.rgb+sin(PosY*5.0+wave)*0.2*PosY;

gl_FragColor =  color+colorUni;

if(color.a<0.5)
discard;
}