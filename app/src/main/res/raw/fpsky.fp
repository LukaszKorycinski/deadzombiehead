precision highp float;

uniform sampler2D texture;
uniform sampler2D texture2;
uniform sampler2D texture3;
uniform float xpos;
uniform vec3 lvlTheme;
uniform float bulletEffect;

varying vec2 out_TexCoord;   
varying vec2 out_TexCoord2;
 


void main() 
{
/*
vec4 color = texture2D( texture2, out_TexCoord);
vec4 color2 = texture2D( texture, out_TexCoord2*2.7);

vec2 out_TexCoordtmp=vec2(out_TexCoord.x-xpos, out_TexCoord.y);
vec4 color3 = texture2D( texture3, out_TexCoordtmp);

color3.rgb=(color3.rgb/color3.a)*lvlTheme;

gl_FragColor =  color3*color3.a+ (color+color2)*(1.0-color3.a);*/

vec4 color = texture2D( texture2, out_TexCoord);
vec4 color2 = texture2D( texture, out_TexCoord2*2.7);
vec2 out_TexCoordtmp=vec2(out_TexCoord.x-xpos, out_TexCoord.y);
vec4 color3 = texture2D( texture3, out_TexCoordtmp);

color3.rgb=(color3.rgb/(color3.a+0.01))*lvlTheme;





color =  vec4(color3.rgb*color3.a+ (color.rgb+color2.rgb)*(1.0-color3.a), 1);



gl_FragColor.a = color.a ;
gl_FragColor.rgb = color.rgb*(1.0-bulletEffect)  ;



}