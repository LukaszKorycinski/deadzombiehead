precision mediump float;

uniform sampler2D texture;
uniform vec2 shadowpos;
uniform vec2 bloodpos;


varying vec2 out_TexCoord;   
varying vec3 PosModeView;


void main() 
{

vec4 color = texture2D( texture, out_TexCoord);

vec3 PosModeView2 = PosModeView;
PosModeView2.z=0.0;
vec3 ShadowPos=vec3(shadowpos.x,shadowpos.y,0.0);

float fog=-0.1+max(min( length (ShadowPos-PosModeView2)*0.075 ,1.2),0.0);
color.rgb = color.rgb - fog;




vec3 bloodposv=vec3(bloodpos.x, bloodpos.y,0.0);

float bloodplum=min(length(bloodposv-PosModeView2),1.0);

color.rgb=vec3(color.r + (1.0-bloodplum), color.g - (1.0-bloodplum), color.b - (1.0-bloodplum));


gl_FragColor.rgb = color.rgb;



if(color.a<0.5)
discard;
}